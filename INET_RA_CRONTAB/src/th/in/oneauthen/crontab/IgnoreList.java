package th.in.oneauthen.crontab;

import java.util.ArrayList;
import java.util.Arrays;

public class IgnoreList {
	private ArrayList<String> EMAIL_LIST = new ArrayList<>(Arrays.asList("Buenos Aires", "Córdoba", "La Plata"));
	private ArrayList<String> FORM_ID_LIST = new ArrayList<>(Arrays.asList("Buenos Aires", "Córdoba", "La Plata"));
	private ArrayList<String> CERT_SN_LIST = new ArrayList<>(Arrays.asList("Buenos Aires", "Córdoba", "La Plata"));
	
	public boolean isIgnoreEmail( String email ) {
		return EMAIL_LIST.contains(email);
	}
	public boolean isIgnoreFormList( String fromId ) {
		return FORM_ID_LIST.contains(fromId);
	}
	public boolean isIgnoreSNList( String certSn ) {
		return CERT_SN_LIST.contains(certSn);
	}
}
