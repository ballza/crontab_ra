package th.in.oneauthen.crontab;

import java.security.cert.CertificateException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import th.athichitsakul.dao.DAO;
import th.in.oneauthen.emailObj.MapCertType;
import th.in.oneauthen.object.CertificateDB;
import th.in.oneauthen.object.RequestCertDB;
import th.in.oneauthen.object.UserRaDB;
import th.in.oneauthen.object.DAO.CertificateDAO;
import th.in.oneauthen.object.DAO.RequestCertDAO;
import th.in.oneauthen.pki.GenericPKIAdapter;
import th.in.oneauthen.pki.PKIConnectorLoader;
import th.in.oneauthen.pki.certificate.X509CertificateUtility;
import th.in.oneauthen.util.ConfigUtil;
import th.in.oneauthen.util2.MailNotificationUtil;

public class Crontab_RA {

	public static void main(String[] args) {

		try {
			if ("cert".equalsIgnoreCase(args[0]))
				updateCertificate();
			else if ("request".equalsIgnoreCase(args[0]))
				updateRequest();
			else
				checkEXP();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			destroy();
		}

	}

	public static List<String> updateCertificate() {
		List<CertificateDB> ListReq = null;
		CertificateDAO certDAO = new CertificateDAO();
		try {
			ListReq = certDAO.list();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<String> certSnList = new ArrayList<String>();
		if (ListReq == null) {

		} else {
			for (CertificateDB req : ListReq)
				certSnList.add(req.getCert_Sn());
		}
		Date expiredDate;
		try {
			GenericPKIAdapter adapter = PKIConnectorLoader.adapterLoader();
			if (adapter == null)
				throw new Exception("Error load PKI connector.");

			List<HashMap<String, String>> listObj = adapter.serviceUpdateCertData(certSnList);
			System.out.println("Query listObj size = " + listObj.size());
			for (HashMap<String, String> certObj : listObj) {
				try {
					if (certObj == null)
						continue;

					CertificateDB cert = certDAO.findBySn(certObj.get(GenericPKIAdapter.SERIAL_NUMBER));
					cert.setCert_Sn(certObj.get(GenericPKIAdapter.SERIAL_NUMBER));
					cert.setCert_x509(certObj.get(GenericPKIAdapter.BASE64));
					cert.setRevokeReason(certObj.get(GenericPKIAdapter.REVOKE_REASON));
					// cert.setStatus(certObj.get(GenericPKIAdapter.STATUS));
					cert.setUnique_id(certObj.get(GenericPKIAdapter.UNIQUE_ID));
					// cert.setCreator(new UserRaDAO().find(2));
					X509CertificateUtility certUtil = new X509CertificateUtility(certObj.get(GenericPKIAdapter.BASE64));
					expiredDate = certUtil.getCertificate().getNotAfter();
					cert.setExpiredDate(expiredDate);

					if (!(certObj.get(GenericPKIAdapter.STATUS).equals("11"))) {
						if (cert.getStatus().equals("success"))
							cert.setStatus("success");
						else if (cert.getStatus().equals("5"))
							cert.setStatus("5");
						else
							cert.setStatus(certObj.get(GenericPKIAdapter.STATUS));
					} else {
						cert.setStatus(certObj.get(GenericPKIAdapter.STATUS));
					}

					certDAO.serviceSyncDB(cert);
				} catch (Exception e) {
					continue;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return certSnList;

	}

	public static void checkEXP() throws Exception {
		String month_config = ConfigUtil.MONTH_EXD;
		String[] month_array = month_config.split(",");

		DateFormat dateNoti = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));
		DateFormat dateNow = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));
		String result = dateNow.format(new Date());
		Date dateN = dateNow.parse(result);
		
		IgnoreList ignore = new IgnoreList();

		for (int i = 0; i < month_array.length; i++) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(dateN);
			if (month_array[i].contains("M"))
				cal.add(Calendar.MONTH, +Integer.parseInt(month_array[i].replace("M", "")));
			else
				cal.add(Calendar.DATE, +Integer.parseInt(month_array[i].replace("D", "")));

			System.out.println(cal.getTime());

			List<CertificateDB> certList = new CertificateDAO().getCertFromStatus("success");
			for (CertificateDB cert : certList) {
				
				if(ignore.isIgnoreSNList(cert.getCert_Sn().toLowerCase()) || ignore.isIgnoreFormList(cert.getUnique_id())) continue;
				
				HashMap<String, String> mapInfo = getInfoCertificate(cert.getCert_x509());

				String contentEmail = ConfigUtil.CHECK_EXD_MSG
						.replace("[USERNAME]", " : " + cert.getCreator().getUserName())
						.replace("[CN]", mapInfo.get("cn"))
						.replace("[CERT_TYPE]", MapCertType.getCertType(cert.getCert_Type()));

				String dateStr = dateNow.format(cert.getExpiredDate());
				// System.out.println(dateStr);
				Calendar calendar = Calendar.getInstance();
				Date dateAP = dateNoti.parse(dateStr);
				calendar.setTime(dateAP);

				if (calendar.compareTo(cal) == 0) {
				
					if(ignore.isIgnoreEmail(mapInfo.get("emailALTN"))) continue;
					// do it	
					if (month_array[i].contains("M")) {// every month
						new MailNotificationUtil().sendEmailEXP(
								ConfigUtil.MAIL_SUBJECT, contentEmail
										.replace("[MONTH]", month_array[i].replace("M", "")).replace("[UNIT]", "เดือน"),
										//"chaichana.si@oneAuthen.in.th,thanonphat.su@oneauthen.in.th");
								ConfigUtil.MAIL_EXD + "," + mapInfo.get("emailALTN"));
					} else {// every date
						new MailNotificationUtil().sendEmailEXP(
								ConfigUtil.MAIL_SUBJECT, contentEmail
										.replace("[MONTH]", month_array[i].replace("D", "")).replace("[UNIT]", "วัน"),
								//"chaichana.si@oneAuthen.in.th,thanonphat.su@oneauthen.in.th");
								ConfigUtil.MAIL_EXD + "," + mapInfo.get("emailALTN"));
					}
				}
			}
		}
	}

	public static HashMap<String, String> getInfoCertificate(String x509) throws CertificateException {
		X509CertificateUtility certUtil = new X509CertificateUtility(x509);
		Collection<List<?>> altNames = certUtil.getSumjectAltName();

		String emailALTN = "";
		String cnALTN = "";
		for (List<?> item : altNames) {
			Integer type = (Integer) item.get(0);
			if (type == 1)
				emailALTN = (String) item.get(1);
			if (type == 4)
				cnALTN = (String) item.get(1);
		}
		HashMap<String, String> response = new HashMap<>();
		response.put("emailALTN", emailALTN);
		response.put("cn", certUtil.getCommonName());
		return response;

	}

	public static List<String> updateRequest() {
		RequestCertDAO reqDao = new RequestCertDAO();
		List<RequestCertDB> ListReq = reqDao.QueryForUpdateStatus();
		List<String> uniqueList = new ArrayList<String>();
		
		
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();
		calendar.add(Calendar.DATE, 1);
		Date tomorrow = calendar.getTime();

		if (ListReq == null) {

		} else {
			for (RequestCertDB req : ListReq)
				uniqueList.add(req.getUniqueId());
		}

		try {
			GenericPKIAdapter adapter = PKIConnectorLoader.adapterLoader();
			if (adapter == null)
				throw new Exception("Error load PKI connector.");

			List<HashMap<String, String>> listObj = adapter.serviceUpdateReq(uniqueList);
			if (listObj == null || listObj.size() == 0) {
				// throw new Exception("empty update request.");
			} else {
				// System.out.println("Query listObj size REQ = " + listObj.size());
				for (int i = 0; i < listObj.size(); i++) {
					HashMap<String, String> certObj = listObj.get(i);
					RequestCertDB cert = reqDao.findByUniqueID(certObj.get(GenericPKIAdapter.UNIQUE_ID));
					cert.setCertSn(certObj.get(GenericPKIAdapter.SERIAL_NUMBER));
					cert.setX509(certObj.get(GenericPKIAdapter.BASE64));
					cert.setRevokeReason(certObj.get(GenericPKIAdapter.REVOKE_REASON));
					cert.setStatus(certObj.get(GenericPKIAdapter.STATUS));
					cert.setReject(certObj.get(GenericPKIAdapter.REJECT));

					if (certObj.get(GenericPKIAdapter.APPLY_TIME) != null) {

						// System.out.println(certObj.get(GenericPKIAdapter.APPLY_TIME));
						// System.out.println(certObj.get(GenericPKIAdapter.UNIQUE_ID));
						String dateStr = certObj.get(GenericPKIAdapter.APPLY_TIME);
						Date date1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(dateStr);

						cert.setTimeApprove(date1);
					} else {
						cert.setTimeApprove(null);
					}

					reqDao.serviceSyncDB(cert);
				}

				String email = "";
				String uniqueId = "";
				String x509 = "";
				String certSn = "";
				UserRaDB creator = null;
				String status = "";
				String certType = "";
				String action = "";
				String revokeReason = "";
				Date expiredDate = null;
				String detail = "";
				String reject = "";
				int careTaker = 0;
				List<RequestCertDB> certStatus2 = reqDao.QueryCertStatus2();
				// System.out.println("QueryResultSize = " + ((certStatus2 == null) ? "N/A" :
				// certStatus2.size()));
				if (certStatus2 != null) {
					for (RequestCertDB listCertReq : certStatus2) {
						try {
							if (listCertReq.getEmail() == null)
								email = "-";
							else
								email = listCertReq.getEmail();
							uniqueId = listCertReq.getUniqueId();
							x509 = listCertReq.getX509();
							certSn = listCertReq.getCertSn();
							creator = listCertReq.getCreator();
							status = listCertReq.getStatus();
							certType = listCertReq.getCertType();
							action = listCertReq.getAction();
							detail = listCertReq.getDetail();
							careTaker = listCertReq.getCare_taker();

							if (listCertReq.getReject() == null)
								reject = "-";
							else
								reject = listCertReq.getReject();
							// X509CertificateUtility certUtil = new X509CertificateUtility(x509);
							// expiredDate = certUtil.getCertificate().getNotAfter();

							if (listCertReq.getRevokeReason() == null)
								revokeReason = "-";
							else
								revokeReason = listCertReq.getRevokeReason();

							CertificateDAO certDao = new CertificateDAO();
							CertificateDB cert = certDao.findByUniqueID(uniqueId);
							if (cert == null) {
								CertificateDB update = new CertificateDB();
								update.setCert_Sn(certSn);
								update.setCert_Type(certType);
								update.setCert_x509(x509);
								update.setStatus(status);
								update.setUnique_id(uniqueId);
								update.setCreator(creator);
								update.setDetail(detail);
								update.setCare_taker(careTaker);
								update.setExpiredDate(tomorrow);
								update.setRevokeReason(revokeReason);

								certDao.save(update);
							} else {

								listCertReq.setStatus("finish");
								listCertReq.setUniqueId(listCertReq.getUniqueId());
								reqDao.UpdateStatusByUnique(listCertReq);

								continue;

							}

							listCertReq.setStatus("finish");
							listCertReq.setUniqueId(listCertReq.getUniqueId());
							reqDao.UpdateStatusByUnique(listCertReq);

						} catch (Exception e) {
							e.printStackTrace();
							continue;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return uniqueList;

	}

	public static void destroy() {
		DAO.destroy();
	}
}
