package th.in.oneauthen.test;

import java.security.cert.CertificateException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import th.in.oneauthen.emailObj.MapCertType;
import th.in.oneauthen.object.CertificateDB;
import th.in.oneauthen.object.DAO.CertificateDAO;
import th.in.oneauthen.pki.certificate.X509CertificateUtility;
import th.in.oneauthen.util.ConfigUtil;
import th.in.oneauthen.util2.MailNotificationUtil;

public class CheckEXP {

	
	
	public static void main(String[] args) throws Exception {

		
		String month_config = ConfigUtil.MONTH_EXD; 
		String[] month_array = month_config.split(",");

		DateFormat dateNoti = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));
		DateFormat dateNow = new SimpleDateFormat("yyyy-MM-dd", new Locale("en"));
		String result = dateNow.format(new Date());
		Date dateN = dateNow.parse(result);

		for (int i = 0; i < month_array.length; i++) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(dateN);
			if (month_array[i].contains("M"))
				cal.add(Calendar.MONTH, +Integer.parseInt(month_array[i].replace("M", "")));
			else
				cal.add(Calendar.DATE, +Integer.parseInt(month_array[i].replace("D", "")));

			System.out.println(cal.getTime());

			List<CertificateDB> certList = new CertificateDAO().getCertFromStatus("success");
			for (CertificateDB cert : certList) {

				HashMap<String, String> mapInfo = getInfoCertificate(cert.getCert_x509());

				String contentEmail = ConfigUtil.CHECK_EXD_MSG
						.replace("[USERNAME]", " : " + cert.getCreator().getUserName())
						.replace("[CN]", mapInfo.get("cn"))
						.replace("[CERT_TYPE]", MapCertType.getCertType(cert.getCert_Type()));

				String dateStr = dateNow.format(cert.getExpiredDate());
				// System.out.println(dateStr);
				Calendar calendar = Calendar.getInstance();
				Date dateAP = dateNoti.parse(dateStr);
				calendar.setTime(dateAP);

				if (calendar.compareTo(cal) == 0) {
					// do it
					if (month_array[i].contains("M")) {//every month
						new MailNotificationUtil().sendEmailEXP(ConfigUtil.MAIL_SUBJECT,
								contentEmail.replace("[MONTH]", month_array[i].replace("M", "")).replace("[UNIT]", "เดือน"),
								ConfigUtil.MAIL_EXD + "," + mapInfo.get("emailALTN"));
					} else {//every date
						new MailNotificationUtil().sendEmailEXP(ConfigUtil.MAIL_SUBJECT,
								contentEmail.replace("[MONTH]", month_array[i].replace("D", "")).replace("[UNIT]", "วัน"),
								ConfigUtil.MAIL_EXD + "," + mapInfo.get("emailALTN"));
					}
				}
			}
		}
	}

	public static HashMap<String, String> getInfoCertificate(String x509) throws CertificateException {
		X509CertificateUtility certUtil = new X509CertificateUtility(x509);
		Collection<List<?>> altNames = certUtil.getSumjectAltName();

		String emailALTN = "";
		String cnALTN = "";
		for (List<?> item : altNames) {
			Integer type = (Integer) item.get(0);
			if (type == 1)
				emailALTN = (String) item.get(1);
			if (type == 4)
				cnALTN = (String) item.get(1);
		}
		HashMap<String, String> response = new HashMap<>();
		response.put("emailALTN", emailALTN);
		response.put("cn", certUtil.getCommonName());
		return response;

	}
}
