package th.in.oneauthen.test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import th.in.oneauthen.object.CertificateDB;
import th.in.oneauthen.object.RequestCertDB;
import th.in.oneauthen.object.UserRaDB;
import th.in.oneauthen.object.DAO.CertificateDAO;
import th.in.oneauthen.object.DAO.RequestCertDAO;
import th.in.oneauthen.pki.GenericPKIAdapter;
import th.in.oneauthen.pki.PKIConnectorLoader;

public class UpdateRequest {

	public static void main(String[] args) {
		check();
	}
	
	
	
	public static List<String> check() {
		RequestCertDAO reqDao = new RequestCertDAO();
		List<RequestCertDB> ListReq = reqDao.QueryForUpdateStatus();
		List<String> certSnList = new ArrayList<String>();

		if (ListReq == null) {

		} else {
			for (RequestCertDB req : ListReq)
				certSnList.add(req.getUniqueId());
		}

		try {
			GenericPKIAdapter adapter = PKIConnectorLoader.adapterLoader();
			if (adapter == null)
				throw new Exception("Error load PKI connector.");

			List<HashMap<String, String>> listObj = adapter.serviceUpdateReq(certSnList);
			if (listObj == null || listObj.size() == 0) {
				// throw new Exception("empty update request.");
			} else {
				// System.out.println("Query listObj size REQ = " + listObj.size());
				for (int i = 0; i < listObj.size(); i++) {
					HashMap<String, String> certObj = listObj.get(i);
					RequestCertDB cert = reqDao.findByUniqueID(certObj.get(GenericPKIAdapter.UNIQUE_ID));
					cert.setCertSn(certObj.get(GenericPKIAdapter.SERIAL_NUMBER));
					cert.setX509(certObj.get(GenericPKIAdapter.BASE64));
					cert.setRevokeReason(certObj.get(GenericPKIAdapter.REVOKE_REASON));
					cert.setStatus(certObj.get(GenericPKIAdapter.STATUS));
					cert.setReject(certObj.get(GenericPKIAdapter.REJECT));

					if (certObj.get(GenericPKIAdapter.APPLY_TIME) != null) {

						// System.out.println(certObj.get(GenericPKIAdapter.APPLY_TIME));
						// System.out.println(certObj.get(GenericPKIAdapter.UNIQUE_ID));
						String dateStr = certObj.get(GenericPKIAdapter.APPLY_TIME);
						Date date1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(dateStr);

						cert.setTimeApprove(date1);
					} else {
						cert.setTimeApprove(null);
					}

					reqDao.serviceSyncDB(cert);
				}

				String email = "";
				String uniqueId = "";
				String x509 = "";
				String certSn = "";
				UserRaDB creator = null;
				String status = "";
				String certType = "";
				String action = "";
				String revokeReason = "";
				Date expiredDate = null;
				String detail = "";
				String reject = "";
				int careTaker = 0;
				List<RequestCertDB> certStatus2 = reqDao.QueryCertStatus2();
				// System.out.println("QueryResultSize = " + ((certStatus2 == null) ? "N/A" :
				// certStatus2.size()));
				if (certStatus2 != null) {
					for (RequestCertDB listCertReq : certStatus2) {
						try {
							if (listCertReq.getEmail() == null)
								email = "-";
							else
								email = listCertReq.getEmail();
							uniqueId = listCertReq.getUniqueId();
							x509 = listCertReq.getX509();
							certSn = listCertReq.getCertSn();
							creator = listCertReq.getCreator();
							status = listCertReq.getStatus();
							certType = listCertReq.getCertType();
							action = listCertReq.getAction();
							detail = listCertReq.getDetail();
							careTaker = listCertReq.getCare_taker();

							if (listCertReq.getReject() == null)
								reject = "-";
							else
								reject = listCertReq.getReject();
							// X509CertificateUtility certUtil = new X509CertificateUtility(x509);
							// expiredDate = certUtil.getCertificate().getNotAfter();

							if (listCertReq.getRevokeReason() == null)
								revokeReason = "-";
							else
								revokeReason = listCertReq.getRevokeReason();

							CertificateDAO certDao = new CertificateDAO();
							CertificateDB cert = certDao.findByUniqueID(uniqueId);
							if (cert == null) {
								CertificateDB update = new CertificateDB();
								update.setCert_Sn(certSn);
								update.setCert_Type(certType);
								update.setCert_x509(x509);
								update.setStatus(status);
								update.setUnique_id(uniqueId);
								update.setCreator(creator);
								update.setDetail(detail);
								update.setCare_taker(careTaker);
								// update.setExpiredDate(expiredDate);
								update.setExpiredDate(new Date());
								update.setRevokeReason(revokeReason);

								certDao.save(update);
							} else {

								listCertReq.setStatus("finish");
								listCertReq.setUniqueId(listCertReq.getUniqueId());
								reqDao.UpdateStatusByUnique(listCertReq);

								continue;

							}

							listCertReq.setStatus("finish");
							listCertReq.setUniqueId(listCertReq.getUniqueId());
							reqDao.UpdateStatusByUnique(listCertReq);

						} catch (Exception e) {
							e.printStackTrace();
							continue;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return certSnList;

	}
}
