package th.in.oneauthen;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.asn1.x500.style.BCStrictStyle;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import th.in.oneauthen.object.CertTypeDB;
import th.in.oneauthen.object.DAO.CertTypesDAO;
import th.in.oneauthen.pki.impl.CHTRA_RequestXML;
import th.in.oneauthen.xmlObject.SubjectDNDataAttribute;

public class GenericXmlRequestGenerator_Prototype {
	private static final BCStrictStyle BCST = new BCStrictStyle();
	
	public static String GET_PARAM_OID (String dnParam) {
		return BCST.attrNameToOID(dnParam).getId();
	}
	private String certType = "";
//	private String certTypeXML = "";
	private String certTypeXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + "<CertType>\r\n"
			+ "	<CertTypeFriendlyName>ใบรับรองสำหรับ เจ้าหน้าที่นิติบุคคล 1 ปี</CertTypeFriendlyName>\r\n"
			+ "	<CertTypeNumber>10030</CertTypeNumber>\r\n" + "	<ForceToken>1</ForceToken>\r\n"
			+ "	<EmailVerify>1</EmailVerify>\r\n" + "	<KeyUsage>digitalSignature|contentCommitment</KeyUsage>\r\n"
			+ "	<SubjectDN>\r\n" + "		<SubjectDN_DataCount>9</SubjectDN_DataCount>\r\n"
			+ "		<SubjectDN_Data1>c</SubjectDN_Data1>\r\n"
			+ "		<SubjectDN_DefaultValue1>TH</SubjectDN_DefaultValue1>\r\n"
			+ "		<SubjectDN_FriendlyName1>ประเทศ</SubjectDN_FriendlyName1>\r\n"
			+ "		<SubjectDN_Data2>o</SubjectDN_Data2>\r\n"
			+ "		<SubjectDN_FriendlyName2>องค์กร</SubjectDN_FriendlyName2>\r\n"
			+ "		<SubjectDN_DefaultValue2></SubjectDN_DefaultValue2>\r\n"
			+ "		<SubjectDN_Data3>t</SubjectDN_Data3>\r\n"
			+ "		<SubjectDN_FriendlyName3>ตำแหน่ง</SubjectDN_FriendlyName3>\r\n"
			+ "		<SubjectDN_DefaultValue3></SubjectDN_DefaultValue3>\r\n"
			+ "		<SubjectDN_Data4>ou</SubjectDN_Data4>\r\n"
			+ "		<SubjectDN_FriendlyName4>แผนก / ฝ่าย</SubjectDN_FriendlyName4>\r\n"
			+ "		<SubjectDN_DefaultValue4></SubjectDN_DefaultValue4>\r\n"
			+ "		<SubjectDN_Data5>cn</SubjectDN_Data5>\r\n"
			+ "		<SubjectDN_FriendlyName5>ชื่อองค์กร (ภาษาไทย)</SubjectDN_FriendlyName5>\r\n"
			+ "		<SubjectDN_DefaultValue5></SubjectDN_DefaultValue5>\r\n"
			+ "		<SubjectDN_Data6>surname</SubjectDN_Data6>\r\n"
			+ "		<SubjectDN_FriendlyName6>นามสกุลภาษาอังกฤษ</SubjectDN_FriendlyName6>\r\n"
			+ "		<SubjectDN_DefaultValue6></SubjectDN_DefaultValue6>\r\n"
			+ "		<SubjectDN_Data7>givenname</SubjectDN_Data7>\r\n"
			+ "		<SubjectDN_FriendlyName7>ชื่อภาษาอังกฤษ</SubjectDN_FriendlyName7>\r\n"
			+ "		<SubjectDN_DefaultValue7></SubjectDN_DefaultValue7>\r\n"
			+ "		<SubjectDN_Data8>serialnumber</SubjectDN_Data8>\r\n"
			+ "		<SubjectDN_FriendlyName8>หมายเลขบัตรประชาชน</SubjectDN_FriendlyName8>\r\n"
			+ "		<SubjectDN_DefaultValue8></SubjectDN_DefaultValue8>\r\n"
			+ "		<SubjectDN_Data9>organizationidentifier</SubjectDN_Data9>\r\n"
			+ "		<SubjectDN_FriendlyName9>หมายเลขประจำตัวผู้เสียภาษีอากร</SubjectDN_FriendlyName9>\r\n"
			+ "		<SubjectDN_DefaultValue9></SubjectDN_DefaultValue9>\r\n" + "	</SubjectDN>\r\n"
			+ "	<Extension>\r\n" + "		<Extension_DataCount>1</Extension_DataCount>\r\n"
			+ "		<Extension_Data1>2.5.29.17</Extension_Data1>\r\n"
			+ "		<Extension_FriendlyName1>อีเมลสำหรับติดต่อนิติบุคคล</Extension_FriendlyName1>\r\n"
			+ "		<Extension_DefaultValue1></Extension_DefaultValue1>\r\n" + "	</Extension>\r\n" + "</CertType>";

	 public GenericXmlRequestGenerator_Prototype(String certTypeID) {
		 try {
//			 CertTypesDAO certTypeDAO = new CertTypesDAO();
//			 List<CertTypeDB> certTypeList = certTypeDAO.getDefineXML(certTypeID);
//			 if (certTypeList != null) {
//				 CertTypeDB certType = certTypeList.get(0);
//				 this.certTypeXML = certType.getDefineXML();
				 this.certType = certTypeID;
//			 }
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
	 }
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

	public ArrayList<Entry<String, SubjectDNDataAttribute>> parseCertType() throws Exception {
		ArrayList<Entry<String, SubjectDNDataAttribute>> requestParam = new ArrayList<>();

		ByteArrayInputStream input = new ByteArrayInputStream(this.certTypeXML.toString().getBytes("UTF-8"));

		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(input);
		NodeList nList = doc.getElementsByTagName("SubjectDN_DataCount");
		int dnDataCount = Integer.parseInt(nList.item(0).getFirstChild().getNodeValue());
		for (int i = 1; i <= dnDataCount; i++) {
			NodeList node = doc.getElementsByTagName("SubjectDN_Data" + i);
			String nodeName = "";
			if (node != null) {
				nodeName = node.item(0).getFirstChild().getNodeValue();
			} else {
				continue;
			}

			SubjectDNDataAttribute dnData = new SubjectDNDataAttribute();
			if (StringUtils.isNotEmpty(nodeName)) {
				dnData.setName(nodeName);
				if (StringUtils.isNotEmpty(dnData.getOid())) {
					dnData.setXmlDataIndex("" + i);
					Node defValNode = doc.getElementsByTagName("SubjectDN_DefaultValue" + i).item(0).getFirstChild();
					if (defValNode != null) {
						dnData.setDefaultVal(defValNode.getNodeValue());
					}
					requestParam.add(new AbstractMap.SimpleEntry<>(dnData.getOid(), dnData));
				}
			}
		}
		return requestParam;
	}

	public String generateRequest(String action, String uniqueId, String csr,
			HashMap<String, String> nameParams) throws Exception {
		CHTRA_RequestXML requestXML = new CHTRA_RequestXML();
		requestXML.setAction(action);
		requestXML.setCertType(this.certType);
		requestXML.setUserID(uniqueId);
		requestXML.setOperator("[Operator]");
		requestXML.setCSR(csr);

		ArrayList<Entry<String, SubjectDNDataAttribute>> dnAttrs = this.parseCertType();
		for (Entry<String, SubjectDNDataAttribute> attr : dnAttrs) {
			SubjectDNDataAttribute dnTemplate = attr.getValue();
			String param = "";
			switch (dnTemplate.getXmlDataIndex()) {
			case "1":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data1(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data1(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "2":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data2(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data2(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "3":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data3(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data3(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "4":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data4(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data4(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "5":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data5(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data5(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "6":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data6(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data6(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "7":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data7(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data7(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "8":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data8(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data8(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "9":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data9(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data9(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			default:
				break;
			}
		}
		
		String result = "";
		try {

			JAXBContext jaxbContext = JAXBContext.newInstance(CHTRA_RequestXML.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(requestXML, sw);
			result = sw.toString();

			result = result.replaceAll(" standalone=\"yes\"", "");
			result = result.replaceAll("  ", "");
			result = result.replaceAll("\n", "");
			result = result.replaceAll("\r", "");

			int a = result.indexOf(">");
			result = result.substring(a + 1, result.length());
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		
		return result;
	}

	public static void main(String[] args) throws Exception {
		GenericXmlRequestGenerator_Prototype xmlReq = new GenericXmlRequestGenerator_Prototype("10030");
		HashMap<String, String> dnParam = new HashMap<>();
		dnParam.put(GET_PARAM_OID("CN"), "ภราดร อธิจิตสกุล");
		dnParam.put(GET_PARAM_OID("T"), "CTO");
		dnParam.put(GET_PARAM_OID("organizationidentifier"), "1234567890123");

		String request = xmlReq.generateRequest("apply", "2020/CA05-12345", "ABCD1234==", dnParam);
		System.out.println(request);
	}
}
