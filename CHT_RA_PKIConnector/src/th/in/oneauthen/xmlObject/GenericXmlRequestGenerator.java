package th.in.oneauthen.xmlObject;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.asn1.x500.style.BCStrictStyle;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import th.in.oneauthen.object.CertTypeDB;
import th.in.oneauthen.object.DAO.CertTypesDAO;
import th.in.oneauthen.pki.impl.CHTRA_RequestXML;
import th.in.oneauthen.xmlObject.SubjectDNDataAttribute;

public class GenericXmlRequestGenerator {
	private static final BCStrictStyle BCST = new BCStrictStyle();
	
	public static String GET_PARAM_OID (String dnParam) {
		return BCST.attrNameToOID(dnParam).getId();
	}
	private String certType = "";
	private String certTypeXML = "";

	 public GenericXmlRequestGenerator(String certTypeID) {
		 try {
			 CertTypesDAO certTypeDAO = new CertTypesDAO();
			 CertTypeDB certTypeList = certTypeDAO.searchByCertTypeID(certTypeID);
			 if (certTypeList != null) {
				 CertTypeDB certType = certTypeList;
				 this.certTypeXML = certType.getDefineXML();
				 this.certType = certTypeID;
			 }
		 }catch (Exception e) {
			 e.printStackTrace();
		 }
	 }
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

	public ArrayList<Entry<String, SubjectDNDataAttribute>> parseCertType() throws Exception {
		ArrayList<Entry<String, SubjectDNDataAttribute>> requestParam = new ArrayList<>();

		ByteArrayInputStream input = new ByteArrayInputStream(this.certTypeXML.toString().getBytes("UTF-8"));

		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(input);
		NodeList nList = doc.getElementsByTagName("SubjectDN_DataCount");
		int dnDataCount = Integer.parseInt(nList.item(0).getFirstChild().getNodeValue());
		for (int i = 1; i <= dnDataCount; i++) {
			NodeList node = doc.getElementsByTagName("SubjectDN_Data" + i);
			String nodeName = "";
			if (node != null) {
				nodeName = node.item(0).getFirstChild().getNodeValue();
			} else {
				continue;
			}

			SubjectDNDataAttribute dnData = new SubjectDNDataAttribute();
			if (StringUtils.isNotEmpty(nodeName)) {
				dnData.setName(nodeName);
				if (StringUtils.isNotEmpty(dnData.getOid())) {
					dnData.setXmlDataIndex("" + i);
					Node defValNode = doc.getElementsByTagName("SubjectDN_DefaultValue" + i).item(0).getFirstChild();
					if (defValNode != null) {
						dnData.setDefaultVal(defValNode.getNodeValue());
					}
					requestParam.add(new AbstractMap.SimpleEntry<>(dnData.getOid(), dnData));
				}
			}
		}
		return requestParam;
	}

	public String generateRequest(String action, String uniqueId, String csr,
			HashMap<String, String> nameParams, String extEmail, String extCn) throws Exception {
		CHTRA_RequestXML requestXML = new CHTRA_RequestXML();
		requestXML.setAction(action);
		requestXML.setCertType(this.certType);
		requestXML.setUserID(uniqueId);
		requestXML.setOperator("[Operator]");
		requestXML.setCSR(csr);
		
		if (StringUtils.isNotEmpty(extEmail)) {
			String extData = extEmail;
			if (StringUtils.isNotEmpty(extCn)) {
				extData = extData.concat(";;dn:CN=").concat(extCn);
			}
			requestXML.setExtension_Data1(extData);
		}

		ArrayList<Entry<String, SubjectDNDataAttribute>> dnAttrs = this.parseCertType();
		for (Entry<String, SubjectDNDataAttribute> attr : dnAttrs) {
			SubjectDNDataAttribute dnTemplate = attr.getValue();
			String param = "";
			switch (dnTemplate.getXmlDataIndex()) {
			case "1":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data1(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data1(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "2":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data2(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data2(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "3":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data3(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data3(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "4":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data4(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data4(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "5":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data5(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data5(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "6":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data6(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data6(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "7":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data7(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data7(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "8":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data8(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data8(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			case "9":
				param = nameParams.get(dnTemplate.getOid());
				if (dnTemplate.hasDefaultVal()) {
					requestXML.setSubjectDN_Data9(dnTemplate.getDefaultVal());
				} else if (StringUtils.isNotEmpty(param)) {
					requestXML.setSubjectDN_Data9(param);
				} else {
					System.out.println(" Skip dn param " + dnTemplate.getXmlDataIndex() + "; " + dnTemplate.getName()
							+ "[" + dnTemplate.getOid() + "]");
				}
				break;
			default:
				break;
			}
		}
		
		String result = "";
		try {

			JAXBContext jaxbContext = JAXBContext.newInstance(CHTRA_RequestXML.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(requestXML, sw);
			result = sw.toString();

			result = result.replaceAll(" standalone=\"yes\"", "");
			result = result.replaceAll("  ", "");
			result = result.replaceAll("\n", "");
			result = result.replaceAll("\r", "");

			int a = result.indexOf(">");
			result = result.substring(a + 1, result.length());
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		
		return result;
	}
}
