package th.in.oneauthen.xmlObject;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.asn1.x500.style.BCStrictStyle;

public class SubjectDNDataAttribute {
	private String name;
	private String friendlyName;
	private String defaultVal;
	private String xmlDataIndex;
	
	final BCStrictStyle bcst = new BCStrictStyle();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOid() {
		return  bcst.attrNameToOID(this.name).getId();
	}
	public String getFriendlyName() {
		return friendlyName;
	}
	public void setFriendlyName(String friendlyName) {
		this.friendlyName = friendlyName;
	}
	public String getDefaultVal() {
		return defaultVal;
	}
	public void setDefaultVal(String defaultVal) {
		this.defaultVal = defaultVal;
	}
	public boolean hasDefaultVal() {
		return StringUtils.isNotEmpty(this.defaultVal);
	}
	public String getXmlDataIndex() {
		return xmlDataIndex;
	}
	public void setXmlDataIndex(String xmlDataIndex) {
		this.xmlDataIndex = xmlDataIndex;
	}
}
