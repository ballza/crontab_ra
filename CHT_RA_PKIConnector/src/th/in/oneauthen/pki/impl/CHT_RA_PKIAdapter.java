package th.in.oneauthen.pki.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.util.encoders.Hex;



import th.in.oneauthen.object.CHTSystemConfigDB;
import th.in.oneauthen.object.CertDataDB;
import th.in.oneauthen.object.CertTypeDB;
import th.in.oneauthen.object.CertificateDB;
import th.in.oneauthen.object.CertRegisterFormsDB;
import th.in.oneauthen.object.DAO.CHTSystemConfigDAO;
import th.in.oneauthen.object.DAO.CertDataDAO;
import th.in.oneauthen.object.DAO.CertRegisterFormDAO;
import th.in.oneauthen.object.DAO.CertificateDAO;
import th.in.oneauthen.object.DAO.CertTypesDAO;
import th.in.oneauthen.pki.CertProfileParameter;
import th.in.oneauthen.pki.GenericPKIAdapter;
import th.in.oneauthen.pki.ModelAccept;
import th.in.oneauthen.pki.certificate.X509CertificateUtility;
import th.in.oneauthen.xmlObject.GenericXmlRequestGenerator;

public class CHT_RA_PKIAdapter implements GenericPKIAdapter {

	@Override
	public HashMap<String, String> getCertificateBySerialNumber(String serialNumber) {
		// TODO Auto-generated method stub
		HashMap<String, String> certInfo = null;
		try {
			CertDataDB cert = new CertDataDAO().findByCertSN(serialNumber);
			if (cert == null)
				throw new Exception("Certificate not found");
			else {
				certInfo = new HashMap<>();
				certInfo.put(SERIAL_NUMBER, cert.getCert_sn());
				certInfo.put(BASE64, cert.getCert_x509base64());
				certInfo.put(STATUS, "" + cert.getCert_status());
				certInfo.put(REVOKE_REASON, "" + cert.getCert_revoke_reason());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return certInfo;
	}

	@Override
	public Collection<CertProfileParameter> listCaSystemCertProfile() {
		ArrayList<CertProfileParameter> certTypeResult = null;

		CertTypesDAO certTypeDAO = new CertTypesDAO();
		try {
			List<CertTypeDB> availableCertType = certTypeDAO.list();

			if (availableCertType != null) {
				certTypeResult = new ArrayList<>();

				for (CertTypeDB certType : availableCertType) {
					if (certType.getDisabled() == null) {
						CertProfileParameter profile = new CertProfileParameter();
						profile.setProfileName(certType.getFriendlyName());
						profile.setProfileId(certType.getCertTypeID());
					}
				}
			}
		} catch (Exception e) {
		}

		return certTypeResult;

	}

	@Override
	public String SaveRequestCert(String userName, String certType, String userId, String userEmail,
			String personal_mail, String userOrganization, String csr, Date registerTime, String CN, String C,
			String cnth, int countCert, String unique_id, String ou, String t, String oid, String l, String sn,
			String g) {
		// TODO Auto-generated method stub
		try {
			CertRegisterFormDAO regisDao = new CertRegisterFormDAO();
			CertRegisterFormsDB regis = new CertRegisterFormsDB();
			String xmlString = "";
			if (certType.equals("10001") || certType.equals("10002"))
				xmlString = new CertApply_API().apply1001and1002("CHTCA3_CertApply", certType, userId, userName, csr, C,
						userOrganization, CN, userId, userEmail, personal_mail, cnth, countCert, unique_id);
			else if (certType.equals("10003") || certType.equals("10004"))
				xmlString = new CertApply_API().apply1003and1004("CHTCA3_CertApply", certType, userId, userName, csr, C,
						userOrganization, CN, userId, userEmail, personal_mail, cnth, countCert, unique_id);
			else if (certType.equals("10005") || certType.equals("10006"))
				xmlString = new CertApply_API().apply1005and1006("CHTCA3_CertApply", certType, userId, userName, csr, C,
						userOrganization, CN, userId, userEmail, personal_mail, cnth, countCert, unique_id, ou, t, oid);
			else if (certType.equals("10007") || certType.equals("10008"))
				xmlString = new CertApply_API().apply1007and1008("CHTCA3_CertApply", certType, userId, userName, csr, C,
						userOrganization, CN, userId, userEmail, personal_mail, cnth, countCert, unique_id, ou, t, oid,
						l);
			else if (certType.equals("10010") || certType.equals("10011"))
				xmlString = new CertApply_API().apply1010and1011("CHTCA3_CertApply", certType, userId, userName, csr, C,
						userOrganization, CN, userId, userEmail, personal_mail, cnth, countCert, unique_id, ou, t, oid);
			else if (certType.equals("10012") || certType.equals("10013"))
				xmlString = new CertApply_API().apply1012and1013("CHTCA3_CertApply", certType, userId, userName, csr, C,
						userOrganization, CN, userId, userEmail, personal_mail, cnth, countCert, unique_id, ou, t, oid);
			else if (certType.equals("10014") || certType.equals("10015"))
				xmlString = new CertApply_API().apply1014and1015("CHTCA3_CertApply", certType, userId, userName, csr, C,
						userOrganization, CN, userId, userEmail, personal_mail, cnth, countCert, unique_id, ou, t, oid);
			else {
				HashMap<String, String> requestParam = new HashMap<>();
				if (StringUtils.isNotEmpty(C)) {
					requestParam.put(GenericXmlRequestGenerator.GET_PARAM_OID("C"), C);
				} 
				
				if (StringUtils.isNotEmpty( cnth )) {
					requestParam.put(GenericXmlRequestGenerator.GET_PARAM_OID("CN"), cnth );
				} 
				
				if (StringUtils.isNotEmpty( g )) {
					requestParam.put(GenericXmlRequestGenerator.GET_PARAM_OID("GIVENNAME"), g );
				} 
				
				if (StringUtils.isNotEmpty( l )) {
					requestParam.put(GenericXmlRequestGenerator.GET_PARAM_OID("L"), l );
				} 
				
				if (StringUtils.isNotEmpty( oid )) {
					requestParam.put(GenericXmlRequestGenerator.GET_PARAM_OID("ORGANIZATIONIDENTIFIER"), oid );
				} 
				
				if (StringUtils.isNotEmpty( userOrganization )) {
					requestParam.put(GenericXmlRequestGenerator.GET_PARAM_OID("O"), userOrganization );
				} 
				
				if (StringUtils.isNotEmpty( ou )) {
					requestParam.put(GenericXmlRequestGenerator.GET_PARAM_OID("OU"), ou );
				} 
				
				if (StringUtils.isNotEmpty( t )) {
					requestParam.put(GenericXmlRequestGenerator.GET_PARAM_OID("T"), t );
				} 
				
				if (StringUtils.isNotEmpty( sn )) {
					requestParam.put(GenericXmlRequestGenerator.GET_PARAM_OID("SURNAME"), sn );
				} 
				
				if (StringUtils.isNotEmpty( l )) {
					requestParam.put(GenericXmlRequestGenerator.GET_PARAM_OID("L"), l );
				} 
				
				GenericXmlRequestGenerator xmlRequest = new GenericXmlRequestGenerator(certType);
				xmlString = xmlRequest.generateRequest("CHTCA3_CertApply", unique_id, csr, 
						requestParam, userEmail, CN);
			}
			

			regis.setUserName(userName);
			// regis.setUserID(userId + "/" + countCert);// w8 ï¿½ï¿½ï¿½
			regis.setUserID(unique_id);
			regis.setUserEmail(personal_mail);
			regis.setUserO(userOrganization);
			regis.setCertReq(csr);
			regis.setStatus(1);
			regis.setRegisterTime(registerTime);
			regis.setRegistXML(xmlString);
			regis.setCertType(certType);

			regisDao.save(regis);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "fail";
		}
		return "success";

	}

	@Override
	public HashMap<String, String> checkCertificateByuniqueID(String unique_id) {

		// TODO Auto-generated method stub
		HashMap<String, String> certInfo = null;
		try {
			CertDataDB cert = new CertDataDAO().getCertsUpdate(unique_id);
			if (cert == null)
				throw new Exception("Certificate not found");
			else {
				certInfo = new HashMap<>();
				certInfo.put(SERIAL_NUMBER, cert.getCert_sn());
				certInfo.put(BASE64, cert.getCert_x509base64());

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return certInfo;
	}

	@Override
	public Date SaveTimeAccep(String unique_id) {
		// TODO Auto-generated method stub
		CertDataDAO accept = new CertDataDAO();

		CertDataDB cert = accept.getCertsUpdate(unique_id);
		if (cert == null) {
			return null;
		}
		cert.setAccept_time(new Date());
		accept.updateAccepCert(cert);

		CertDataDB afterAccept = accept.findByUniqueID(unique_id);
		System.out.println("pass");
		return afterAccept.getAccept_time();

	}

	@Override
	public HashMap<String, String> checkCertificateByuniqueIDRegis(String unique_id) {
		// TODO Auto-generated method stub
		HashMap<String, String> certInfo = null;
		try {
			CertRegisterFormsDB cert = new CertRegisterFormDAO().findByUniqueID(unique_id);
			if (cert == null)
				return null;
			else {
				certInfo = new HashMap<>();
				certInfo.put(SERIAL_NUMBER, cert.getCertSn());
				certInfo.put(BASE64, cert.getCertReq());
				int statusInt = cert.getStatus();
				String StatusStr = Integer.toString(statusInt);
				certInfo.put(STATUS, StatusStr);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return certInfo;
	}

	@Override
	public String SaveRevokeRequest(String userName, String unique_id, int revokeReason, String userEmail,
			String userOrganization, String certSn, String form_status, String action, Date registerTime) {
		// TODO Auto-generated method stub
		try {
			CertRegisterFormDAO regisDao = new CertRegisterFormDAO();
			CertRegisterFormsDB regis = new CertRegisterFormsDB();
			String xmlString = new CertRevoke_API().Revoke(certSn);

			regis.setUserName(userName);
			regis.setUserID(unique_id);// BallzaR5R5
			regis.setRegistXML(xmlString);
			regis.setUserEmail(userEmail);
			regis.setUserO(userOrganization);
			regis.setCertSn(certSn);
			regis.setStatus(1);
			regis.setAct(action);
			regis.setRegisterTime(registerTime);
			String revokeReasonStr = Integer.toString(revokeReason);
			regis.setRevokeReason(revokeReasonStr);

			regisDao.save(regis);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "fail";
		}
		return "success";

	}

	@Override
	public Date syncForRevokeRequest(String unique_id) {
		// TODO Auto-generated method stub
		CertDataDB cert = new CertDataDAO().findByUniqueID(unique_id);
		if (cert == null) {
			return null;
		}
		if (cert.getAccept_time() == null) {
			return null;
		}

		return cert.getAccept_time();
	}

	@Override
	public List<HashMap<String, String>> serviceUpdateReq(List<String> uniqueList) {
		List<HashMap<String, String>> List = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> certInfo = null;
		for (int i = 0; i < uniqueList.size(); i++) {
			CertRegisterFormsDB data = new CertRegisterFormDAO().serviceByUniqueID(uniqueList.get(i));
			if (data == null)
				try {
					// throw new Exception ("Certificate not found");
					System.out.println(uniqueList.get(i) + "  : out of CertRegisterFormsDB");

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			else {
				certInfo = new HashMap<>();
				if (data.getCertSn() == null)
					certInfo.put(SERIAL_NUMBER, "-");
				else
					certInfo.put(SERIAL_NUMBER, data.getCertSn());
				// CertDataDB x509 = new
				// CertDataDAO().findByUniqueID(uniqueList.get(i));
				//
				// if(data.getCertReq()==null && data.getAct().equals("Enroll")
				// )
				// certInfo.put(BASE64, "-");
				// else if(data.getCertReq()!=null &&
				// data.getAct().equals("Enroll") )
				// certInfo.put(BASE64, x509.getCert_x509base64());
				// else
				// certInfo.put(BASE64, "-");
				if (data.getCertReq() == null)
					certInfo.put(BASE64, "-");
				else
					certInfo.put(BASE64, data.getCertReq());

				if (certInfo.put(REVOKE_REASON, data.getRevokeReason()) == null)
					certInfo.put(REVOKE_REASON, "-");
				else
					certInfo.put(REVOKE_REASON, data.getRevokeReason());
				certInfo.put(STATUS, "" + data.getStatus());
				certInfo.put(UNIQUE_ID, "" + data.getUserID());

				if (data.getRejectMessage() == null)
					certInfo.put(REJECT, "-");
				else
					certInfo.put(REJECT, data.getRejectMessage());

				if (data.getApplyTime() == null)
					certInfo.put(APPLY_TIME, null);
				else
					certInfo.put(APPLY_TIME, data.getApplyTime().toString());

				List.add(certInfo);
			}

		}
		// TODO Auto-generated method stub
		return List;
	}

	@Override
	public List<HashMap<String, String>> serviceUpdateCertData(List<String> uniqueList) {
		// TODO Auto-generated method stub
		List<HashMap<String, String>> List = new ArrayList<HashMap<String, String>>();
		
		
		uniqueList.forEach(item->{
			CertDataDB data = new CertDataDAO().findByCertSN(item);
			HashMap<String, String> certInfo = new HashMap<>();
			if (data == null) {
				System.out.println(item);
				return;
			}
			else {
				if(StringUtils.isEmpty(data.getCert_sn())) 
					certInfo.put(SERIAL_NUMBER, "-");
				else
					certInfo.put(SERIAL_NUMBER, data.getCert_sn());
				if(StringUtils.isEmpty(data.getCert_x509base64())) 
					certInfo.put(BASE64, "-");
				else
					certInfo.put(BASE64, data.getCert_x509base64());
				if (data.getCert_revoke_reason() == null)
					certInfo.put(REVOKE_REASON, "-");
				else
					certInfo.put(REVOKE_REASON, "" + data.getCert_revoke_reason()); 
				certInfo.put(STATUS, "" + data.getCert_status());
				certInfo.put(UNIQUE_ID, "" + data.getUniqueID());
				List.add(certInfo);
						
			}
		});
		
		return List;
	}

	@Override
	public Date checkToken(String certSN, String token) {
		// TODO Auto-generated method stub

		// MessageDigest messageDigest = null;
		// try {
		// messageDigest = MessageDigest.getInstance("SHA-256");
		// } catch (NoSuchAlgorithmException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		List<CertRegisterFormsDB> listCert = new CertRegisterFormDAO().queyCertSN(certSN);
		if (listCert == null || listCert.size() == 0) {
			System.out.println("ERROR certSN invalid in CertRegisterFormsDB");
			return null;
		}
		CertDataDAO accept = new CertDataDAO();
		for (CertRegisterFormsDB cert : listCert) {

			cert.getCert_register_formsId();

			String formsIDstr = Integer.toString(cert.getCert_register_formsId());
			MessageDigest messageDigest = null;
			try {
				messageDigest = MessageDigest.getInstance("SHA-256");
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// messageDigest.update(formsIDstr.getBytes());
			// String encryptedString = new String(messageDigest.digest());
			messageDigest.update(formsIDstr.getBytes());
			byte[] byteMd = messageDigest.digest();
			String encryptedString = new String(byteMd);

			System.out.println(encryptedString);

			encryptedString = Hex.toHexString(byteMd);
			System.out.println(encryptedString);

			encryptedString = encryptedString.substring(5, 15);
			if (encryptedString.toLowerCase().equals(token.toLowerCase())) {

				// CertDataDAO accept = new CertDataDAO();
				CertDataDB update = accept.findByCertSN(certSN);
				if (update == null) {
					System.out.println("ERROR certSN invalid in CertDataDB");
					return null;
				} else {
					if (update.getAccept_time() == null) {
						update.setAccept_time(new Date());
						accept.updateAccepCert(update);
					} else {
						System.out.println("============================================================");
						System.out.println("ERROR certSN accepted");
						return null;
					}
				}
			} else {
				continue;
			}
		}
		CertDataDB afterAccept = accept.findByCertSN(certSN);

		return afterAccept.getAccept_time();

	}

	@Override
	public X509CertificateUtility checkAndResponseInfo(String certSN, String token) {
		// TODO Auto-generated method stub

		List<CertRegisterFormsDB> listCert = new CertRegisterFormDAO().queyCertSN(certSN);
		if (listCert == null || listCert.size() == 0) {
			System.out.println("ERROR certSN invalid in CertRegisterFormsDB");
			return null;
		}
		CertDataDAO accept = new CertDataDAO();
		X509CertificateUtility certUtil = null;

		System.out.println("Token = " + token);

		for (CertRegisterFormsDB cert : listCert) {

			cert.getCert_register_formsId();
			String formsIDstr = Integer.toString(cert.getCert_register_formsId());
			MessageDigest messageDigest = null;
			try {
				messageDigest = MessageDigest.getInstance("SHA-256");
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			messageDigest.update(formsIDstr.getBytes());
			byte[] byteMd = messageDigest.digest();
			String encryptedString = new String(byteMd);

			System.out.println(encryptedString);
			encryptedString = Hex.toHexString(byteMd);
			System.out.println(encryptedString);

			encryptedString = encryptedString.substring(5, 15);
			if (encryptedString.toLowerCase().equals(token.toLowerCase())) {
				CertDataDB detailCert = accept.findByCertSN(certSN);
				if (detailCert == null) {
					System.out.println("ERROR certSN invalid in CertDataDB");
					return null;
				} else {
					try {
						certUtil = new X509CertificateUtility(detailCert.getCert_x509base64());
						System.out.println("=======================================================================");
						System.out.println(certUtil.getCertificateSubjectDN());
					} catch (CertificateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			} else {
				continue;
			}
		}

		return certUtil;
	}

	@Override
	public Date unAccept(String certSN) {
		// TODO Auto-generated method stub
		CertDataDAO unAcceptDao = new CertDataDAO();
		CertDataDB unAccept = unAcceptDao.findByCertSN(certSN);
		if (unAccept == null) {
			return null;
		}

		// check unaccepted
		// if(unAccept.getCert_status() == 5){
		// return null;
		// }

		if (unAccept.getAccept_time() != null) {
			return null;
		}

		unAccept.setAccept_time(new Date());
		// unAccept.setCert_status(5);

		unAcceptDao.updateUnAccept(unAccept);
		CertDataDB afterUnAccept = unAcceptDao.findByCertSN(certSN);

		return afterUnAccept.getAccept_time();
	}

	@Override
	public ModelAccept newAccept(String certSN, String token) {
		// TODO Auto-generated method stub

		ModelAccept response = new ModelAccept();

		List<CertRegisterFormsDB> listCert = new CertRegisterFormDAO().queyCertSN(certSN);
		if (listCert == null || listCert.size() == 0) {
			System.out.println("ERROR certSN invalid in CertRegisterFormsDB");

			response.setDate("ERROR");
			response.setDetail("ERROR certSN invalid in CertRegisterFormsDB");
			return response;
		}
		CertDataDAO accept = new CertDataDAO();
		for (CertRegisterFormsDB cert : listCert) {

			cert.getCert_register_formsId();

			String formsIDstr = Integer.toString(cert.getCert_register_formsId());
			MessageDigest messageDigest = null;
			try {
				messageDigest = MessageDigest.getInstance("SHA-256");
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// messageDigest.update(formsIDstr.getBytes());
			// String encryptedString = new String(messageDigest.digest());
			messageDigest.update(formsIDstr.getBytes());
			byte[] byteMd = messageDigest.digest();
			String encryptedString = new String(byteMd);

			System.out.println(encryptedString);

			encryptedString = Hex.toHexString(byteMd);
			System.out.println(encryptedString);

			encryptedString = encryptedString.substring(5, 15);
			if (encryptedString.toLowerCase().equals(token.toLowerCase())) {

				// CertDataDAO accept = new CertDataDAO();
				CertDataDB update = accept.findByCertSN(certSN);
				if (update == null) {
					System.out.println("ERROR certSN invalid in CertDataDB");

					response.setDate("ERROR");
					response.setDetail("ERROR certSN invalid in CertDataDB");
					return response;
				} else {
					if (update.getAccept_time() == null) {
						update.setAccept_time(new Date());
						accept.updateAccepCert(update);
					} else {
						System.out.println("============================================================");
						System.out.println("ERROR certSN accepted");

						DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						String strDate = dateFormat.format(update.getAccept_time());

						response.setDate(strDate);
						response.setDetail("ERROR certSN accepted");
						return response;
					}
				}
			} else {
				continue;
			}
		}
		CertDataDB afterAccept = accept.findByCertSN(certSN);

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String strDate = dateFormat.format(afterAccept.getAccept_time());

		response.setDate(strDate);
		response.setDetail("SUCCESS");

		return response;

	}

	@Override
	public List<HashMap<String, String>> check10Accept(List<HashMap<String, String>> listMap) {
		// TODO Auto-generated method stub

		CertRegisterFormDAO certDAO = new CertRegisterFormDAO();
		List<HashMap<String, String>> certList = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> certDetail = null;

		for (HashMap<String, String> certInfo : listMap) {

			if (certInfo.get(GenericPKIAdapter.STATUS).equals("5")) {
				CertRegisterFormsDB cert = certDAO.serviceByUniqueID(certInfo.get(GenericPKIAdapter.UNIQUE_ID));
				if (cert == null)
					continue;
				certDetail = new HashMap<>();
				CertDataDB certData = new CertDataDAO().findByCertSN(cert.getCertSn());
				if (certData == null)
					continue;

				certDetail.put(GenericPKIAdapter.APPLY_TIME, certData.getAccept_time() + "");
				certDetail.put(GenericPKIAdapter.EMAIL, cert.getUserEmail());
				certDetail.put(GenericPKIAdapter.SERIAL_NUMBER, cert.getCertSn());
				certDetail.put(GenericPKIAdapter.USERNAME, cert.getUserName());
				certDetail.put(GenericPKIAdapter.STATUS, "5");

				certList.add(certDetail);

			} else {
				CertRegisterFormsDB cert = certDAO.serviceByUniqueID(certInfo.get(GenericPKIAdapter.UNIQUE_ID));
				if (cert == null)
					continue;
				certDetail = new HashMap<>();
				certDetail.put(GenericPKIAdapter.APPLY_TIME, cert.getApplyTime() + "");
				certDetail.put(GenericPKIAdapter.EMAIL, cert.getUserEmail());
				certDetail.put(GenericPKIAdapter.SERIAL_NUMBER, cert.getCertSn());
				certDetail.put(GenericPKIAdapter.USERNAME, cert.getUserName());
				certDetail.put(GenericPKIAdapter.STATUS, cert.getStatus() + "");

				certList.add(certDetail);

			}

		}

		return certList;
	}

	@Override
	public String getMailConfig(String keyName) {
		// TODO Auto-generated method stub
		CHTSystemConfigDAO config = new CHTSystemConfigDAO();
		CHTSystemConfigDB data = config.getContents(keyName);
		return data.getConfig_contents();
	}

	@Override
	public int getFormID(String uniqueID) {
		// TODO Auto-generated method stub
		CertRegisterFormDAO cert = new CertRegisterFormDAO();
		CertRegisterFormsDB certInfo = cert.findByUniqueID(uniqueID);

		return certInfo.getCert_register_formsId();
	}

	@Override
	public String EditCSR(String uniqueID, String CSRB64) {
		// TODO Auto-generated method stub
		try {
			CertRegisterFormDAO editDao = new CertRegisterFormDAO();
			CertRegisterFormsDB edit = editDao.serviceByUniqueID(uniqueID);
			  String xmlStr = edit.getRegistXML();
			  String stripped = xmlStr.replaceAll("(<CSR.*?>.*?<\\/CSR>)|(<CSR.*?>.*?<\\/CSR>)", "<CSR>"+CSRB64+"</CSR>");
			  edit.setCertReq(CSRB64);
			  edit.setRegistXML(stripped);
			  editDao.insertCSR(edit);
			return "success";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "fail";
		}

	}

	@Override
	public Boolean deleteRequest(String uniqueID) {
		// TODO Auto-generated method stub
		try {
			CertRegisterFormDAO rmDao = new CertRegisterFormDAO();
			CertRegisterFormsDB rm = rmDao.serviceByUniqueID(uniqueID);
			if (rm != null) {
				rmDao.remove(rm.getCert_register_formsId());
			}
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

}
