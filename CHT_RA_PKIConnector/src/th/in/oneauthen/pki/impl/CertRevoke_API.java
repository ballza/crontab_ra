package th.in.oneauthen.pki.impl;








import java.io.StringWriter;
import java.net.InetAddress;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class CertRevoke_API {
	public  String Revoke(String certSn ) {

	CHTRA_RequestXML revork = new CHTRA_RequestXML();
	revork.setAction("CHTCA3_CertRevoke");
	revork.setOperator("[Operator]");
	revork.setCertRevoke_Reason("[RevokeReason]");
	revork.setCertRevoke_DataCount("1");
	revork.setCertRevoke_Data1(certSn);
	


	  
	  try {

		JAXBContext jaxbContext = JAXBContext.newInstance(CHTRA_RequestXML.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(revork, sw);
		String xmlString = sw.toString();
		
		String noindent = xmlString.replaceAll(" standalone=\"yes\"","");
		String noindent2 = noindent.replaceAll("  ","");
		String noindent3 = noindent2.replaceAll("\n","");
		String noindent5 = noindent3.replaceAll("\r","");
		
		 int a =  noindent5.indexOf(">");
		 noindent5 = noindent5.substring(a+1, noindent5.length());
		
		System.out.println(noindent5);	
	//	System.out.println(xmlString);	

		
		

		return noindent5;
//        try {
//            InetAddress i = InetAddress.getLocalHost();
//            System.out.println(i);                  // host name and IP address
//            System.out.println(i.getHostName());    // name
//            System.out.println(i.getHostAddress()); // IP address only
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
		

	      } catch (JAXBException e) {
		e.printStackTrace();
	      }
	return"fail";

	}
}

