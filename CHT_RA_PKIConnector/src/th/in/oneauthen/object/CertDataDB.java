package th.in.oneauthen.object;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;



@Entity
@Table(name="certs_data")
@GenericGenerator(name ="idGenerator",strategy="native")
@NamedQueries(value = { 
		@NamedQuery(name="querySelfCerts", query="SELECT u FROM CertDataDB u WHERE u.uniqueID = :personalID and u.cert_status = 0 and u.accept_time is not NULL"),
		@NamedQuery(name="queryCertsAccept", query="SELECT u FROM CertDataDB u WHERE u.uniqueID = :personalIDAccep and u.cert_status = 0 and u.accept_time is NULL"),
		@NamedQuery(name="FindByCertSN", query="SELECT u FROM CertDataDB u WHERE u.cert_sn = :CertSN "),
		@NamedQuery(name="UniqueID", query="SELECT u FROM CertDataDB u WHERE u.uniqueID = :uniqueID "),
	})
public class CertDataDB implements Serializable {


	
	
	@Id
	@GeneratedValue(generator="idGenerator", strategy=GenerationType.AUTO) @Column(name ="cert_id",nullable = false)
	private int cert_id;
	@Column(name = "cert_typeid", nullable = false)
	private String cert_typeid;
	@Column(name = "unique_id", nullable = false)
	private String uniqueID;
	@Column(name = "cert_sn", nullable = false)
	private String cert_sn;
	@Column(name = "cert_status",nullable = false)
	private int cert_status;
	@Column(name = "cert_x509base64")
	private String cert_x509base64;
	@Column(name = "cert_notbefore")
	private Date cert_notbefore;
	@Column(name = "cert_notafter")
	private Date cert_notafter;
	@Column(name = "TID")
	private String TID;
	@Column(name = "last_update",nullable = false)
	private Date last_update;
	@Column(name = "form_id")
	private Integer form_id;
	@Column(name = "cert_revoke_reason")
	private Integer cert_revoke_reason;
	@Column(name = "accept_time")
	private Date accept_time;
	
	public int getCert_id() {
		return cert_id;
	}
	public void setCert_id(int cert_id) {
		this.cert_id = cert_id;
	}
	public String getCert_typeid() {
		return cert_typeid;
	}
	public void setCert_typeid(String cert_typeid) {
		this.cert_typeid = cert_typeid;
	}

	public String getUniqueID() {
		return uniqueID;
	}
	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}
	public String getCert_sn() {
		return cert_sn;
	}
	public void setCert_sn(String cert_sn) {
		this.cert_sn = cert_sn;
	}
	public int getCert_status() {
		return cert_status;
	}
	public void setCert_status(int cert_status) {
		this.cert_status = cert_status;
	}
	public String getCert_x509base64() {
		return cert_x509base64;
	}
	public void setCert_x509base64(String cert_x509base64) {
		this.cert_x509base64 = cert_x509base64;
	}
	public Date getCert_notbefore() {
		return cert_notbefore;
	}
	public void setCert_notbefore(Date cert_notbefore) {
		this.cert_notbefore = cert_notbefore;
	}
	public Date getCert_notafter() {
		return cert_notafter;
	}
	public void setCert_notafter(Date cert_notafter) {
		this.cert_notafter = cert_notafter;
	}
	public String getTID() {
		return TID;
	}
	public void setTID(String tID) {
		TID = tID;
	}
	public Date getLast_update() {
		return last_update;
	}
	public void setLast_update(Date last_update) {
		this.last_update = last_update;
	}
	public int getForm_id() {
		return form_id;
	}
	public void setForm_id(int form_id) {
		this.form_id = form_id;
	}
	public Integer getCert_revoke_reason() {
		return cert_revoke_reason;
	}
	public void setCert_revoke_reason(int cert_revoke_reason) {
		this.cert_revoke_reason = cert_revoke_reason;
	}
	public Date getAccept_time() {
		return accept_time;
	}
	public void setAccept_time(Date accept_time) {
		this.accept_time = accept_time;
	}

	
	
	



}
