package th.in.oneauthen.object;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="cert_types")
@GenericGenerator(name ="idGenerator",strategy="native")
@NamedQueries(value = { 
		@NamedQuery(name="queryCertTypeID", query="SELECT u FROM CertTypeDB u WHERE u.disabled is NULL"),
		@NamedQuery(name="queryDefineXML", query="SELECT u.defineXML FROM CertTypeDB u WHERE u.CertTypeID = :CertTypeID"),
		@NamedQuery(name="searchByCertTypeID", query="SELECT u FROM CertTypeDB u WHERE u.CertTypeID = :certTypeId"),
	})
public class CertTypeDB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4163657688906105011L;
	
	@Id
	@GeneratedValue(generator="idGenerator", strategy=GenerationType.AUTO) @Column(name ="id",nullable = false)
	private int cert_typesId;
	@Column(name = "CertTypeFriendlyName", nullable = false)
	private String friendlyName;
	@Column(name = "CertTypeID", nullable = false)
	private String CertTypeID;
	@Column(name = "HardWareToken",nullable = false)
	@ColumnDefault("0")
	private int hardToken;
	@Column(name = "DefineXML", nullable = false)
	private String defineXML;
	@Column(name = "last_update")
	private Date last_update;
	@Column(name = "disabled")
	private Date disabled;
	@Column(name = "isPersonal")
	@ColumnDefault("0")
	private int isPersonal;
	@Column(name = "KeyUsage")
	private String keyUsage;
	
	public int getCert_typesId() {
		return cert_typesId;
	}
	public void setCert_typesId(int cert_typesId) {
		this.cert_typesId = cert_typesId;
	}
	public String getFriendlyName() {
		return friendlyName;
	}
	public void setFriendlyName(String friendlyName) {
		this.friendlyName = friendlyName;
	}
	public String getCertTypeID() {
		return CertTypeID;
	}
	public void setCertTypeID(String certTypeID) {
		CertTypeID = certTypeID;
	}
	public int getHardToken() {
		return hardToken;
	}
	public void setHardToken(int hardToken) {
		this.hardToken = hardToken;
	}
	public String getDefineXML() {
		return defineXML;
	}
	public void setDefineXML(String defineXML) {
		this.defineXML = defineXML;
	}
	public Date getLast_update() {
		return last_update;
	}
	public void setLast_update(Date last_update) {
		this.last_update = last_update;
	}
	public Date getDisabled() {
		return disabled;
	}
	public void setDisabled(Date disabled) {
		this.disabled = disabled;
	}
	public int getIsPersonal() {
		return isPersonal;
	}
	public void setIsPersonal(int isPersonal) {
		this.isPersonal = isPersonal;
	}
	public String getKeyUsage() {
		return keyUsage;
	}
	public void setKeyUsage(String keyUsage) {
		this.keyUsage = keyUsage;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
