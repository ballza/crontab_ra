package th.in.oneauthen.object.DAO;

import java.util.List;

import org.apache.log4j.Logger;

import th.athichitsakul.dao.impl.StandardDAOImpl;
import th.in.oneauthen.object.CertDataDB;
import th.in.oneauthen.object.UserRaDB;

public class CertDataDAO extends StandardDAOImpl<CertDataDB> {
	// private Logger logger =
	// SystemLogger.generateSystemLogger(certs_dataDAO.class);

	public CertDataDAO() {
		super("generalrao");
		// TODO Auto-generated constructor stub
	}

	public List<CertDataDB> getSelfCerts(String unique_id) {
		List<CertDataDB> historyTotal = null;
		try {

			localDAO.begin();
			historyTotal = (List<CertDataDB>) localDAO.getEntityManager().createNamedQuery("querySelfCerts")
					.setParameter("personalID", unique_id).getResultList();

		} catch (Exception e) {
			// logger.error("Invalid query querySelfCert" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}

	public CertDataDB getCertsUpdate(String unique_id) {
		CertDataDB historyTotal = null;
		try {

			localDAO.begin();
			historyTotal = (CertDataDB) localDAO.getEntityManager().createNamedQuery("queryCertsAccept")
					.setParameter("personalIDAccep", unique_id).getSingleResult();

		} catch (Exception e) {
			// logger.error("Invalid query querySelfCert" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}

	public void updateTimeAccept(CertDataDB updateData) {
		try {
			localDAO.begin();
			CertDataDB db = localDAO.getEntityManager().find(CertDataDB.class, updateData.getCert_id());

			// == do update ==
			db.setAccept_time(updateData.getAccept_time());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for "+updateData.getUserName(),e);
		} finally {
			localDAO.close();
		}

	}

	public CertDataDB findByCertSN(String CertSN) {
		CertDataDB historyTotal = null;
		try {

			localDAO.begin();
			historyTotal = (CertDataDB) localDAO.getEntityManager().createNamedQuery("FindByCertSN")
					.setParameter("CertSN", CertSN).getSingleResult();

		} catch (Exception e) {
			// logger.error("Invalid query querySelfCert" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}

	public CertDataDB findByUniqueID(String uniqueID) {
		CertDataDB historyTotal = null;
		try {

			localDAO.begin();
			historyTotal = (CertDataDB) localDAO.getEntityManager().createNamedQuery("UniqueID")
					.setParameter("uniqueID", uniqueID).getSingleResult();

		} catch (Exception e) {
			// logger.error("Invalid query querySelfCert" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}

	public void updateAccepCert(CertDataDB updateData) {
		try {
			localDAO.begin();
			CertDataDB db = localDAO.getEntityManager().find(CertDataDB.class, updateData.getCert_id());

			// == do update ==
			db.setAccept_time((updateData.getAccept_time()));
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for "+updateData.getUserName(),e);
		} finally {
			localDAO.close();
		}

	}
	
	public void updateUnAccept(CertDataDB updateData) {
		try {
			localDAO.begin();
			CertDataDB db = localDAO.getEntityManager().find(CertDataDB.class, updateData.getCert_id());

			// == do update ==
			db.setAccept_time(updateData.getAccept_time());
//			db.setCert_status(updateData.getCert_status());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for "+updateData.getUserName(),e);
		} finally {
			localDAO.close();
		}

	}

}
