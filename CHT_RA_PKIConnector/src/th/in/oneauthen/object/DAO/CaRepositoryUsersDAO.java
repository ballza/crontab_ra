package th.in.oneauthen.object.DAO;

import java.util.List;

import org.apache.log4j.Logger;

import th.athichitsakul.dao.impl.StandardDAOImpl;
import th.in.oneauthen.object.CaRepositoryUsersDB;


public class CaRepositoryUsersDAO extends StandardDAOImpl<CaRepositoryUsersDB>{
//	private Logger logger = SystemLogger.generateSystemLogger(ca_repository_usersDAO.class);

	public CaRepositoryUsersDAO() {
		super("generalrao");
		// TODO Auto-generated constructor stub
	}
	
	public List<CaRepositoryUsersDB> QueryTest(String account) {
		List<CaRepositoryUsersDB> historyTotal = null;
		try {

				localDAO.begin();
				historyTotal = (List<CaRepositoryUsersDB>) localDAO.getEntityManager().createNamedQuery("testQuery")
						.setParameter("accountTest", account)
						.getResultList();

			
		} catch (Exception e) {
			//logger.error("Invalid minDate loading with  userUid" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}

}
