package th.in.oneauthen.object.DAO;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import th.athichitsakul.dao.impl.StandardDAOImpl;
import th.in.oneauthen.object.CertRegisterFormsDB;
import th.in.oneauthen.object.RequestCertDB;

public class CertRegisterFormDAO extends StandardDAOImpl<CertRegisterFormsDB> {

	public CertRegisterFormDAO() {
		super("generalrao");
		// TODO Auto-generated constructor stub
	}

	public CertRegisterFormsDB findByUniqueID(String uniqueID) {
		CertRegisterFormsDB historyTotal = null;
		try {

			localDAO.begin();
			historyTotal = (CertRegisterFormsDB) localDAO.getEntityManager().createNamedQuery("FindByUniqueID")
					.setParameter("uniqueID", uniqueID).getSingleResult();

		} catch (Exception e) {
			// logger.error("Invalid query querySelfCert" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}

	public CertRegisterFormsDB serviceByUniqueID(String uniqueID) {
		CertRegisterFormsDB historyTotal = null;
		try {

			localDAO.begin();
			historyTotal = (CertRegisterFormsDB) localDAO.getEntityManager().createNamedQuery("serviceByUniqueID")
					.setParameter("ServiceUniqueID", uniqueID).getSingleResult();

		} catch (Exception e) {
			// logger.error("Invalid query querySelfCert" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}
	
	public List<CertRegisterFormsDB> queyCertSN(String certSN) {
		List<CertRegisterFormsDB> historyTotal = null;
		try {

			localDAO.begin();
			historyTotal = (List<CertRegisterFormsDB>) localDAO.getEntityManager().createNamedQuery("checkBycertSN")
					.setParameter("certSN", certSN).getResultList();

		} catch (Exception e) {
			// logger.error("Invalid query querySelfCert" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}
	
	public void insertCSR(CertRegisterFormsDB updateData) throws Exception {
		try {
			localDAO.begin();
			CertRegisterFormsDB db = localDAO.getEntityManager().find(CertRegisterFormsDB.class, updateData.getCert_register_formsId());
			// == do update ==
			if (db == null)
				throw new NullPointerException("Reqeust data not found");
			
				db.setRegistXML(updateData.getRegistXML());
				db.setCertReq(updateData.getCertReq());
				localDAO.getEntityManager().merge(db);
				localDAO.getTransaction().commit();

		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			throw e;
		} finally {
			localDAO.close();
		}
	}

}
