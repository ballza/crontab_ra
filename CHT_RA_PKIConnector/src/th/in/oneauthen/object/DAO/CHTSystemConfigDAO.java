package th.in.oneauthen.object.DAO;

import th.athichitsakul.dao.impl.StandardDAOImpl;
import th.in.oneauthen.object.CHTSystemConfigDB;



public class CHTSystemConfigDAO  extends StandardDAOImpl<CHTSystemConfigDB> {

	public CHTSystemConfigDAO() {
		super("generalrao");
		// TODO Auto-generated constructor stub
	}
	
	public CHTSystemConfigDB getContents(String item_name) {
		CHTSystemConfigDB historyTotal = null;
		try {

			localDAO.begin();
			historyTotal = (CHTSystemConfigDB) localDAO.getEntityManager()
					.createNamedQuery("getContents")
					.setParameter("item_name", item_name)
					.getSingleResult();

		} catch (Exception e) {
			// logger.error("Invalid query querySelfCert" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}

}
