package th.in.oneauthen.object.DAO;

import java.util.List;

import org.apache.log4j.Logger;

import th.athichitsakul.dao.impl.StandardDAOImpl;
import th.in.oneauthen.object.CertTypeDB;

public class CertTypesDAO  extends StandardDAOImpl<CertTypeDB> {

	public CertTypesDAO() {
		super("generalrao");
		// TODO Auto-generated constructor stub
	}
	
	public List<CertTypeDB> getTypeId() {
		List<CertTypeDB> historyTotal = null;
		try {

				localDAO.begin();
				historyTotal = (List<CertTypeDB>) localDAO.getEntityManager().createNamedQuery("queryCertTypeID")
						.getResultList();

			
		} catch (Exception e) {
//			logger.error("Invalid query certTypeID" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}
	
	public List<CertTypeDB> getDefineXML(String CertTypeID) {
		List<CertTypeDB> historyTotal = null;
		try {

				localDAO.begin();
				historyTotal = (List<CertTypeDB>) localDAO.getEntityManager().createNamedQuery("queryDefineXML")
						.setParameter("CertTypeID", CertTypeID)
						.getResultList();

			
		} catch (Exception e) {
//			logger.error("Invalid query certTypeID" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}

	public CertTypeDB searchByCertTypeID(String certTypeId) {
		List<CertTypeDB> queryResult = null;
		try {

				localDAO.begin();
				queryResult = (List<CertTypeDB>) localDAO.getEntityManager().createNamedQuery("searchByCertTypeID")
						.setParameter("certTypeId", certTypeId)
						.getResultList();

				return queryResult.get(0);
		} catch (Exception e) {
//			logger.error("Invalid query certTypeID" , e);
			return null;
		} finally {
			localDAO.close();
		}
		
	}

}
