package th.in.oneauthen.object;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name="systemconfig")
@GenericGenerator(name ="idGenerator",strategy="native")
@NamedQueries(value = { 
		@NamedQuery(name="getContents", query="SELECT u FROM CHTSystemConfigDB u WHERE u.config_key = :item_name"),
	})
public class CHTSystemConfigDB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6122666684364156269L;
	
	@Id
	@GeneratedValue(generator="idGenerator", strategy=GenerationType.AUTO) @Column(name ="id",nullable = false)
	private int config_id;
	@Column(name = "Item_Name", nullable = false)
	private String config_key;
	@Column(name = "Contents")
	private String config_contents;
	@Column(name = "last_update")
	private Date config_time;
	public int getConfig_id() {
		return config_id;
	}
	public void setConfig_id(int config_id) {
		this.config_id = config_id;
	}
	public String getConfig_key() {
		return config_key;
	}
	public void setConfig_key(String config_key) {
		this.config_key = config_key;
	}
	public String getConfig_contents() {
		return config_contents;
	}
	public void setConfig_contents(String config_contents) {
		this.config_contents = config_contents;
	}
	public Date getConfig_time() {
		return config_time;
	}
	public void setConfig_time(Date config_time) {
		this.config_time = config_time;
	}
	
	

}
