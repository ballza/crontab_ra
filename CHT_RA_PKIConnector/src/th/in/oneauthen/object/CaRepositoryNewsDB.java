package th.in.oneauthen.object;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="ca_repository_news")
@GenericGenerator(name ="idGenerator",strategy="native")
public class CaRepositoryNewsDB  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4602373015367642111L;
	
	
	@Id
	@GeneratedValue(generator="idGenerator", strategy=GenerationType.AUTO) @Column(name ="id",nullable = false)
	private int ca_rep_newId;
	@Column(name = "title", nullable = false)
	private String title;
	@Column(name = "contents")
	private String contents;
	@Column(name = "author",nullable = false)
	private String author;
	@Column(name = "last_update",nullable = false)
	private Date last_update;
	@Column(name = "disabled")
	private Date disabled;
	public int getCa_rep_newId() {
		return ca_rep_newId;
	}
	public void setCa_rep_newId(int ca_rep_newId) {
		this.ca_rep_newId = ca_rep_newId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Date getLast_update() {
		return last_update;
	}
	public void setLast_update(Date last_update) {
		this.last_update = last_update;
	}
	public Date getDisabled() {
		return disabled;
	}
	public void setDisabled(Date disabled) {
		this.disabled = disabled;
	}


	
}
