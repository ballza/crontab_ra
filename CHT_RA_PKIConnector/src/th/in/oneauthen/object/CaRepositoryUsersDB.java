package th.in.oneauthen.object;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="ca_repository_users")
@GenericGenerator(name ="idGenerator",strategy="native")
@NamedQueries(value = { 
		@NamedQuery(name="testQuery", query="SELECT u FROM CaRepositoryUsersDB u WHERE u.account = :accountTest"),
	})
public class CaRepositoryUsersDB implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4099239508239161549L;

	@Id
	@GeneratedValue(generator="idGenerator", strategy=GenerationType.AUTO) @Column(name ="id",nullable = false)
	private int ca_rep_usersId;
	@Column(name = "account", nullable = false)
	private String account;
	@Column(name = "password_hash", nullable = false)
	private String password_hash;
	@Column(name = "disabled")
	private Date disabled;
	@Column(name = "last_update",nullable = false)
	private Date last_update;
	public int getCa_rep_usersId() {
		return ca_rep_usersId;
	}
	public void setCa_rep_usersId(int ca_rep_usersId) {
		this.ca_rep_usersId = ca_rep_usersId;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword_hash() {
		return password_hash;
	}
	public void setPassword_hash(String password_hash) {
		this.password_hash = password_hash;
	}
	public Date getDisabled() {
		return disabled;
	}
	public void setDisabled(Date disabled) {
		this.disabled = disabled;
	}
	public Date getLast_update() {
		return last_update;
	}
	public void setLast_update(Date last_update) {
		this.last_update = last_update;
	}


	
}
