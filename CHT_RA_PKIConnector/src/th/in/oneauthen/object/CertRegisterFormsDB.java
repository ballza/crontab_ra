package th.in.oneauthen.object;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="cert_register_forms")
@GenericGenerator(name ="idGenerator",strategy="native")
@NamedQueries(value = { 
		@NamedQuery(name="FindByUniqueID", query="SELECT u FROM CertRegisterFormsDB u WHERE u.userID = :uniqueID and u.status = 2"),
		@NamedQuery(name="serviceByUniqueID", query="SELECT u FROM CertRegisterFormsDB u WHERE u.userID = :ServiceUniqueID"),
		@NamedQuery(name="checkBycertSN", query="SELECT u FROM CertRegisterFormsDB u WHERE u.certSn = :certSN"),
	})
public class CertRegisterFormsDB implements Serializable {

	/**serviceByUniqueID
	 * 
	 */
	private static final long serialVersionUID = -4997072129453835766L;
	@Id
	@GeneratedValue(generator="idGenerator", strategy=GenerationType.AUTO) @Column(name ="form_id",nullable = false)
	private int cert_register_formsId;
	@Column(name = "user_name", nullable = false)
	private String userName;
	@Column(name = "user_id", nullable = false)
	private String userID;
	@Column(name = "user_email", nullable = false)
	private String userEmail;
	@Column(name = "user_organization")
	private String userO;
	@Column(name = "cert_request")
	private String certReq;
	@Column(name = "form_status",nullable = false)
	@ColumnDefault("0")
	private int status = 0;
	@Column(name = "cert_type")
	private String certType;
	@Column(name = "register_time")
	private Date registerTime;
	@Column(name = "verifymail_time")
	private Date verifyMailTime;
	@Column(name = "cert_sn")
	private String certSn;
	@Column(name = "apply_time")
	private Date applyTime;
	@Column(name = "action",nullable = false )
	@ColumnDefault("'Enroll'")
	private String act = "Enroll";
	@Column(name = "RegistXML")
	private String registXML;
	@Column(name = "RevokeReason")
	private String revokeReason;
	@Column(name = "RejectMessage")
	private String rejectMessage;
	
	public int getCert_register_formsId() {
		return cert_register_formsId;
	}
	public void setCert_register_formsId(int cert_register_formsId) {
		this.cert_register_formsId = cert_register_formsId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserO() {
		return userO;
	}
	public void setUserO(String userO) {
		this.userO = userO;
	}
	public String getCertReq() {
		return certReq;
	}
	public void setCertReq(String certReq) {
		this.certReq = certReq;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCertType() {
		return certType;
	}
	public void setCertType(String certType) {
		this.certType = certType;
	}
	public Date getRegisterTime() {
		return registerTime;
	}
	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}
	public Date getVerifyMailTime() {
		return verifyMailTime;
	}
	public void setVerifyMailTime(Date verifyMailTime) {
		this.verifyMailTime = verifyMailTime;
	}
	public String getCertSn() {
		return certSn;
	}
	public void setCertSn(String certSn) {
		this.certSn = certSn;
	}
	public Date getApplyTime() {
		return applyTime;
	}
	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}
	public String getAct() {
		return act;
	}
	public void setAct(String act) {
		this.act = act;
	}
	public String getRegistXML() {
		return registXML;
	}
	public void setRegistXML(String registXML) {
		this.registXML = registXML;
	}
	public String getRevokeReason() {
		return revokeReason;
	}
	public void setRevokeReason(String revokeReason) {
		this.revokeReason = revokeReason;
	}
	public String getRejectMessage() {
		return rejectMessage;
	}
	public void setRejectMessage(String rejectMessage) {
		this.rejectMessage = rejectMessage;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
}
