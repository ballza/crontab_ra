package th.in.oneauthen.object;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name="user_ra")
@GenericGenerator(name ="idGenerator",strategy="native")
@NamedQueries(value = { 
		@NamedQuery(name="QueryUsername", query="SELECT u FROM UserRaDB u WHERE u.userName = :userName"),
		@NamedQuery(name="QueryUserRa", query="FROM UserRaDB"),
		@NamedQuery(name="QueryUser", query="FROM UserRaDB"),
		@NamedQuery(name="findCertFP", query="SELECT u FROM UserRaDB u WHERE u.certFP = :clientCertFP"),
		@NamedQuery(name="findBySAMLToken", query="SELECT u FROM UserRaDB u WHERE u.samlTokenUid = :samlTokenUid"),
	})
public class UserRaDB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7876182319885826492L;
	
	@Id
	@GeneratedValue(generator="idGenerator", strategy=GenerationType.AUTO) @Column(name ="ID", nullable = false)
	private int userId;
	@Column(name = "USERNAME", unique=true, nullable = false)
	private String userName;
	@Column(name = "TOKEN_UID", nullable = false)
	private String samlTokenUid;
	@Column(name = "LAST_LOGIN")
	private Date lastLogin;
	@Column(name = "PERSONAL_ID", nullable = false)
	private String personalID;
	@Column(name = "COUNT_CERT", nullable = false)
	private Integer countCert;
	@Column(name = "TELEPHONE")
	private String telephone;
	@Column(name = "PASSWORD")
	private String  pwd;
	@Column(name = "CERT_FP")
	private String  certFP;
	@Column(name = "TYPE")
	private String  type;
	
	
	
	
	public String getCertFP() {
		return certFP;
	}
	public void setCertFP(String certFP) {
		this.certFP = certFP;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Integer getCountCert() {
		return countCert;
	}
	public void setCountCert(Integer countCert) {
		this.countCert = countCert;
	}
	public int getUserId() {
		
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getSamlTokenUid() {
		return samlTokenUid;
	}
	public void setSamlTokenUid(String samlTokenUid) {
		this.samlTokenUid = samlTokenUid;
	}
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	public String getPersonalID() {
		return personalID;
	}
	public void setPersonalID(String personalID) {
		this.personalID = personalID;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	

	
	
}
