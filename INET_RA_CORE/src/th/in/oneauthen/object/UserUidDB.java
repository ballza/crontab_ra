package th.in.oneauthen.object;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name="user_uid")
//@SequenceGenerator(name = "idGenerator", initialValue=1, schema="user_uid")
@GenericGenerator(name ="idGenerator",strategy="native")
@NamedQueries(value = { 
		@NamedQuery(name="findByUsername", query="SELECT u FROM UserUidDB u WHERE u.userName = :userName"),
		@NamedQuery(name="findUser100", query="SELECT u FROM UserUidDB u WHERE u.type = :type"),
		@NamedQuery(name="findOneId", query="SELECT u FROM UserUidDB u WHERE u.oneId = :oneId"),
		@NamedQuery(name="findAdmin", query="SELECT u FROM UserUidDB u WHERE u.userName = :userName"),
		@NamedQuery(name="findUserOnechat", query="SELECT u FROM UserUidDB u WHERE u.oneEmail = :oneEmail"),
		////////////////// admin ////////////////////
		@NamedQuery(name="findUser", query="FROM UserUidDB")
	})
public class UserUidDB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2334340074748240300L;
	@Id
	@GeneratedValue(generator="idGenerator", strategy=GenerationType.AUTO) @Column(name ="ID")
	private int userId;
	@Column(name = "USERNAME", unique=true, nullable = false)
	private String userName;
	@Column(name = "PASSWORD")
	private String password;
	@Column(name = "TOKEN_UID")
	private String samlTokenUid;
	@Column(name = "LAST_LOGIN")
	private Date lastLogin;
	@Column(name = "COMPANY_NAME")
	private String companyName;
	@Column(name = "TYPE_ID")
	private String type;
	@Column(name = "ID_ONE")
	private String oneId;
	@Column(name = "EMAIL")
	private String oneEmail;
	
	
	
	
	public String getOneId() {
		return oneId;
	}
	public void setOneId(String oneId) {
		this.oneId = oneId;
	}
	public String getOneEmail() {
		return oneEmail;
	}
	public void setOneEmail(String oneEmail) {
		this.oneEmail = oneEmail;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getUserId() {
		return userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getSamlTokenUid() {
		return samlTokenUid;
	}
	public void setSamlTokenUid(String samlTokenUid) {
		this.samlTokenUid = samlTokenUid;
	}
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
}