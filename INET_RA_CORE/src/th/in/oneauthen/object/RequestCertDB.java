package th.in.oneauthen.object;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import th.in.oneauthen.object.UserRaDB;

@Entity
@Table(name="request_cert")
@GenericGenerator(name ="idGenerator",strategy="native")
@NamedQueries(value = { 
		@NamedQuery(name="ForUpdateStatus", query="SELECT u FROM RequestCertDB u WHERE u.status = '1'"),
		@NamedQuery(name="reqUniqueID", query="SELECT u FROM RequestCertDB u WHERE u.uniqueId = :uniqueID"),
		@NamedQuery(name="updateCertStatus2", query="SELECT u FROM RequestCertDB u WHERE u.status = '2' and u.action = 'Enroll' and u.reject = '-'"),
		@NamedQuery(name="SelfReq", query="SELECT u FROM RequestCertDB u WHERE u.creator = :User and (u.status = '1' or u.status = '0') order by u.Id desc"),
		@NamedQuery(name="reqCertSnAndEnroll", query="SELECT u FROM RequestCertDB u WHERE u.action = 'Enroll' and u.certSn = :certSN"),
		@NamedQuery(name="ManagerReq", query="SELECT u FROM RequestCertDB u WHERE u.care_taker = :careTaker and (u.status = '1' or u.status = '0') order by u.Id desc"),
	})
public class RequestCertDB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 258681168733361079L;
	
	@Id
	@GeneratedValue(generator="idGenerator", strategy=GenerationType.AUTO) @Column(name ="ID",nullable = false)
	private int Id;
	@Column(name = "UNIQUE_ID", nullable = false)
	private String uniqueId;
	@Column(name = "EMAIL")
	private String email;
	@Column(name = "X509")
	private String x509;
	@Column(name = "CERT_SN")
	private String certSn;
	@ManyToOne
	@JoinColumn(name = "CREATOR" , nullable = false)
	private UserRaDB creator;
	
	@JoinColumn(name = "CARE_TAKER" , nullable = false)
	private Integer care_taker;
	
	@Column(name = "STATUS", nullable = false)
	private String status;
	@Column(name = "CERT_TYPE")
	private String certType;
	@Column(name = "ACTION", nullable = false)
	private String action;
	@Column(name = "REVOKEREASON")
	private String revokeReason;
	@Column(name = "REJECT")
	private String reject;
	@Column(name = "TIME_APPROVE")
	private Date  timeApprove;
	@Column(name = "DETAIL")
	private String  detail;
	@Column(name = "PATH_PDF")
	private String  pathPdf;
	

	

	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getX509() {
		return x509;
	}
	public void setX509(String x509) {
		this.x509 = x509;
	}
	public String getCertSn() {
		return certSn;
	}
	public void setCertSn(String certSn) {
		this.certSn = certSn;
	}
	public UserRaDB getCreator() {
		return creator;
	}
	public void setCreator(UserRaDB creator) {
		this.creator = creator;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCertType() {
		return certType;
	}
	public void setCertType(String certType) {
		this.certType = certType;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getRevokeReason() {
		return revokeReason;
	}
	public void setRevokeReason(String revokeReason) {
		this.revokeReason = revokeReason;
	}
	public String getReject() {
		return reject;
	}
	public void setReject(String reject) {
		this.reject = reject;
	}
	public Date getTimeApprove() {
		return timeApprove;
	}
	public void setTimeApprove(Date timeApprove) {
		this.timeApprove = timeApprove;
	}
	public String getPathPdf() {
		return pathPdf;
	}
	public void setPathPdf(String pathPdf) {
		this.pathPdf = pathPdf;
	}
	public Integer getCare_taker() {
		return care_taker;
	}
	public void setCare_taker(Integer care_taker) {
		this.care_taker = care_taker;
	}
	
	
	
	
	

}