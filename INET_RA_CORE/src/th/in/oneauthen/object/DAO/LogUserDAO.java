package th.in.oneauthen.object.DAO;

import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;

import th.athichitsakul.dao.DAO;
import th.athichitsakul.dao.impl.StandardDAOImpl;
import th.in.oneauthen.object.LogUserDB;
import th.in.oneauthen.object.UserRaDB;


public class LogUserDAO extends StandardDAOImpl<LogUserDB> {

	public LogUserDAO() {
		super("inetra");
		// TODO Auto-generated constructor stub
	}
	
	public List<LogUserDB> findInDate (Date date) {
		List<LogUserDB> historyTotal = null;
		try {

			localDAO.begin();
				historyTotal = (List<LogUserDB>) localDAO.getEntityManager().createNamedQuery("getInDate")
						.setParameter("dateFind", date,TemporalType.DATE)
						.getResultList();

			
		} catch (Exception e) {
//			logger.error("Invalid loading with "+ dateFind +"and userID"+userUID , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}
	
	public List<LogUserDB> findUserInDate (int creator) {
		List<LogUserDB> historyTotal = null;
		try {
			UserRaDB user = new UserRaDAO().find(creator);
			if (user == null) {
				return null;
			} else {
			localDAO.begin();
				historyTotal = (List<LogUserDB>) localDAO.getEntityManager().createNamedQuery("getUserInDate")
						.setParameter("user", user)
						.getResultList();
			}
			
		} catch (Exception e) {
//			logger.error("Invalid loading with "+ dateFind +"and userID"+userUID , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}
	
	public List<LogUserDB> findBeweenDate (Date dateFrist,Date dateEnd) {
		List<LogUserDB> historyTotal = null;
		try {

			localDAO.begin();
				historyTotal = (List<LogUserDB>) localDAO.getEntityManager().createNamedQuery("getBetweenDate")
						.setParameter("dateFrist", dateFrist,TemporalType.DATE)
						.setParameter("dateEnd", dateEnd,TemporalType.DATE)
						.getResultList();

			
		} catch (Exception e) {
//			logger.error("Invalid loading with "+ dateFind +"and userID"+userUID , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}
	
	public List<LogUserDB> findInDateStatus (Date date, String status) {
		List<LogUserDB> historyTotal = null;
		try {

			localDAO.begin();
				historyTotal = (List<LogUserDB>) localDAO.getEntityManager().createNamedQuery("getInDateStatus")
						.setParameter("dateFind", date,TemporalType.DATE)
						.setParameter("status", status)
						.getResultList();

			
		} catch (Exception e) {
//			logger.error("Invalid loading with "+ dateFind +"and userID"+userUID , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}
	
	public List<LogUserDB> findInDateEvent (Date date, String event) {
		List<LogUserDB> historyTotal = null;
		try {

			localDAO.begin();
				historyTotal = (List<LogUserDB>) localDAO.getEntityManager().createNamedQuery("getInDateEvent")
						.setParameter("dateFind", date,TemporalType.DATE)
						.setParameter("event", event)
						.getResultList();

			
		} catch (Exception e) {
//			logger.error("Invalid loading with "+ dateFind +"and userID"+userUID , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}

}