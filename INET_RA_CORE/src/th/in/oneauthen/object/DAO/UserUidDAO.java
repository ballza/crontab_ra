package th.in.oneauthen.object.DAO;

import java.util.List;

import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.Base64;

import th.athichitsakul.dao.impl.StandardDAOImpl;
import th.in.oneauthen.object.UserUidDB;
import th.in.oneauthen.util.CryptoUtil;
import th.in.oneauthen.util.SystemLogger;

public class UserUidDAO extends StandardDAOImpl<UserUidDB> {
	public static final String SALT = "AccTkn-";
	private Logger logger = SystemLogger.generateSystemLogger(UserUidDAO.class);

	/**
	 * Introduce DAO in superclass that this UserDAOImpl going to use MySQL-PU
	 * persistance-unit configuration.
	 */
	public UserUidDAO() {
		super("inetra");
	}

	/**
	 * Instead of using method find, that can search any object by their primary
	 * key. This method made this UserDAO can be search by username which is an
	 * another unique parameter.
	 * 
	 * @param username
	 * @return User instance of specify username
	 */

	public void updateAttrEmail(UserUidDB updateData) {
		try {
			localDAO.begin();
			UserUidDB db = localDAO.getEntityManager().find(UserUidDB.class, updateData.getUserId());

			// == do update ==
			// db.setProfileKey(updateData.getProfileKey());
			// db.setProfileKeyPIN(updateData.getProfileKeyPIN());
			db.setOneEmail(updateData.getOneEmail());

			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			logger.error("Update user profile failed for " + updateData.getOneEmail(), e);
		} finally {
			localDAO.close();
		}

	}

	public void updateAttrType(UserUidDB updateData) {
		localDAO.begin();
		try {
			UserUidDB db = localDAO.getEntityManager().find(UserUidDB.class, updateData.getUserId());

			// == do update ==
			// db.setProfileKey(updateData.getProfileKey());
			// db.setProfileKeyPIN(updateData.getProfileKeyPIN());
			db.setType(updateData.getType());

			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			logger.error("Update user profile failed for " + updateData.getType(), e);
		}

	}

	public List<UserUidDB> adminFind(int start, int limitEnd) {
		List<UserUidDB> user = null;
		try {
			localDAO.begin();
			user = (List<UserUidDB>) localDAO.getEntityManager().createNamedQuery("findUser").setMaxResults(limitEnd)
					.setFirstResult(start).getResultList();
		} catch (Exception e) {
			logger.error("Invalid user loading with type id = ", e);
		} finally {
			localDAO.close();
		}
		return user;
	}

	public UserUidDB findUsername(String username) {
		UserUidDB user = null;
		try {
			localDAO.begin();
			user = (UserUidDB) localDAO.getEntityManager().createNamedQuery("findByUsername")
					.setParameter("userName", username).getSingleResult();
		} catch (Exception e) {
			logger.error("Invalid user loading with name " + username, e);
		} finally {
			localDAO.close();
		}
		return user;
	}

	public UserUidDB findOneId(String oneId) {
		UserUidDB user = null;
		try {
			localDAO.begin();
			user = (UserUidDB) localDAO.getEntityManager().createNamedQuery("findOneId").setParameter("oneId", oneId)
					.getSingleResult();
		} catch (Exception e) {
			logger.error("Invalid user loading with ID " + oneId, e);
		} finally {
			localDAO.close();
		}
		return user;
	}

	public static UserUidDB findUserByOneId(String oneId) {

		try {
			return new UserUidDAO().findOneId(oneId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public UserUidDB findEmailOnechat(String email) {
		UserUidDB user = null;
		try {
			localDAO.begin();
			user = (UserUidDB) localDAO.getEntityManager().createNamedQuery("findUserOnechat")
					.setParameter("oneEmail", email).getSingleResult();
		} catch (Exception e) {
			logger.error("Invalid user loading with ID " + email, e);
		} finally {
			localDAO.close();
		}
		return user;
	}

	public static UserUidDB findUserByeEmail(String email) {

		try {
			return new UserUidDAO().findEmailOnechat(email);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public UserUidDB findAdmin(String userName) {
		UserUidDB user = null;
		try {
			localDAO.begin();
			user = (UserUidDB) localDAO.getEntityManager().createNamedQuery("findAdmin")
					.setParameter("userName", userName).getSingleResult();
		} catch (Exception e) {
			logger.error("Invalid user loading with ID " + userName, e);
		} finally {
			localDAO.close();
		}
		return user;
	}

	public static UserUidDB findAdminByUser(String userName) {

		try {
			return new UserUidDAO().findAdmin(userName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String generateUserAccessToken(String username) {
		try {
			String accessToken = Base64.toBase64String(CryptoUtil.encryptData((SALT + username).getBytes()));
			return accessToken;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static UserUidDB findUserByAccessToken(String accessToken) {
		// int userId = -1;
		try {
			String plainText = new String(CryptoUtil.decryptData(Base64.decode(accessToken)));
			plainText = plainText.replaceAll(SALT, "").trim();
			// userId = Integer.parseInt(plainText);

			return new UserUidDAO().findUsername(plainText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void updateTimeLogin(UserUidDB updateData) {
		localDAO.begin();
		try {
			UserUidDB db = localDAO.getEntityManager().find(UserUidDB.class, updateData.getUserId());

			// == do update ==
			db.setLastLogin(updateData.getLastLogin());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			logger.error("Update last login failed for " + updateData.getUserName(), e);
		}

	}

	public List<UserUidDB> findUserLimt100(String type_id, int start, int end) {
		List<UserUidDB> user = null;
		try {
			localDAO.begin();
			user = (List<UserUidDB>) localDAO.getEntityManager().createNamedQuery("findUser100")
					.setParameter("type", type_id).setFirstResult(start).setMaxResults(end).getResultList();
		} catch (Exception e) {
			logger.error("Invalid user loading with type id = " + type_id, e);
		} finally {
			localDAO.close();
		}
		return user;
	}
}