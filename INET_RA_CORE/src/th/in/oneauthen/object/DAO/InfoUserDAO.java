package th.in.oneauthen.object.DAO;

import java.util.List;

import th.athichitsakul.dao.impl.StandardDAOImpl;
import th.in.oneauthen.object.CertificateDB;
import th.in.oneauthen.object.InfoUserDB;
import th.in.oneauthen.object.UserRaDB;

public class InfoUserDAO extends StandardDAOImpl<InfoUserDB>{

	public InfoUserDAO() {
		super("inetra");
		// TODO Auto-generated constructor stub
	}
	
	public InfoUserDB getInfoUser(int creator) {
		InfoUserDB info = null;
		try {
			UserRaDB user = new UserRaDAO().find(creator);
			if (user == null) {
				return null;
			} else {
				localDAO.begin();
				info = (InfoUserDB) localDAO.getEntityManager().createNamedQuery("QueryCreator")
						.setParameter("creator", user).getSingleResult();
			}

		} catch (Exception e) {
			//e.printStackTrace();
			// logger.error("Invalid user loading with type id = ", e);
		} finally {
			localDAO.close();
		}
		return info;
	}
	
	public void updateBizInfo(InfoUserDB updateData) {

		try {
			localDAO.begin();
			InfoUserDB db = localDAO.getEntityManager().find(InfoUserDB.class, updateData.getInfoID());

			// == do update ==
			db.setBizInfo(updateData.getBizInfo());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
		} finally {
			localDAO.close();
		}

	}
	
	public void updatePersonalInfo(InfoUserDB updateData) {

		try {
			localDAO.begin();
			InfoUserDB db = localDAO.getEntityManager().find(InfoUserDB.class, updateData.getInfoID());

			// == do update ==
			db.setPersonalInfo(updateData.getPersonalInfo());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
		} finally {
			localDAO.close();
		}

	}
	
	

}
