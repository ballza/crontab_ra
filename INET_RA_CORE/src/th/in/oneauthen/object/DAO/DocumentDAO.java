package th.in.oneauthen.object.DAO;

import th.athichitsakul.dao.impl.StandardDAOImpl;
import th.in.oneauthen.object.DocumentDB;



public class DocumentDAO extends StandardDAOImpl<DocumentDB>{

	public DocumentDAO() {
		super("inetra");
		// TODO Auto-generated constructor stub
	}
	
	
	public static synchronized int updateCount(int index) {
		DocumentDAO docDAO = null;
		try {
			
			docDAO = new DocumentDAO();
			
			docDAO.localDAO.begin();
			DocumentDB db = docDAO.localDAO.getEntityManager().find(DocumentDB.class, index);
			
			int count = db.getCount();
			count++;
			db.setCount(count);
			
			//Thread.sleep(200);
			//commit
			//== do update ==
//            db.setCount(updateData.getCount());
			docDAO.localDAO.getEntityManager().merge(db);
			docDAO.localDAO.getTransaction().commit();
			return count;
		} catch (Exception e) {
			if (docDAO.localDAO.getTransaction().isActive()) {
				docDAO.localDAO.getTransaction().rollback();
			}
		//	logger.error("Update last login failed for "+updateData.getUserName(),e);
		} finally {
			docDAO.localDAO.close();
		}
		return -1;
	}
	
	public static void main(String[] args) {
		
	}

}
