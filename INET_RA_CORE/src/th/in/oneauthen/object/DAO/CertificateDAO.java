package th.in.oneauthen.object.DAO;

import java.util.List;

import th.athichitsakul.dao.impl.StandardDAOImpl;
import th.in.oneauthen.object.CertificateDB;
import th.in.oneauthen.object.RequestCertDB;
import th.in.oneauthen.object.UserRaDB;

public class CertificateDAO extends StandardDAOImpl<CertificateDB> {

	public CertificateDAO() {
		super("inetra");
		// TODO Auto-generated constructor stub
	}

	public CertificateDB findByUniqueID(String uniqueID) {
		CertificateDB historyTotal = null;
		try {

			localDAO.begin();
			historyTotal = (CertificateDB) localDAO.getEntityManager().createNamedQuery("FindByUniqueIDCert")
					.setParameter("uniqueID", uniqueID).getSingleResult();

		} catch (Exception e) {
			// logger.error("Invalid query querySelfCert" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}
	
	public CertificateDB findBySn(String certSn) {
		CertificateDB historyTotal = null;
		try {

			localDAO.begin();
			historyTotal = (CertificateDB) localDAO.getEntityManager().createNamedQuery("findBySn")
					.setParameter("certSn", certSn).getSingleResult();

		} catch (Exception e) {
			// logger.error("Invalid query querySelfCert" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}

	public CertificateDB revokeByCertSn(String CertSn) {
		CertificateDB historyTotal = null;
		try {

			localDAO.begin();
			historyTotal = (CertificateDB) localDAO.getEntityManager().createNamedQuery("revoke")
					.setParameter("CertSn", CertSn).getSingleResult();

		} catch (Exception e) {
			// logger.error("Invalid query querySelfCert" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}

	public void updateCertSnAndX509(CertificateDB updateData) {
		try {
			localDAO.begin();
			CertificateDB db = localDAO.getEntityManager().find(CertificateDB.class, updateData.getCert_ID());

			// == do update ==
			db.setCert_Sn(updateData.getCert_Sn());
			db.setCert_x509(updateData.getCert_x509());
			db.setStatus(updateData.getStatus());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for "+updateData.getUserName(),e);
		} finally {
			localDAO.close();
		}

	}

	public void updateCertStatusForRevoke(CertificateDB updateData) {
		try {
			localDAO.begin();
			CertificateDB db = localDAO.getEntityManager().find(CertificateDB.class, updateData.getCert_ID());

			// == do update ==
			db.setStatus(updateData.getStatus());
			db.setAccept_time(updateData.getAccept_time());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for "+updateData.getUserName(),e);
		} finally {
			localDAO.close();
		}

	}

	public List<CertificateDB> getSelfCert(int creator) {
		List<CertificateDB> Cert = null;
		try {
			UserRaDB user = new UserRaDAO().find(creator);
			if (user == null) {
				return null;
			} else {
				localDAO.begin();
				Cert = (List<CertificateDB>) localDAO.getEntityManager().createNamedQuery("findSelfCert")
						.setParameter("User", user).getResultList();
			}

		} catch (Exception e) {
			// logger.error("Invalid user loading with type id = ", e);
		} finally {
			localDAO.close();
		}
		return Cert;
	}
	
	public List<CertificateDB> getManagerCert(int careTaker) {
		List<CertificateDB> Cert = null;
		try {
				localDAO.begin();
				Cert = (List<CertificateDB>) localDAO.getEntityManager().createNamedQuery("findManagerCert")
						.setParameter("careTaker", careTaker).getResultList();

		} catch (Exception e) {
			// logger.error("Invalid user loading with type id = ", e);
		} finally {
			localDAO.close();
		}
		return Cert;
	}
	

	public List<CertificateDB> getSelfRequest(int creator) {
		List<CertificateDB> Cert = null;
		try {
			UserRaDB user = new UserRaDAO().find(creator);
			if (user == null) {
				return null;
			} else {
				localDAO.begin();
				Cert = (List<CertificateDB>) localDAO.getEntityManager().createNamedQuery("SelfCertRequest")
						.setParameter("User", user).getResultList();
			}

		} catch (Exception e) {
			// logger.error("Invalid user loading with type id = ", e);
		} finally {
			localDAO.close();
		}
		return Cert;
	}

	public void updateTimeAccept(CertificateDB updateData) {

		try {
			localDAO.begin();
			CertificateDB db = localDAO.getEntityManager().find(CertificateDB.class, updateData.getCert_ID());

			// == do update ==
			db.setAccept_time(updateData.getAccept_time());
			db.setStatus(updateData.getStatus());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
		} finally {
			localDAO.close();
		}

	}

	public void changeToRevoker(CertificateDB updateData) {
		try {
			localDAO.begin();
			CertificateDB db = localDAO.getEntityManager().find(CertificateDB.class, updateData.getCert_ID());

			// == do update ==
			db.setAccept_time(updateData.getAccept_time());
			db.setStatus(updateData.getStatus());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
		} finally {
			localDAO.close();
		}

	}

	public void serviceSyncDB(CertificateDB updateData) {
		try {
			localDAO.begin();
			CertificateDB db = localDAO.getEntityManager().find(CertificateDB.class, updateData.getCert_ID());

			// == do update ==
			db.setCert_x509(updateData.getCert_x509());
			db.setStatus(updateData.getStatus());
			db.setRevokeReason(updateData.getRevokeReason());
			db.setCert_Sn(updateData.getCert_Sn());
			db.setCreator(updateData.getCreator());
			db.setExpiredDate(updateData.getExpiredDate());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			e.printStackTrace();
		} finally {
			localDAO.close();
		}
	}
	
	public List<CertificateDB> Check10Day() {
		List<CertificateDB> user = null;
		try {
			localDAO.begin();
			user = (List<CertificateDB>) localDAO.getEntityManager().createNamedQuery("Check10Day")
					.getResultList();
		} catch (Exception e) {
			// logger.error("Invalid user loading with type id = ", e);
		} finally {
			localDAO.close();
		}
		return user;
	}
	
	
	public void EJBCA_revoke(CertificateDB updateData) {
		try {
			localDAO.begin();
			CertificateDB db = localDAO.getEntityManager().find(CertificateDB.class, updateData.getCert_ID());

			// == do update ==
			db.setRevokeReason(updateData.getRevokeReason());
			db.setStatus(updateData.getStatus());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
		} finally {
			localDAO.close();
		}

	}
	
	
	public List<CertificateDB> getCertFromStatus(String statusCert) {
		List<CertificateDB> Cert = null;
		try {
				localDAO.begin();
				Cert = (List<CertificateDB>) localDAO.getEntityManager().createNamedQuery("findCertFromStatus")
						.setParameter("statusCert", statusCert).getResultList();

		} catch (Exception e) {
			// logger.error("Invalid user loading with type id = ", e);
		} finally {
			localDAO.close();
		}
		return Cert;
	}

}
