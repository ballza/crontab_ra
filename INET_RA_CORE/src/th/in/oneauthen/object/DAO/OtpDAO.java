package th.in.oneauthen.object.DAO;

import java.util.List;

import org.apache.log4j.Logger;

import th.athichitsakul.dao.impl.StandardDAOImpl;
import th.in.oneauthen.object.OtpDB;
import th.in.oneauthen.util.SystemLogger;

public class OtpDAO extends StandardDAOImpl<OtpDB>{
	private Logger logger = SystemLogger.generateSystemLogger(OtpDAO.class);
	public OtpDAO() {
		super("inetra");
	}
	/**
	 * Use for authenticator OTP 
	 * @param credentialId
	 * @return OTP verification object
	 */
	public OtpDB findByCredential ( int credentialId ) {
		try {
			@SuppressWarnings("unchecked")
			List<OtpDB> queryResult = (List<OtpDB>) localDAO.getEntityManager().createNamedQuery("findByUserId")
					.setParameter("userId", credentialId)
					.getResultList();
			
			if (queryResult.size()==1) return queryResult.get(0);
			else return null;
		} catch ( Exception e ) {
			logger.error("Invalid otp loading with credentialId "+credentialId , e);
		} finally {
			localDAO.close();
		}
		return null;
	}
	/**
	 * Use for EMail and SMS OTP
	 * 
	 * @param credentialId
	 * @param refCode
	 * @return OTP verification object
	 */
	public OtpDB findByCredentialAndRefCode ( int credentialId, String refCode ) {
		try {
			@SuppressWarnings("unchecked")
			List<OtpDB> queryResult = (List<OtpDB>) localDAO.getEntityManager().createNamedQuery("findByUserIdAndRefCode")
					.setParameter("userId", credentialId)
					.setParameter("refCode", refCode)
					.getResultList();
			
			if (queryResult.size()==1) return queryResult.get(0);
			else return null;
		} catch ( Exception e ) {
			logger.error("Invalid otp loading with credentialId:"+credentialId +" and refCode:" + refCode , e);
		} finally {
			localDAO.close();
		}
		return null;
	}
	
	public void updateOtpStatus ( OtpDB otp, int status ) {
		try {
			localDAO.begin();
			OtpDB otpDb = localDAO.getEntityManager().find(OtpDB.class, otp.getId());
			
			if (otpDb != null) {
				otpDb.setStatus(status);
				localDAO.getEntityManager().persist(otpDb);
				localDAO.getTransaction().commit();
			} else {
				// update failed
			}
			
		} catch ( Exception e ) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			logger.error("Invalid otp update for optId:"+otp.getId() , e);
		} finally {
			localDAO.close();
		}
	}
}
