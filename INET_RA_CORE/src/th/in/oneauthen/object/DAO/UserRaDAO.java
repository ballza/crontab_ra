package th.in.oneauthen.object.DAO;

import java.util.List;

import th.athichitsakul.dao.impl.StandardDAOImpl;
import th.in.oneauthen.object.RequestCertDB;
import th.in.oneauthen.object.UserRaDB;
import th.in.oneauthen.object.UserUidDB;

public class UserRaDAO extends StandardDAOImpl<UserRaDB> {

	public UserRaDAO() {
		super("inetra");
		// TODO Auto-generated constructor stub
	}

	public UserRaDB findUsername(String username) {
		UserRaDB user = null;
		try {
			localDAO.begin();
			user = (UserRaDB) localDAO.getEntityManager().createNamedQuery("QueryUsername")
					.setParameter("userName", username).getSingleResult();
		} catch (Exception e) {
			// logger.error("Invalid user loading with name "+username , e);
		} finally {
			localDAO.close();
		}
		return user;
	}

	public void updateCountCert(UserRaDB updateData) {
		try {
			localDAO.begin();
			UserRaDB db = localDAO.getEntityManager().find(UserRaDB.class, updateData.getUserId());

			// == do update ==
			db.setCountCert((updateData.getCountCert()));
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for
			// "+updateData.getUserName(),e);
		} finally {
			localDAO.close();
		}

	}
	

	public void updateTelephone(UserRaDB updateData) {
		try {
			localDAO.begin();
			UserRaDB db = localDAO.getEntityManager().find(UserRaDB.class, updateData.getUserId());

			// == do update ==
			db.setTelephone(updateData.getTelephone());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for
			// "+updateData.getUserName(),e);
		} finally {
			localDAO.close();
		}

	}

	public UserRaDB findUserByOneIdUid(String oneId_uid) {
		UserRaDB user = null;
		try {
			localDAO.begin();
			user = (UserRaDB) localDAO.getEntityManager().createNamedQuery("findBySAMLToken").setParameter("samlTokenUid", oneId_uid)
					.getSingleResult();
		} catch (Exception e) {
//			logger.error("Invalid user loading with samlTokenUid =  " + oneId_uid, e);
		} finally {
			localDAO.close();
		}
		return user;
	}
	
	public List<UserRaDB> adminFind(int limitEnd) {
		List<UserRaDB> user = null;
		try {
			localDAO.begin();
			user = (List<UserRaDB>) localDAO.getEntityManager().createNamedQuery("QueryUserRa").setMaxResults(limitEnd)
					.setFirstResult(1).getResultList();
		} catch (Exception e) {
			//logger.error("Invalid user loading with type id = ", e);
		} finally {
			localDAO.close();
		}
		return user;
	}
	


	public List<UserRaDB> QueryUser(int limit) {
		List<UserRaDB> user = null;
		try {
			localDAO.begin();
			user = (List<UserRaDB>) localDAO.getEntityManager().createNamedQuery("queryUser").setMaxResults(limit)
					.getResultList();
		} catch (Exception e) {
			// logger.error("Invalid user loading with type id = ", e);
		} finally {
			localDAO.close();
		}
		return user;
	}

	public void updateTimeLogin(UserRaDB updateData) {

		try {
			localDAO.begin();
			UserRaDB db = localDAO.getEntityManager().find(UserRaDB.class, updateData.getUserId());

			// == do update ==
			db.setLastLogin(updateData.getLastLogin());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for " +
			// updateData.getUserName(), e);
		} finally {
			localDAO.close();
		}
	}
	
	public void updateTelephoneuser(UserRaDB updateData) {

		try {
			localDAO.begin();
			UserRaDB db = localDAO.getEntityManager().find(UserRaDB.class, updateData.getUserId());

			// == do update ==
			db.setLastLogin(updateData.getLastLogin());
			db.setTelephone(updateData.getTelephone());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for " +
			// updateData.getUserName(), e);
		} finally {
			localDAO.close();
		}
	}
	
	public void updateLastLoginAndSamlInfo(UserRaDB updateData) {
		localDAO.begin();
		try {
			UserRaDB db = localDAO.getEntityManager().find(UserRaDB.class, updateData.getUserId());
			// == do update ==
			db.setSamlTokenUid(updateData.getSamlTokenUid());
			db.setLastLogin(updateData.getLastLogin());
			db.setTelephone(updateData.getTelephone());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
		} finally {
			localDAO.close();
		}
	}
	
//	public void updatePwd(UserRaDB updateData) {
//
//		try {
//			localDAO.begin();
//			UserRaDB db = localDAO.getEntityManager().find(UserRaDB.class, updateData.getUserId());
//
//			// == do update ==
//			db.setPwd(updateData.getPwd());
//			localDAO.getEntityManager().merge(db);
//			localDAO.getTransaction().commit();
//		} catch (Exception e) {
//			if (localDAO.getTransaction().isActive()) {
//				localDAO.getTransaction().rollback();
//			}
//			// logger.error("Update last login failed for " +
//			// updateData.getUserName(), e);
//		} finally {
//			localDAO.close();
//		}
//	}
//	
//	public UserRaDB findSpUserByCertSHA256Fp(String clientCertFP) {
//		
//		UserRaDB user = null;
//		try {
//			localDAO.begin();
//			user = (UserRaDB) localDAO.getEntityManager().createNamedQuery("findCertFP")
//					.setParameter("clientCertFP", clientCertFP).getSingleResult();
//		} catch (Exception e) {
//		//	logger.error("Invalid user loading with clientCertFP " + clientCertFP, e);
//		} finally {
//			localDAO.close();
//		}
//		return user;
//		
//	}
	
	public void updateType(UserRaDB updateData) {
		try {
			localDAO.begin();
			UserRaDB db = localDAO.getEntityManager().find(UserRaDB.class, updateData.getUserId());

			// == do update ==
			db.setType(updateData.getType());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for
			// "+updateData.getUserName(),e);
		} finally {
			localDAO.close();
		}

	}
	
	public void updateSamLToken(UserRaDB updateData) {
		try {
			localDAO.begin();
			UserRaDB db = localDAO.getEntityManager().find(UserRaDB.class, updateData.getUserId());

			// == do update ==
			db.setSamlTokenUid(updateData.getSamlTokenUid());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for
			// "+updateData.getUserName(),e);
		} finally {
			localDAO.close();
		}

	}


}
