package th.in.oneauthen.object.DAO;

import th.athichitsakul.dao.impl.StandardDAOImpl;
import th.in.oneauthen.object.AdminDB;
import th.in.oneauthen.object.UserRaDB;


public class AdminDAO extends StandardDAOImpl<AdminDB> {

	public AdminDAO() {
		super("inetra");
		// TODO Auto-generated constructor stub
	}

	
	
	public AdminDB findUsername(String username) {
		AdminDB user = null;
		try {
			localDAO.begin();
			user = (AdminDB) localDAO.getEntityManager().createNamedQuery("QueryAdminName")
					.setParameter("userName", username).getSingleResult();
		} catch (Exception e) {
			// logger.error("Invalid user loading with name "+username , e);
		} finally {
			localDAO.close();
		}
		return user;
	}
	
	public AdminDB ThumbPrint(String Thumbprint) {
		AdminDB user = null;
		try {
			localDAO.begin();
			user = (AdminDB) localDAO.getEntityManager().createNamedQuery("QueryThumbprint")
					.setParameter("Thumbprint", Thumbprint).getSingleResult();
		} catch (Exception e) {
			// logger.error("Invalid user loading with name "+username , e);
		} finally {
			localDAO.close();
		}
		return user;
	}
	
	
	public void updateTimeLogin(AdminDB updateData) {

		try {
			localDAO.begin();
			AdminDB db = localDAO.getEntityManager().find(AdminDB.class, updateData.getAdminId());

			// == do update ==
			db.setLastLogin(updateData.getLastLogin());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for " +
			// updateData.getUserName(), e);
		} finally {
			localDAO.close();
		}
	}
}
