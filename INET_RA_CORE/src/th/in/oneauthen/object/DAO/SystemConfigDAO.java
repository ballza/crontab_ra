package th.in.oneauthen.object.DAO;

import th.athichitsakul.dao.impl.StandardDAOImpl;
import th.in.oneauthen.object.SystemConfigDB;
import th.in.oneauthen.object.UserRaDB;

public class SystemConfigDAO extends StandardDAOImpl<SystemConfigDB>{

	public SystemConfigDAO() {
		super("inetra");
		// TODO Auto-generated constructor stub
	}
	
	
	public void editConf(SystemConfigDB updateData) {
		try {
			localDAO.begin();
			SystemConfigDB db = localDAO.getEntityManager().find(SystemConfigDB.class, updateData.getKey());

			// == do update ==
			db.setValue((updateData.getValue()));
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for
			// "+updateData.getUserName(),e);
		} finally {
			localDAO.close();
		}
	}
	
	public String getConfig ( String key, String defaultValue ) {
		try{
			localDAO.begin();
			SystemConfigDB result = localDAO.getEntityManager().find( SystemConfigDB.class, key );
			if ( result != null )
				return result.getValue();
			else 
				return defaultValue;
		}catch(Exception e){
			return defaultValue;
		}finally{
			localDAO.close();
		}
	}


}
