package th.in.oneauthen.object.DAO;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import th.athichitsakul.dao.impl.StandardDAOImpl;
import th.in.oneauthen.object.CertificateDB;
import th.in.oneauthen.object.RequestCertDB;
import th.in.oneauthen.object.UserRaDB;
import th.in.oneauthen.object.UserUidDB;

public class RequestCertDAO extends StandardDAOImpl<RequestCertDB> {

	public RequestCertDAO() {
		super("inetra");
		// TODO Auto-generated constructor stub
	}

	public List<RequestCertDB> QueryForUpdateStatus() {
		List<RequestCertDB> user = null;
		try {
			localDAO.begin();
			user = (List<RequestCertDB>) localDAO.getEntityManager().createNamedQuery("ForUpdateStatus")
					.getResultList();
		} catch (Exception e) {
			// logger.error("Invalid user loading with type id = ", e);
		} finally {
			localDAO.close();
		}
		return user;
	}

	public RequestCertDB findByUniqueID(String uniqueID) {
		RequestCertDB historyTotal = null;
		try {

			localDAO.begin();
			historyTotal = (RequestCertDB) localDAO.getEntityManager().createNamedQuery("reqUniqueID")
					.setParameter("uniqueID", uniqueID).getSingleResult();

		} catch (Exception e) {
			// logger.error("Invalid query querySelfCert" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}
	
	public RequestCertDB findCertSnAndEnroll(String certSN) {
		RequestCertDB historyTotal = null;
		try {

			localDAO.begin();
			historyTotal = (RequestCertDB) localDAO.getEntityManager().createNamedQuery("reqCertSnAndEnroll")
					.setParameter("certSN", certSN).getSingleResult();

		} catch (Exception e) {
			// logger.error("Invalid query querySelfCert" , e);
		} finally {
			localDAO.close();
		}
		return historyTotal;
	}

	public void serviceSyncDB(RequestCertDB updateData) {
		try {
			localDAO.begin();
			RequestCertDB db = localDAO.getEntityManager().find(RequestCertDB.class, updateData.getId());

			// == do update ==
			db.setCertSn((updateData.getCertSn()));
			db.setX509(updateData.getX509());
			db.setStatus(updateData.getStatus());
			db.setRevokeReason(updateData.getRevokeReason());
			db.setReject(updateData.getReject());
			db.setTimeApprove(updateData.getTimeApprove());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			e.printStackTrace();
			// logger.error("Update last login failed for "+updateData.getUserName(),e);
		} finally {
			localDAO.close();
		}

	}

	public List<RequestCertDB> QueryCertStatus2() {
		List<RequestCertDB> user = null;
		try {
			localDAO.begin();
			user = (List<RequestCertDB>) localDAO.getEntityManager().createNamedQuery("updateCertStatus2")
					.getResultList();
		} catch (Exception e) {
		} finally {
			localDAO.close();
		}
		return user;
	}

	public void UpdateStatusByUnique(RequestCertDB updateData) {
		
		try {
			localDAO.begin();
			RequestCertDB db = localDAO.getEntityManager().find(RequestCertDB.class, updateData.getId());

			// == do update ==

			db.setStatus(updateData.getStatus());
			db.setCertSn(updateData.getCertSn());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for "+updateData.getUserName(),e);
		} finally {
			localDAO.close();
		}

	}

	public List<RequestCertDB> getSelfRequest(int creator) {
		List<RequestCertDB> Cert = null;
		try {
			UserRaDB user = new UserRaDAO().find(creator);
			if (user == null) {
				return null;
			} else {
				localDAO.begin();
				Cert = (List<RequestCertDB>) localDAO.getEntityManager().createNamedQuery("SelfReq")
						.setParameter("User", user).getResultList();
			}

		} catch (Exception e) {
			e.printStackTrace();
			// logger.error("Invalid user loading with type id = ", e);
		} finally {
			localDAO.close();
		}
		return Cert;
	}
	
	public List<RequestCertDB> getRequestManager(int careTaker) {
		List<RequestCertDB> Cert = null;
		try {
				localDAO.begin();
				Cert = (List<RequestCertDB>) localDAO.getEntityManager().createNamedQuery("ManagerReq")
						.setParameter("careTaker", careTaker).getResultList();

		} catch (Exception e) {
			e.printStackTrace();
			// logger.error("Invalid user loading with type id = ", e);
		} finally {
			localDAO.close();
		}
		return Cert;
	}
	
	public void insertCSR(RequestCertDB updateData) throws Exception {
		try {
			localDAO.begin();
			RequestCertDB db = localDAO.getEntityManager().find(RequestCertDB.class, updateData.getId());
			// == do update ==
			if (db == null)
				throw new NullPointerException("Reqeust data not found");

			
				db.setX509(updateData.getX509());
				db.setStatus(updateData.getStatus());
				localDAO.getEntityManager().merge(db);
				localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			throw e;
		} finally {
			localDAO.close();
		}

	}
	
	public void UpdateCSR(RequestCertDB updateData) throws Exception {
		
		try {
			localDAO.begin();
			RequestCertDB db = localDAO.getEntityManager().find(RequestCertDB.class, updateData.getId());

			// == do update ==

			db.setX509(updateData.getX509());
			db.setStatus(updateData.getStatus());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for "+updateData.getUserName(),e);
			throw e;
		} finally {
			localDAO.close();
		}

	}
	
public void UpdateStatus(RequestCertDB updateData) throws Exception {
		
		try {
			localDAO.begin();
			RequestCertDB db = localDAO.getEntityManager().find(RequestCertDB.class, updateData.getId());

			// == do update ==
			db.setStatus(updateData.getStatus());
			localDAO.getEntityManager().merge(db);
			localDAO.getTransaction().commit();
		} catch (Exception e) {
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			// logger.error("Update last login failed for "+updateData.getUserName(),e);
			throw e;
		} finally {
			localDAO.close();
		}

	}

}
