package th.in.oneauthen.object;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="test")
public class testDB implements Serializable {
	private static final long serialVersionUID = -1243742509515363563L;

	@Id
	@Column(name = "COL_1", unique=true, nullable=false)
	private String col_1;
	@Column(name = "COL_2" ,nullable=false)
	private String col_2 ;
	public String getCol_1() {
		return col_1;
	}
	public void setCol_1(String key) {
		this.col_1 = col_1;
	}
	public String getCol_2() {
		return col_2;
	}
	public void setCol_2(String value) {
		this.col_2 = col_2;
	}
}
