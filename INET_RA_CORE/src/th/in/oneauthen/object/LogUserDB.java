package th.in.oneauthen.object;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="loguser")
@GenericGenerator(name ="idGenerator",strategy="native")
@NamedQueries(value = {
			@NamedQuery(name="getInDate", query="SELECT u FROM LogUserDB u where DATE(logTime) = :dateFind "),
			@NamedQuery(name="getUserInDate", query="SELECT u FROM LogUserDB u where  u.creator = :user "),
			@NamedQuery(name="getBetweenDate", query="SELECT u FROM LogUserDB u where DATE(logTime) BETWEEN :dateFrist AND :dateEnd "),
			@NamedQuery(name="getInDateStatus", query="SELECT u FROM LogUserDB u where DATE(logTime) = :dateFind AND u.logStatus = :status "),
			@NamedQuery(name="getInDateEvent", query="SELECT u FROM LogUserDB u where  u.logAction = :event AND DATE(logTime) = :dateFind "),
		})
public class LogUserDB implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6664418538186840059L;
	

	@Id
	@GeneratedValue(generator="idGenerator", strategy=GenerationType.AUTO) @Column(name ="ID",nullable = false)
	private int log_ID;
	@Column(name = "LOG_TIME", nullable = false)
	private Date logTime;
	@Column(name = "LOG_DETAIL", nullable = false)
	private String logDetail;
	@Column(name = "LOG_PAGE", nullable = false)
	private String logPage;
	@Column(name = "LOG_ACTION", nullable = false)
	private String logAction;
	@Column(name = "LOG_STATUS", nullable = false)
	private String logStatus;
	
	@ManyToOne
	@JoinColumn(name = "CREATOR" )
	private UserRaDB creator;



	
	
	public String getLogStatus() {
		return logStatus;
	}

	public void setLogStatus(String logStatus) {
		this.logStatus = logStatus;
	}

	public String getLogPage() {
		return logPage;
	}

	public void setLogPage(String logPage) {
		this.logPage = logPage;
	}

	public String getLogAction() {
		return logAction;
	}

	public void setLogAction(String logAction) {
		this.logAction = logAction;
	}

	public int getLog_ID() {
		return log_ID;
	}

	public void setLog_ID(int log_ID) {
		this.log_ID = log_ID;
	}

	public Date getLogTime() {
		return logTime;
	}

	public void setLogTime(Date logTime) {
		this.logTime = logTime;
	}

	public String getLogDetail() {
		return logDetail;
	}

	public void setLogDetail(String logDetail) {
		this.logDetail = logDetail;
	}

	public UserRaDB getCreator() {
		return creator;
	}

	public void setCreator(UserRaDB creator) {
		this.creator = creator;
	}
	
	
	
}