package th.in.oneauthen.object;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import th.in.oneauthen.object.UserRaDB;

@Entity
@Table(name="certificate")
@GenericGenerator(name ="idGenerator",strategy="native")
@NamedQueries(value = { 
		@NamedQuery(name="FindByUniqueIDCert", query="SELECT u FROM CertificateDB u WHERE u.unique_id = :uniqueID"),
		@NamedQuery(name="findBySn", query="SELECT u FROM CertificateDB u WHERE u.cert_Sn = :certSn"),
		@NamedQuery(name="findSelfCert", query="SELECT u FROM CertificateDB u WHERE u.creator = :User order by u.cert_ID desc"),
		@NamedQuery(name="revoke", query="SELECT u FROM CertificateDB u WHERE u.cert_Sn = :CertSn and u.cert_Type is not NULL"),
		@NamedQuery(name="SelfCertRequest", query="SELECT u FROM CertificateDB u WHERE u.creator = :User and(u.cert_Type is NULL OR u.cert_Sn is NULL)"),
		@NamedQuery(name="Check10Day", query="SELECT u FROM CertificateDB u WHERE u.accept_time is NULL OR u.status = '5'"),
		@NamedQuery(name="findManagerCert", query="SELECT u FROM CertificateDB u WHERE u.care_taker = :careTaker order by u.cert_ID desc"),
		@NamedQuery(name="findCertFromStatus", query="SELECT u FROM CertificateDB u WHERE u.status = :statusCert"),
	})
public class CertificateDB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 258681168733361079L;
	
	@Id
	@GeneratedValue(generator="idGenerator", strategy=GenerationType.AUTO) @Column(name ="ID",nullable = false)
	private int cert_ID;
	@Column(name = "CERT_TYPE", nullable = false)
	private String cert_Type;
	
	@ManyToOne
	@JoinColumn(name = "CREATOR" , nullable = false)
	private UserRaDB creator;
	
	@JoinColumn(name = "CARE_TAKER" , nullable = false)
	private Integer care_taker;
	
	@Column(name = "CERT_SN")
	private String cert_Sn;
	@Column(name = "STATUS", nullable = false)
	private String status;
	@Column(name = "CERT_X509")
	private String cert_x509;
	
	@Column(name = "UNIQUE_ID", nullable = false)
	private String  unique_id;
	
	@Column(name = "ACCEPT_TIME")
	private Date accept_time;
	
	@Column(name = "EXPIRED_DATE", nullable = false)
	private Date expiredDate;
	
	@Column(name = "REVOKE_REASON")
	private String  revokeReason;
	
	@Column(name = "DETAIL")
	private String  detail;
	

	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public Date getExpiredDate() {
		return expiredDate;
	}
	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}
	public String getRevokeReason() {
		return revokeReason;
	}
	public void setRevokeReason(String revokeReason) {
		this.revokeReason = revokeReason;
	}
	public Date getAccept_time() {
		return accept_time;
	}
	public void setAccept_time(Date accept_time) {
		this.accept_time = accept_time;
	}
	public String getUnique_id() {
		return unique_id;
	}
	public void setUnique_id(String unique_id) {
		this.unique_id = unique_id;
	}
	public int getCert_ID() {
		return cert_ID;
	}
	public void setCert_ID(int cert_ID) {
		this.cert_ID = cert_ID;
	}
	public String getCert_Type() {
		return cert_Type;
	}
	public void setCert_Type(String cert_Type) {
		this.cert_Type = cert_Type;
	}
	public UserRaDB getCreator() {
		return creator;
	}
	public void setCreator(UserRaDB creator) {
		this.creator = creator;
	}
	public String getCert_Sn() {
		return cert_Sn;
	}
	public void setCert_Sn(String cert_Sn) {
		this.cert_Sn = cert_Sn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCert_x509() {
		return cert_x509;
	}
	public void setCert_x509(String cert_x509) {
		this.cert_x509 = cert_x509;
	}
	public Integer getCare_taker() {
		return care_taker;
	}
	public void setCare_taker(Integer care_taker) {
		this.care_taker = care_taker;
	}
	
	


}
