package th.in.oneauthen.object;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="user_admin")
@GenericGenerator(name ="idGenerator",strategy="native")
@NamedQueries(value = { 
		@NamedQuery(name="QueryAdminName", query="SELECT u FROM AdminDB u WHERE u.userName = :userName"),
		@NamedQuery(name="QueryAdminS", query="FROM AdminDB"),
		@NamedQuery(name="QueryThumbprint", query="SELECT u FROM AdminDB u WHERE u.thumbPrint = :Thumbprint"),
	})
public class AdminDB implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9221836194444403692L;
	
	
	@Id
	@GeneratedValue(generator="idGenerator", strategy=GenerationType.AUTO) @Column(name ="ID", nullable = false)
	private int adminId;
	@Column(name = "USERNAME", unique=true, nullable = false)
	private String userName;
	@Column(name = "THUMB_PRINT", unique=true, nullable = false)
	private String thumbPrint;
	@Column(name = "X509", nullable = false)
	private String x509;
	@Column(name = "LAST_LOGIN")
	private Date lastLogin;

	
	
	
	public String getThumbPrint() {
		return thumbPrint;
	}
	public void setThumbPrint(String thumbPrint) {
		this.thumbPrint = thumbPrint;
	}
	public String getX509() {
		return x509;
	}
	public void setX509(String x509) {
		this.x509 = x509;
	}
	public int getAdminId() {
		return adminId;
	}
	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	
	
	

}
