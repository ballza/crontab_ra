package th.in.oneauthen.object;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name="info_user")
@GenericGenerator(name ="idGenerator",strategy="native")
@NamedQueries(value = { 
		@NamedQuery(name="QueryCreator", query="SELECT u FROM InfoUserDB u WHERE u.creator = :creator"),
	})
public class InfoUserDB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1243742509515363563L;

	@Id
	@GeneratedValue(generator="idGenerator", strategy=GenerationType.AUTO) @Column(name ="INFO_ID",nullable = false)
	private String infoID;
	@Column(name = "MOBILE")
	private String mobile ;
	@Column(name = "PERSONAL_INFO")
	private String personalInfo;
	@Column(name = "BIZ_INFO")
	private String bizInfo;
	@ManyToOne
	@JoinColumn(name = "CREATOR" , nullable = false)
	private UserRaDB creator;
	public String getInfoID() {
		return infoID;
	}
	public void setInfoID(String infoID) {
		this.infoID = infoID;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPersonalInfo() {
		return personalInfo;
	}
	public void setPersonalInfo(String personalInfo) {
		this.personalInfo = personalInfo;
	}
	public String getBizInfo() {
		return bizInfo;
	}
	public void setBizInfo(String bizInfo) {
		this.bizInfo = bizInfo;
	}
	public UserRaDB getCreator() {
		return creator;
	}
	public void setCreator(UserRaDB creator) {
		this.creator = creator;
	}
	

	
}
