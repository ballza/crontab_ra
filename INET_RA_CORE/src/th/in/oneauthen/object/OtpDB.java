package th.in.oneauthen.object;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "OTP")
@NamedQueries(value = {
		@NamedQuery(name = "findByUserId", query = "SELECT otp FROM OtpDB otp WHERE otp.userId = :userId AND otp.status = 1 AND otp.type=3"),
		@NamedQuery(name = "findByUserIdAndRefCode", query = "SELECT otp FROM OtpDB otp WHERE otp.userId = :userId AND otp.refCode = :refCode AND otp.status = 1") })
public class OtpDB {
	@Id
	@GeneratedValue @Column(name ="ID")
	private int id;
	@Column(name = "REF_CODE")
	private String refCode;
	@Column(name = "REF_TIME")
	private long t0;
	@Column(name = "SEED_VALUE")
	private String seed;
	@Column(name = "USER_ID")
	private int userId;
	@Column(name = "STATUS")
	private int status;
	@Column(name = "TYPE")
	private int type;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRefCode() {
		return refCode;
	}

	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}

	public long getT0() {
		return t0;
	}

	public void setT0(long t0) {
		this.t0 = t0;
	}

	public String getSeed() {
		return seed;
	}

	public void setSeed(String seed) {
		this.seed = seed;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
