package th.in.oneauthen.object;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="document")
@GenericGenerator(name ="idGenerator",strategy="native")

public class DocumentDB  implements Serializable {

 private static final long serialVersionUID = 258681168733361079L;
 
 @Id
 @GeneratedValue(generator="idGenerator", strategy=GenerationType.AUTO) @Column(name ="DOCUMENT_ID",nullable = false)
 private Integer documentId;
 @Column(name = "TEMPLATE_PATH", nullable = false)
 private String templatePath;
 @Column(name = "COUNT", nullable = false)
 private Integer count;
 
 


 public Integer getDocumentId() {
	return documentId;
}
public void setDocumentId(Integer documentId) {
	this.documentId = documentId;
}
public String getTemplatePath() {
  return templatePath;
 }
 public void setTemplatePath(String templatePath) {
  this.templatePath = templatePath;
 }
public Integer getCount() {
	return count;
}
public void setCount(Integer count) {
	this.count = count;
}

 
 
}