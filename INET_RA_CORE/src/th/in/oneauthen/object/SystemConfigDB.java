package th.in.oneauthen.object;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="configuration")
public class SystemConfigDB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1243742509515363563L;

	@Id
	@Column(name = "CONFIG_KEY", unique=true, nullable=false)
	private String key;
	@Column(name = "CONFIG_VALUE" ,nullable=false)
	private String value ;
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
