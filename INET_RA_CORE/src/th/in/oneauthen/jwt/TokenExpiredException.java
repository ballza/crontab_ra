package th.in.oneauthen.jwt;

public class TokenExpiredException extends Exception {
	public TokenExpiredException(String string) {
		super (string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1579524159948958327L;
}
