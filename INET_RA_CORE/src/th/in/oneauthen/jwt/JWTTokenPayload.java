package th.in.oneauthen.jwt;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.util.encoders.Base64;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.InvalidJwtSignatureException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import th.in.oneauthen.object.UserRaDB;
import th.in.oneauthen.object.DAO.SystemConfigDAO;

public class JWTTokenPayload {
	private String audience;
	private String jwtId;
	private Date issueTime;
	private Date notBefore;
	private Date notAfter;
	private String subject;
	private String username;
	private String[] scopes;
	private String authenInformation;
	private String rawToken;
	private UserRaDB userInfo;
	
	private boolean verfied;
	
	private static PublicKey VERIFICATION_KEY;
	private static final String VER_KEY_KEY = "BEARER_PUBLIC_KEY";
	
	// Share token public key patch D_2020_03_25
	private static final String DEFAULT_PUB = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4qyXOIqcyF0zBOyZTOItbVhUQ+frwlpkUW2+mfH+7d0WxaW9lGIWTHKkMM+sCdCpQ6Y0eHECj+1iadZR/8LZbH8r8zwfAEhXwGBVtXN0XcGKYwBjmH0iGhGZPMALetCuyRaXpOTbJUps4P6QQkr/xONJWtQP7n9vXCBCRIlgUie3vh2nOBl/OLg3xgNdEdCrMJlxcUa3J/EyXyde6Ougqgvs//qefdQCkM0txRmRzk81811d/ucORgYlk/0+MksxElP5gHAdOfqtsG4bDgSrKBlpAJH4mp3Y2BTsaIHGw0gGdgMRgZosqkIOaKMOOTHV4cL7pm5opX9vhUUwhw5idQIDAQAB";

	private JWTTokenPayload() throws Exception {
		if (JWTTokenPayload.VERIFICATION_KEY==null) {
			String pubKey = new SystemConfigDAO().getConfig(VER_KEY_KEY, DEFAULT_PUB);
			X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(pubKey.getBytes()));
			KeyFactory kf = KeyFactory.getInstance("RSA");
			PublicKey publicKey = kf.generatePublic(spec);
			
			if (publicKey!=null) {
				JWTTokenPayload.VERIFICATION_KEY=publicKey;
			}
		}
	}

	public static JWTTokenPayload getInstance(String jwtToken) throws Exception {
		JWTTokenPayload token = new JWTTokenPayload();
		try {
			token.initialVerifiedToken(jwtToken);
		} catch (InvalidJwtSignatureException e) {
			token.initialUnVerifiedToken(jwtToken);
		}
		return token;
	}
	
	public void initialVerifiedToken(String jwtToken) throws Exception {
		long start = System.currentTimeMillis();
		JwtClaims jwtClaims = null;
		try {
			JwtConsumer jwtParser = new JwtConsumerBuilder()
					.setSkipAllDefaultValidators()
					.setVerificationKey(JWTTokenPayload.VERIFICATION_KEY)
					.build();
			jwtClaims = jwtParser.processToClaims(jwtToken);

			this.setAudience(jwtClaims.getAudience().get(0));
			if (StringUtils.isEmpty(this.getAudience())) {
				throw new NullPointerException("JWT Token audience not found");
			}
		} catch (InvalidJwtException | MalformedClaimException e) {
			throw e;
		}
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode payload = mapper.readTree(jwtClaims.getRawJson());
		this.username = payload.get("username").asText();
		this.verfied = true;

		this.setJwtId(jwtClaims.getJwtId());
		if (StringUtils.isEmpty(this.getJwtId())) {
			throw new NullPointerException("JWT Token id not found");
		}

		this.setSubject(jwtClaims.getSubject());
		if (StringUtils.isEmpty(this.getSubject())) {
			throw new NullPointerException("JWT Token subject not found");
		}
		
		this.rawToken = jwtToken;
		this.setNotBefore(new Date(jwtClaims.getNotBefore().getValueInMillis()));
		this.setNotAfter(new Date(jwtClaims.getExpirationTime().getValueInMillis()));
		this.setIssueTime(new Date(jwtClaims.getIssuedAt().getValueInMillis()));
		System.out.println("Initial jwt verification complete in "+(System.currentTimeMillis() - start));
	}
	
	public void initialUnVerifiedToken(String jwtToken) throws Exception {
		JwtClaims jwtClaims = null;
		try {
			JwtConsumer jwtParser = new JwtConsumerBuilder()
					.setSkipAllValidators()
					.setDisableRequireSignature()
					.setSkipSignatureVerification().build();
			jwtClaims = jwtParser.processToClaims(jwtToken);

			this.setAudience(jwtClaims.getAudience().get(0));
			if (StringUtils.isEmpty(this.getAudience())) {
				throw new NullPointerException("JWT Token audience not found");
			}
		} catch (InvalidJwtException | MalformedClaimException e) {
			throw new Exception("Invalid JWT Token data.", e);
		}

		this.setJwtId(jwtClaims.getJwtId());
		if (StringUtils.isEmpty(this.getJwtId())) {
			throw new NullPointerException("JWT Token id not found");
		}

		this.setSubject(jwtClaims.getSubject());
		if (StringUtils.isEmpty(this.getSubject())) {
			throw new NullPointerException("JWT Token subject not found");
		}
		
		this.rawToken = jwtToken;
		this.setNotBefore(new Date(jwtClaims.getNotBefore().getValueInMillis()));
		this.setNotAfter(new Date(jwtClaims.getExpirationTime().getValueInMillis()));
		this.setIssueTime(new Date(jwtClaims.getIssuedAt().getValueInMillis()));
	}

	public String getAudience() {
		return audience;
	}

	public void setAudience(String audience) {
		this.audience = audience;
	}

	public String getJwtId() {
		return jwtId;
	}

	public void setJwtId(String jwtId) {
		this.jwtId = jwtId;
	}

	public Date getIssueTime() {
		return issueTime;
	}

	public void setIssueTime(Date issueTime) {
		this.issueTime = issueTime;
	}

	public Date getNotBefore() {
		return notBefore;
	}

	public void setNotBefore(Date notBefore) {
		this.notBefore = notBefore;
	}

	public Date getNotAfter() {
		return notAfter;
	}

	public void setNotAfter(Date notAfter) {
		this.notAfter = notAfter;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String[] getScopes() {
		return scopes;
	}

	public void setScopes(String[] scopes) {
		this.scopes = scopes;
	}

	public String getAuthenInformation() {
		return authenInformation;
	}

	public void setAuthenInformation(String authenInformation) {
		this.authenInformation = authenInformation;
	}

	public String getJwtEncoded() {
		return rawToken;
	}

	public UserRaDB getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserRaDB userInfo) {
		this.userInfo = userInfo;
	}
	
	public String getUsername () {
		return this.username;
	}

	public boolean isVerfied() {
		return verfied;
	}

//	public static void main(String[] args) throws Exception {
//		String oriBearer = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQ4YWU1OTY4N2ZmNTg5Yzg3OTFmYTRiZDZiYmE1YTM5YTQxYzliNGM2OTk3OWU1YmY3NzBiMGJlODRmZGFkZWI4MDJmOTM3ZTI3YmJhMjMwIn0.eyJhdWQiOiIzOSIsImp0aSI6IjQ4YWU1OTY4N2ZmNTg5Yzg3OTFmYTRiZDZiYmE1YTM5YTQxYzliNGM2OTk3OWU1YmY3NzBiMGJlODRmZGFkZWI4MDJmOTM3ZTI3YmJhMjMwIiwiaWF0IjoxNTg1MjI0OTkwLCJuYmYiOjE1ODUyMjQ5OTAsImV4cCI6MTU4NTMxMTM5MCwic3ViIjoiMzEwNDcxODMyMiIsInNjb3BlcyI6W119.PTob19UComU-G9xwIr6yjIX6u3nwJ75fFe8GCjIN-ieg2e6icDU-y9GxY6i_lYfsc-gCjLz-LL8z1oMXTQGtNKCz7AbYpY1dRk8UYxCFo4TcyVQzmUEliTY_ztvfdWdfG8SYMueqKed0oKQoXjIw9D6Ljn438kMZdKzFWgcjwMM3p3rPkD0kuhhCrz0bhR-amXTmGx_cKvEzYp7ZCdmeAWQJr-RUbByO3gZLomVSC_yAz0yuFIrRdmURtwFkR0CHTBYiWWnEFwbxSoUyJ5VKVgRQj7cCIwE4fD3hZMu6CY2ljk51yB3CzlsVmYXZXglxrFJTVXcIdRMBPI0Mjs0zL9vp89lAwMgqIWVzqZS2cqjUfI7PiJ8O7K5D3UFO-P3_toC37cVBrb2uMagyupSqMZApxwhu9hPGiYrPfWMRfkU3Wk6bu4FjZXr23ShoXm3vCTNU-VA6Mk6OFZ2ACEEEd9NvgCDnSurVgH0RnhGb4hdgdTO9eBFbxGbrJJbzFlI76UUXT8drngTLLajGnA2FheyakoZVY4FAXX9AXzS-16p8-ttl-3hKhBDLJPsXQtorDN1Dm4dp5Rq8wNkMgKbsQRDA7t17x66OacQ5CgpeDSWpr_nn_1Lq7NIw8IUPwyysIntwp_Yug9DpMFg37wZys1a7S8dVQgxIo5GvVQqmfXg";
//		String bearer = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQ4YWU1OTY4N2ZmNTg5Yzg3OTFmYTRiZDZiYmE1YTM5YTQxYzliNGM2OTk3OWU1YmY3NzBiMGJlODRmZGFkZWI4MDJmOTM3ZTI3YmJhMjMwIiwia2lkIjoiIn0.eyJhdWQiOiIzOSIsImp0aSI6IjQ4YWU1OTY4N2ZmNTg5Yzg3OTFmYTRiZDZiYmE1YTM5YTQxYzliNGM2OTk3OWU1YmY3NzBiMGJlODRmZGFkZWI4MDJmOTM3ZTI3YmJhMjMwIiwiaWF0IjoxNTg1MjI1MDAyLCJuYmYiOjE1ODUyMjUwMDIsImV4cCI6MTU4NTIyNTMwMiwic3ViIjoiMzEwNDcxODMyMiIsInVzZXJuYW1lIjoieDIwYXRoaW5rcGFkIn0.LlVJ8bDv9oB624FBbATko7isdAx_vGBWcf1WVyaC8bMjAaa813hQ8kqp0KexHnVgF0zwVUEkf9c0-o81CLZr3lNMxaHpgaanZb6ES2PgpeMay7PAtQgfzGDBbyku6pCTUhX5Kzm-LuBmCWUZlZOAhRfoYO7UzHOKFKApvtEo27ISa1AXeiyL4hgfiabhR3AYjT-wg7uQJfVRkEiA6zxzYChDxAv5gV42bwx59t4JOcBFyYx1i5Uo9Jc38xnJUDct3RVSm1LkvwRM0fOUS7FtZ7ZoDj8vyPkyN9do2MuuiYGQ0p4S0run7dOlNli4hhUhzSBevqEVvz_jCjuZRQULPQ";
//		String maliciousBearer = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImZkODcwYjNjZDY5ZmNkNjYzNDBhOTUxMzZhOWI4YzUwYzI0Zjk1Yzg1NDVlMmU0NDM2MDJmMWViOTk1YmI0ZGE3ZjkzZTllNTRmYTU1MTJjIiwia2lkIjoiIn0.eyJhdWQiOiIzOSIsImp0aSI6ImZkODcwYjNjZDY5ZmNkNjYzNDBhOTUxMzZhOWI4YzUwYzI0Zjk1Yzg1NDVlMmU0NDM2MDJmMWViOTk1YmI0ZGE3ZjkzZTllNTRmYTU1MTJjIiwiaWF0IjoxNTg1MjE4MDg0LCJuYmYiOjE1ODUyMTgwODQsImV4cCI6MTU4NTIxODM4NCwic3ViIjoiMTIzNDU2Nzg5MCJ9.lws1mbLyOIRgRJ2ANbCpvrGPoDuSWOMrX85nM2aLg3joBegMEzeAVbmJmaN1TzIQjSnDNGTzRFu9ptAm2trpYGC34g6hHV_QxkGumttZNXdrloZS2uNjH9KhP8HwAuBvXnjMNmY-D_uPxhOYR9dNXZ3aoJeNI1A8qj2vUqQ7ua_9aqaXF7vcO1BAQDBNiiWh8ilVvEhFudrTb4mE6EZZQ1H8dImTuCqkLz4Sz5j-x8rlxX8wsCtz0EH_dDuQPjIpJab8ow-nusaFkcgwiuygjRAFZy7nsHXnY2mUHSxuWwHbtRPrVtKYaJE9DgngCC2W2jwMYI8jCGRT0dHD-MzYhA";
//		
//		JWTTokenPayload jwt = getInstance(maliciousBearer);
//		if(jwt.verfied) {
//			System.out.println(jwt.getUsername());
//		} else {
//			System.out.println(jwt.getSubject());
//		}
//		
////		String pubKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4qyXOIqcyF0zBOyZTOItbVhUQ+frwlpkUW2+mfH+7d0WxaW9lGIWTHKkMM+sCdCpQ6Y0eHECj+1iadZR/8LZbH8r8zwfAEhXwGBVtXN0XcGKYwBjmH0iGhGZPMALetCuyRaXpOTbJUps4P6QQkr/xONJWtQP7n9vXCBCRIlgUie3vh2nOBl/OLg3xgNdEdCrMJlxcUa3J/EyXyde6Ougqgvs//qefdQCkM0txRmRzk81811d/ucORgYlk/0+MksxElP5gHAdOfqtsG4bDgSrKBlpAJH4mp3Y2BTsaIHGw0gGdgMRgZosqkIOaKMOOTHV4cL7pm5opX9vhUUwhw5idQIDAQAB";
////
////		String maliciousBearer = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImZkODcwYjNjZDY5ZmNkNjYzNDBhOTUxMzZhOWI4YzUwYzI0Zjk1Yzg1NDVlMmU0NDM2MDJmMWViOTk1YmI0ZGE3ZjkzZTllNTRmYTU1MTJjIiwia2lkIjoiIn0.eyJhdWQiOiIzOSIsImp0aSI6ImZkODcwYjNjZDY5ZmNkNjYzNDBhOTUxMzZhOWI4YzUwYzI0Zjk1Yzg1NDVlMmU0NDM2MDJmMWViOTk1YmI0ZGE3ZjkzZTllNTRmYTU1MTJjIiwiaWF0IjoxNTg1MjE4MDg0LCJuYmYiOjE1ODUyMTgwODQsImV4cCI6MTU4NTIxODM4NCwic3ViIjoiMTIzNDU2Nzg5MCJ9.lws1mbLyOIRgRJ2ANbCpvrGPoDuSWOMrX85nM2aLg3joBegMEzeAVbmJmaN1TzIQjSnDNGTzRFu9ptAm2trpYGC34g6hHV_QxkGumttZNXdrloZS2uNjH9KhP8HwAuBvXnjMNmY-D_uPxhOYR9dNXZ3aoJeNI1A8qj2vUqQ7ua_9aqaXF7vcO1BAQDBNiiWh8ilVvEhFudrTb4mE6EZZQ1H8dImTuCqkLz4Sz5j-x8rlxX8wsCtz0EH_dDuQPjIpJab8ow-nusaFkcgwiuygjRAFZy7nsHXnY2mUHSxuWwHbtRPrVtKYaJE9DgngCC2W2jwMYI8jCGRT0dHD-MzYhA";
////
////		PublicKey publicKey = null;
////		X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.decode(pubKey.getBytes()));
////		KeyFactory kf = KeyFactory.getInstance("RSA");
////		publicKey = kf.generatePublic(spec);
////
////		JWTTokenPayload token = new JWTTokenPayload();
////		JwtClaims jwtClaims = null;
////		try {
////			// JwtConsumer jwtParser = new
////			// JwtConsumerBuilder().setSkipAllValidators().setDisableRequireSignature().setSkipSignatureVerification().build();
////			// jwtClaims = jwtParser.processToClaims(jwtToken);
////
////			JwtConsumer jwtParser = new JwtConsumerBuilder().setSkipAllDefaultValidators().setVerificationKey(publicKey)
////					.build();
////			jwtClaims = jwtParser.processToClaims(maliciousBearer);
////
////			token.setAudience(jwtClaims.getAudience().get(0));
////			if (StringUtils.isEmpty(token.getAudience())) {
////				throw new NullPointerException("JWT Token audience not found");
////			}
////
////			System.out.println(token.getAudience());
////		} catch (InvalidJwtException | MalformedClaimException e) {
////			throw new Exception("Invalid JWT Token data.", e);
////		}
//	}
}
