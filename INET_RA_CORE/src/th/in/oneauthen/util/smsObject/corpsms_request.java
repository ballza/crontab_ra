package th.in.oneauthen.util.smsObject;

import java.io.Serializable;
import java.io.StringWriter;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;

@SuppressWarnings("serial")
@XmlRootElement(name = "corpsms_request")
public class corpsms_request implements Serializable {
	private String tid; // Transaction ID (May be referenc code)
	private String header = "null";// Optional String length (1024 maximum) Only used for Push SI and Bookmark
									// message format
	private ArrayList<msisDn> recipients = new ArrayList<>(); // length (10-11 maximum) Recipient number in
																// international format (10 or 11digits). Recipient
																// Limit 1,000 .m

	private String mtype = "E"; // “T” = Thai “E” = English“W” = Push SI“B” = Bookmark Type of message format
	private String msg; // String length (1024 maximum) Content of message.
	private String sender = "OneAuthSMS"; // String length (11 maximum) Only Registered
	private String sendtime; // Optional String Only format(dd/MM/yyyy HH:mm)
	private String key;

	public corpsms_request() {
	}

	public corpsms_request(String receiverMobileNo, String message, String tid) {
		this.tid = tid;
		this.msg = message;
		msisDn target = new msisDn();
		target.msisdn = receiverMobileNo;
		this.recipients.add(target);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		this.sendtime = sdf.format(new Date());
		this.key = this.genSmsObjectKey(receiverMobileNo);
	}

	private String genSmsObjectKey(String msisdn) {
		String key = null;
		if (StringUtils.isNotEmpty(this.tid) && StringUtils.isNotEmpty(msisdn)) {
			try {
				String source = msisdn + this.tid;

				MessageDigest md = MessageDigest.getInstance("MD5");
				byte[] bytes = md.digest(source.getBytes());
				StringBuffer sb = new StringBuffer();

				for (int i = 0; i < bytes.length; i++) {
					byte b = bytes[i];
					String hex = Integer.toHexString(b & 0xFF);

					if (hex.length() < 2) {
						hex = "0" + hex;
					}
					sb.append(hex);
				}
				key = sb.toString();
			} catch (Exception e) {
				System.out.println("Generate Key Error !!");
				e.printStackTrace();
			}
		}
		return key;
	}

	public static class msisDn {
		public String msisdn;
	}

	public String getTid() {
		return tid;
	}

	public String getHeader() {
		return header;
	}

	public ArrayList<msisDn> getRecipients() {
		return recipients;
	}

	public String getMtype() {
		return mtype;
	}

	public String getMsg() {
		return msg;
	}

	public String getSender() {
		return sender;
	}

	public String getSendtime() {
		return sendtime;
	}

	public String getKey() {
		return key;
	}

	// ======================
	public void setTid(String tid) {
		this.tid = tid;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public void setRecipients(ArrayList<msisDn> recipients) {
		this.recipients = recipients;
	}

	public void setMtype(String mtype) {
		this.mtype = mtype;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public void setSendtime(String sendtime) {
		this.sendtime = sendtime;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String toString() {
		// Print XML String to Console
		StringWriter sw = new StringWriter();
		try {
			// Create JAXB Context
			JAXBContext jaxbContext = JAXBContext.newInstance(corpsms_request.class);
			// Create Marshaller
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			// Required formatting??
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			// Write XML to StringWriter
			jaxbMarshaller.marshal(this, sw);

		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Verify XML Content
		String xmlContent = sw.toString();
		return xmlContent;
	}
}
