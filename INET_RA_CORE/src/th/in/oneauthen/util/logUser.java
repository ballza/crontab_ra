package th.in.oneauthen.util;

import java.util.Date;

import th.in.oneauthen.object.LogUserDB;
import th.in.oneauthen.object.UserRaDB;
import th.in.oneauthen.object.DAO.LogUserDAO;

public class logUser {
	
	public void log(UserRaDB User, String action, String logDetail, String logPage, String logStatus) {

		LogUserDAO logUserDao = new LogUserDAO();
		LogUserDB logUser = new LogUserDB();
		logUser.setCreator(User);
		logUser.setLogAction(action);
		logUser.setLogTime(new Date());
		logUser.setLogDetail(logDetail);
		logUser.setLogPage(logPage);
		logUser.setLogStatus(logStatus);
		

		try {
			logUserDao.save(logUser);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
