package th.in.oneauthen.util;


import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.InvalidOperationException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.poifs.crypt.dsig.SignatureConfig;
import org.apache.poi.poifs.crypt.dsig.SignatureInfo;
import org.apache.poi.poifs.crypt.dsig.SignaturePart;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import th.in.oneauthen.object.CertificateDB;
import th.in.oneauthen.object.UserRaDB;
import th.in.oneauthen.object.DAO.CertificateDAO;
import th.in.oneauthen.object.DAO.UserRaDAO;

public class ReadExcel {
	private String CertSN;
	private byte[] ExcelByte = null;

	public List<User> Response(byte[] ExcelByte) {
		List<User> listUser = new ReadExcel().readExcel(ExcelByte);

//		for (User user : listUser) {
//			System.out.println(user.getUser_Name() + "  test");
//			System.out.println(user.getCareTakerID() + "  test");
//		}
		return listUser;
	}

	private List<User> readExcel(byte[] ExcelByte) {
		try {
			this.ExcelByte = ExcelByte;
			Workbook workbook = WorkbookFactory.create(new ByteArrayInputStream(this.ExcelByte), "UTF-8");
			//Workbook workbook = WorkbookFactory.create(new FileInputStream(pathToExcel), "UTF-8");
			// If you have only one sheet you can get it by index of the sheet
			// Sheet sheet = workbook.getSheetAt(0);
			Iterator<Sheet> sheetItr = workbook.sheetIterator();
			while (sheetItr.hasNext()) {
				Sheet sheet = sheetItr.next();
				// For Users sheet create List of objects
//				if (sheet.getSheetName().equals("Sheet2")) {
					return readExcelSheet(sheet);
//				} else {
//					// For other sheet just print the cell values
//					printExcelSheet(sheet);
//				}
			}

		} catch (EncryptedDocumentException | IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private List<User> readExcelSheet(Sheet sheet) throws ParseException, IOException {

		OPCPackage pkg = null;
		try {
			pkg = OPCPackage.open(new ByteArrayInputStream(this.ExcelByte));
		} catch (InvalidOperationException | InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SignatureConfig sic = new SignatureConfig();
		sic.setOpcPackage(pkg);
		SignatureInfo si = new SignatureInfo();
		si.setSignatureConfig(sic);
		boolean isValid = si.verifySignature();
		for (SignaturePart sp : si.getSignatureParts()) {
			if (sp.validate()) {
				this.CertSN = sp.getSigner().getSerialNumber().toString(16);
			}
		}

		int userID = CheckSN(this.CertSN);
		System.out.println(this.CertSN);
		// System.out.println("Starting to read sheet- " + sheet.getSheetName());
		Iterator<Row> rowItr = sheet.iterator();
		List<User> userList = new ArrayList<>();
		// Iterate each row in the sheet
		while (rowItr.hasNext()) {
			User user = new User();
			Row row = rowItr.next();
			// First row is header so skip it
			if (row.getRowNum() == 0 || row.getRowNum() == 1 || row.getRowNum() == 2 || row.getRowNum() == 3) {
				continue;
			}
			Iterator<Cell> cellItr = row.cellIterator();
			// Iterate each cell in a row
			user.setCareTakerID(userID);
			while (cellItr.hasNext()) {

				Cell cell = cellItr.next();
				int index = cell.getColumnIndex();
				switch (index) {
				case 1:
					// String encoded = URLEncoder.encode(getValueFromCell(cell)+"", "UTF-8");
					// System.out.println(java.net.URLDecoder.decode(encoded,
					// StandardCharsets.UTF_8.name()));
					user.setName_TH((String) getValueFromCell(cell));
					break;
				case 2:
					user.setName_Eng((String) getValueFromCell(cell));
					break;
				case 3:
					user.setUser_Name((String) getValueFromCell(cell));
					break;
				case 4:
					try {
						user.setBirthday((Date) getValueFromCell(cell));
					} catch (Exception e) {
						// TODO: handle exception
						user.setBirthday(null);
					}
					break;
				case 5:
					user.setRace((String) getValueFromCell(cell));
					break;
				case 6:
					user.setNationality((String) getValueFromCell(cell));
					break;
				case 7:
					user.setIdentification_citizen((String) getValueFromCell(cell));
					break;
				case 8:
					user.setIssued_by((String) getValueFromCell(cell));
					break;
				case 9:
					user.setMFD((String) getValueFromCell(cell));
					break;
				case 10:
					user.setEXD((String) getValueFromCell(cell));
					break;
				case 11:
					user.setAddress((String) getValueFromCell(cell));
					break;
				case 12:
					user.setTel((String) getValueFromCell(cell));
					break;
				case 13:
					user.setPhone((String) getValueFromCell(cell));
					break;
				case 14:
					user.setEmail((String) getValueFromCell(cell));
					break;
				case 15:
					user.setT((String) getValueFromCell(cell));
					break;
				case 16:
					user.setCN((String) getValueFromCell(cell));
					break;
				case 17:
					user.setOU((String) getValueFromCell(cell));
					break;
				case 18:
					user.setO((String) getValueFromCell(cell));
					break;
				case 19:
					user.setIdentification_company((String) getValueFromCell(cell));
					break;
				case 20:
					user.setCoordinator_Name((String) getValueFromCell(cell));
					break;
				case 21:
					user.setCoordinator_Email((String) getValueFromCell(cell));
					break;
				case 22:
					user.setCoordinator_Phone((String) getValueFromCell(cell));
					break;
				case 23:
					user.setCertType((String) getValueFromCell(cell));
					break;
				case 24:
					user.setCSR((String) getValueFromCell(cell));
					break;



				}
			}
			userList.add(user);

		}
		// for(User user : userList) {
		// System.out.println(user.getFirstName() + " " + user.getLastName() + " " +
		// user.getEmail() + " " + user.getDOB());
		// }

		return userList;

	}

	// This method is used to print cell values
	private void printExcelSheet(Sheet sheet) throws ParseException {
		System.out.println("Starting to read sheet- " + sheet.getSheetName());
		Iterator<Row> rowItr = sheet.iterator();
		while (rowItr.hasNext()) {
			Row row = rowItr.next();
			if (row.getRowNum() == 0) {
				continue;
			}
			Iterator<Cell> cellItr = row.cellIterator();
			while (cellItr.hasNext()) {
				Cell cell = cellItr.next();
				System.out.println(
						"Cell Type- " + cell.getCellTypeEnum().toString() + " Value- " + getValueFromCell(cell));
			}
		}
	}

	// Method to get cell value based on cell type
	private Object getValueFromCell(Cell cell) {
		switch (cell.getCellTypeEnum()) {
		case STRING:
			return cell.getStringCellValue();
		case BOOLEAN:
			return cell.getBooleanCellValue();
		case NUMERIC:
			if (DateUtil.isCellDateFormatted(cell)) {
				return cell.getDateCellValue();
			}
			return cell.getNumericCellValue();
		case FORMULA:
			return cell.getCellFormula();
		case BLANK:
			return "";
		default:
			return "";
		}
	}

	private int CheckSN(String SN) {

		CertificateDAO certDao = new CertificateDAO();
		CertificateDB certData = certDao.findBySn(SN);
		if (certData == null) {
			return -1;
		}
		certData.getCreator().getUserId();
		updateType(certData.getCreator());

		return certData.getCreator().getUserId();
	}
	private void updateType(UserRaDB userObj) {
		userObj.setType("manager");
		try {
			new UserRaDAO().updateType(userObj);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

}
