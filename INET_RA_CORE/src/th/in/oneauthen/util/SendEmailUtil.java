package th.in.oneauthen.util;

import java.io.IOException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang3.StringUtils;

import th.in.oneauthen.emailObj.EmailRequest;



public class SendEmailUtil {
	
	public void sendEmail(EmailRequest reqest) throws IOException {
		
		//String sendTo ,String mailCC1 ,String mailCC2 ,String pathPdf ,String DocNumber **old version
		   String to = reqest.getSendTo();
		   
		  String mailCC = reqest.getMailCC();

		      // Sender's email ID needs to be mentioned
		      String from = ConfigUtil.MAIL_SEND;
		      final String username = ConfigUtil.MAIL_AUTH;//change accordingly
		      final String password = ConfigUtil.MAIL_AUTH_PWD;//change accordingly

		      // Assuming you are sending email through relay.jangosmtp.net
		      String host = ConfigUtil.HOST;

		      Properties props = new Properties();
		      props.put("mail.smtp.auth", ConfigUtil.AUTH);
		     // props.put("mail.smtp.starttls.enable", "true");
		      props.put("mail.smtp.socketFactory.port", ConfigUtil.SOCKETFACTORY_PORT);
		      props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		      props.put("mail.smtp.host", host);
		      props.put("mail.smtp.port", ConfigUtil.PORT);

		      Session session = Session.getInstance(props,
		         new javax.mail.Authenticator() {
		            protected PasswordAuthentication getPasswordAuthentication() {
		               return new PasswordAuthentication(username, password);
		            }
		         });

		      try {

		         // Create a default MimeMessage object.
		         Message message = new MimeMessage(session);

		         // Set From: header field of the header.
		         message.setFrom(new InternetAddress(from));

		         // Set To: header field of the header.
		         message.setRecipients(Message.RecipientType.TO,
		            InternetAddress.parse(to));
		         if(StringUtils.isNotEmpty(mailCC))
		        	 message.setRecipients(Message.RecipientType.CC,InternetAddress.parse(mailCC));

		         // Set Subject: header field
		         message.setSubject(ConfigUtil.MAIL_SUBJECT_REQEST);

		         // This mail has 2 part, the BODY and the embedded image
		         MimeMultipart multipart = new MimeMultipart("related");

		         // first part (the html)
		         String head = ConfigUtil.MAIL_HEAD_CONTENT;
		         String update_head = head.replace("[USERNAME]",reqest.getOwnerName()).replace("[CN]", reqest.getCn())
		        		 .replace("[CERT_TYPE]", reqest.getCertype()).replace("[CERT_YEAR]", reqest.getCertyear());

		        // String tail = ConfigUtil.MAIL_TAIL_CONTENT;
		         BodyPart messageBodyPart = new MimeBodyPart();
		         String htmlText = update_head+"<H1></H1>";
		         messageBodyPart.setContent(htmlText, "text/html; charset=UTF-8");
		         // add it
		         multipart.addBodyPart(messageBodyPart);
		         // second part (the image)
		         messageBodyPart = new MimeBodyPart();
		         DataSource fds = new FileDataSource(ConfigUtil.MAIL_IMAGE);
		         messageBodyPart.setDataHandler(new DataHandler(fds));
		         messageBodyPart.setHeader("Content-ID", "<map>");
		         messageBodyPart.setFileName("map.png");
		         
		         BodyPart  messageBodyPartImgIcon = new MimeBodyPart();
		         DataSource fds2 = new FileDataSource(ConfigUtil.MAIL_IMAGE_ICON);
		         messageBodyPartImgIcon.setDataHandler(new DataHandler(fds2));
		         messageBodyPartImgIcon.setHeader("Content-ID", "<iconCA>");
		         messageBodyPartImgIcon.setFileName("iconCA.png");
		         
		         BodyPart messageBodyPartPDF = new MimeBodyPart();
		         String filename = reqest.getPathPdf();
		         DataSource source = new FileDataSource(filename);
		         messageBodyPartPDF.setDataHandler(new DataHandler(source));
		         messageBodyPartPDF.setFileName("Document.pdf");


		         // add image to the multipart
		         multipart.addBodyPart(messageBodyPart);
		         multipart.addBodyPart(messageBodyPartImgIcon);
		         multipart.addBodyPart(messageBodyPartPDF);

		         // put everything together
		         message.setContent(multipart);
		         // Send message
		         Transport.send(message);

		         System.out.println("Sent message successfully....");

		      } catch (MessagingException e) {
		         throw new RuntimeException(e);
		      }
	}
	
	public static void main(String[] args) throws IOException {
		
		EmailRequest request = new EmailRequest();
		request.setCertyear("10005");
		request.setCertype("10005");
		request.setCn("ชัยชนะ สีทัด");
		request.setMailCC("chaichana.si@oneauthen.in.th");
		request.setOwnerName("balltest");
		request.setPathPdf("C:\\Users\\hp\\Desktop\\signed-Doc65.pdf");
		request.setSendTo("ballbbcona@gmail.com");
		
		new SendEmailUtil().sendEmail(request);
	}

}
