package th.in.oneauthen.util;







import java.io.StringWriter;
import java.net.InetAddress;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class CertApply_API {
	public  String apply1001and1002(String action, String certType, String userId, String operator, String csr, String C, String O, String CN,
			String idPerson, String email, String personal_mail,String CNTH,int countCert,String unique_id) {

	CHTRA_RequestXML customer = new CHTRA_RequestXML();
	customer.setAction(action);
	customer.setCertType(certType);
//	customer.setUserID(userId + "/"+countCert);
	customer.setUserID(unique_id);
	customer.setOperator("[Operator]");
	customer.setCSR(csr);
	customer.setSubjectDN_Data1(C);
//	customer.setSubjectDN_Data2(O);
	customer.setSubjectDN_Data2(CNTH);
	customer.setSubjectDN_Data3(idPerson);
	customer.setExtension_Data1(email+";;dn:CN="+CN);

	  
	  try {

		JAXBContext jaxbContext = JAXBContext.newInstance(CHTRA_RequestXML.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(customer, sw);
		String xmlString = sw.toString();
		
		String noindent = xmlString.replaceAll(" standalone=\"yes\"","");
		String noindent2 = noindent.replaceAll("  ","");
		String noindent3 = noindent2.replaceAll("\n","");
		String noindent5 = noindent3.replaceAll("\r","");
		
		System.out.println(xmlString);		
		
		 int a =  noindent5.indexOf(">");
		 noindent5 = noindent5.substring(a+1, noindent5.length());

		
		

		return noindent5;
//        try {
//            InetAddress i = InetAddress.getLocalHost();
//            System.out.println(i);                  // host name and IP address
//            System.out.println(i.getHostName());    // name
//            System.out.println(i.getHostAddress()); // IP address only
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
		

	      } catch (JAXBException e) {
		e.printStackTrace();
	      }
	return"fail";

	}
	
	public  String apply1003and1004(String action, String certType, String userId, String operator, String csr, String C, String O, String CN,
			String idPerson, String email, String personal_mail, String CNTH, int countCert ,String unique_id) {

	CHTRA_RequestXML customer = new CHTRA_RequestXML();
	customer.setAction(action);
	customer.setCertType(certType);
//	customer.setUserID(userId + "/"+countCert);
	customer.setUserID(unique_id);
	customer.setOperator("[Operator]");
	customer.setCSR(csr);
	customer.setSubjectDN_Data1(C);
//	customer.setSubjectDN_Data2(O);
	customer.setSubjectDN_Data2(CNTH);
//	customer.setSubjectDN_Data3(idPerson);
	customer.setExtension_Data1(email+";;dn:CN="+CN);

	  
	  try {

		JAXBContext jaxbContext = JAXBContext.newInstance(CHTRA_RequestXML.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(customer, sw);
		String xmlString = sw.toString();
		
		String noindent = xmlString.replaceAll(" standalone=\"yes\"","");
		String noindent2 = noindent.replaceAll("  ","");
		String noindent3 = noindent2.replaceAll("\n","");
		String noindent5 = noindent3.replaceAll("\r","");
		
		System.out.println(xmlString);		
		
		 int a =  noindent5.indexOf(">");
		 noindent5 = noindent5.substring(a+1, noindent5.length());

		
		

		return noindent5;
//        try {
//            InetAddress i = InetAddress.getLocalHost();
//            System.out.println(i);                  // host name and IP address
//            System.out.println(i.getHostName());    // name
//            System.out.println(i.getHostAddress()); // IP address only
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
		

	      } catch (JAXBException e) {
		e.printStackTrace();
	      }
	return"fail";

	}
	
	public  String apply1005and1006(String action, String certType, String userId, String operator, String csr, String C, String O, String CN,
			String idPerson, String email, String personal_mail, String CNTH, int countCert ,String unique_id ,String ou ,String t ,String oid) {

	CHTRA_RequestXML customer = new CHTRA_RequestXML();
	customer.setAction(action);
	customer.setCertType(certType);
	customer.setUserID(unique_id);
	customer.setOperator("[Operator]");
	customer.setCSR(csr);
	customer.setSubjectDN_Data1(C);
	customer.setSubjectDN_Data2(O);
	customer.setSubjectDN_Data3(t);
	customer.setSubjectDN_Data4(ou);
	customer.setSubjectDN_Data5(CNTH);
	customer.setSubjectDN_Data6(oid);
	customer.setExtension_Data1(email+";;dn:CN="+CN);

	  
	  try {

		JAXBContext jaxbContext = JAXBContext.newInstance(CHTRA_RequestXML.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(customer, sw);
		String xmlString = sw.toString();
		
		String noindent = xmlString.replaceAll(" standalone=\"yes\"","");
		String noindent2 = noindent.replaceAll("  ","");
		String noindent3 = noindent2.replaceAll("\n","");
		String noindent5 = noindent3.replaceAll("\r","");
		
		System.out.println(xmlString);		
		
		 int a =  noindent5.indexOf(">");
		 noindent5 = noindent5.substring(a+1, noindent5.length());

		
		

		return noindent5;
//        try {
//            InetAddress i = InetAddress.getLocalHost();
//            System.out.println(i);                  // host name and IP address
//            System.out.println(i.getHostName());    // name
//            System.out.println(i.getHostAddress()); // IP address only
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
		

	      } catch (JAXBException e) {
		e.printStackTrace();
	      }
	return"fail";

	}
	
	
}



