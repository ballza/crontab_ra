package th.in.oneauthen.util;

import java.util.Date;

public class User {

	private String Name_TH;
	private String Name_Eng;
	private String User_Name;
	private Date Birthday;
	private String Race;
	private String Nationality;
	private String Identification_citizen;
	private String Issued_by;
	private String MFD;
	private String EXD;
	private String Address;
	private String Tel;
	private String Phone;
	private String Email;
	private String T;
	private String OU;
	private String O;
	private String Identification_company;
	private String Coordinator_Name;
	private String Coordinator_Email;
	private String Coordinator_Phone;
	private String CSR;
	private int CareTakerID;
	private String CertType;
	private String CN;
	private int Creator;

	public String getName_TH() {
		return Name_TH;
	}

	public void setName_TH(String name_TH) {
		Name_TH = name_TH;
	}

	public String getName_Eng() {
		return Name_Eng;
	}

	public void setName_Eng(String name_Eng) {
		Name_Eng = name_Eng;
	}

	public String getUser_Name() {
		return User_Name;
	}

	public void setUser_Name(String user_Name) {
		User_Name = user_Name;
	}

	public Date getBirthday() {
		return Birthday;
	}

	public void setBirthday(Date birthday) {
		Birthday = birthday;
	}

	public String getRace() {
		return Race;
	}

	public void setRace(String race) {
		Race = race;
	}

	public String getNationality() {
		return Nationality;
	}

	public void setNationality(String nationality) {
		Nationality = nationality;
	}

	public String getIdentification_citizen() {
		return Identification_citizen;
	}

	public void setIdentification_citizen(String identification_citizen) {
		Identification_citizen = identification_citizen;
	}

	public String getIssued_by() {
		return Issued_by;
	}

	public void setIssued_by(String issued_by) {
		Issued_by = issued_by;
	}



	public String getMFD() {
		return MFD;
	}

	public void setMFD(String mFD) {
		MFD = mFD;
	}

	public String getEXD() {
		return EXD;
	}

	public void setEXD(String eXD) {
		EXD = eXD;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getTel() {
		return Tel;
	}

	public void setTel(String tel) {
		Tel = tel;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getT() {
		return T;
	}

	public void setT(String t) {
		T = t;
	}

	public String getOU() {
		return OU;
	}

	public void setOU(String oU) {
		OU = oU;
	}

	public String getO() {
		return O;
	}

	public void setO(String o) {
		O = o;
	}

	public String getIdentification_company() {
		return Identification_company;
	}

	public void setIdentification_company(String identification_company) {
		Identification_company = identification_company;
	}

	public String getCoordinator_Name() {
		return Coordinator_Name;
	}

	public void setCoordinator_Name(String coordinator_Name) {
		Coordinator_Name = coordinator_Name;
	}

	public String getCoordinator_Email() {
		return Coordinator_Email;
	}

	public void setCoordinator_Email(String coordinator_Email) {
		Coordinator_Email = coordinator_Email;
	}

	public String getCoordinator_Phone() {
		return Coordinator_Phone;
	}

	public void setCoordinator_Phone(String coordinator_Phone) {
		Coordinator_Phone = coordinator_Phone;
	}

	public String getCSR() {
		return CSR;
	}

	public void setCSR(String cSR) {
		CSR = cSR;
	}

	public int getCareTakerID() {
		return CareTakerID;
	}

	public void setCareTakerID(int careTakerID) {
		CareTakerID = careTakerID;
	}

	public String getCertType() {
		return CertType;
	}

	public void setCertType(String certType) {
		CertType = certType;
	}

	public String getCN() {
		return CN;
	}

	public void setCN(String cN) {
		CN = cN;
	}

	public int getCreator() {
		return Creator;
	}

	public void setCreator(int creator) {
		Creator = creator;
	}
	
	

}
