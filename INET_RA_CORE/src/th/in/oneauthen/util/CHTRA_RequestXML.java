package th.in.oneauthen.util;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CHTRA_RequestXML")
public class CHTRA_RequestXML {

	String Action ;
	String CertType ;
	String UserID ;
	String Operator ;
	String CSR ;
	String SubjectDN_Data1 ;
	String SubjectDN_Data2 ;
	String SubjectDN_Data3 ;
	String SubjectDN_Data4 ;
	String SubjectDN_Data5 ;
	String SubjectDN_Data6 ;
	String Extension_Data1 ;
	
	String CertRevoke_Reason ;
	String CertRevoke_DataCount ;
	String CertRevoke_Data1 ;
	
	


	public String getAction() {
		return Action;
	}

	@XmlElement(name = "Action")
	public void setAction(String action) {
		Action = action;
	}

	public String getCertType() {
		return CertType;
	}

	@XmlElement(name = "CertType")
	public void setCertType(String certType) {
		CertType = certType;
	}

	public String getUserID() {
		return UserID;
	}

	@XmlElement(name = "UserID")
	public void setUserID(String userID) {
		UserID = userID;
	}

	public String getOperator() {
		return Operator;
	}

	@XmlElement(name = "Operator")
	public void setOperator(String operator) {
		Operator = operator;
	}

	public String getCSR() {
		return CSR;
	}

	@XmlElement(name = "CSR")
	public void setCSR(String cSR) {
		CSR = cSR;
	}

	public String getSubjectDN_Data1() {
		return SubjectDN_Data1;
	}

	@XmlElement(name = "SubjectDN_Data1")
	public void setSubjectDN_Data1(String subjectDN_Data1) {
		SubjectDN_Data1 = subjectDN_Data1;
	}

	public String getSubjectDN_Data2() {
		return SubjectDN_Data2;
	}

	@XmlElement(name = "SubjectDN_Data2")
	public void setSubjectDN_Data2(String subjectDN_Data2) {
		SubjectDN_Data2 = subjectDN_Data2;
	}

	public String getSubjectDN_Data3() {
		return SubjectDN_Data3;
	}

	@XmlElement(name = "SubjectDN_Data3")
	public void setSubjectDN_Data3(String subjectDN_Data3) {
		SubjectDN_Data3 = subjectDN_Data3;
	}

	public String getSubjectDN_Data4() {
		return SubjectDN_Data4;
	}

	@XmlElement(name = "SubjectDN_Data4")
	public void setSubjectDN_Data4(String subjectDN_Data4) {
		SubjectDN_Data4 = subjectDN_Data4;
	}
	

	public String getSubjectDN_Data5() {
		return SubjectDN_Data5;
	}
	@XmlElement(name = "SubjectDN_Data5")
	public void setSubjectDN_Data5(String subjectDN_Data5) {
		SubjectDN_Data5 = subjectDN_Data5;
	}

	public String getSubjectDN_Data6() {
		return SubjectDN_Data6;
	}
	@XmlElement(name = "SubjectDN_Data6")
	public void setSubjectDN_Data6(String subjectDN_Data6) {
		SubjectDN_Data6 = subjectDN_Data6;
	}

	public String getExtension_Data1() {
		return Extension_Data1;
	}

	@XmlElement(name = "Extension_Data1")
	public void setExtension_Data1(String extension_Data1) {
		Extension_Data1 = extension_Data1;
	}

	public String getCertRevoke_Reason() {
		return CertRevoke_Reason;
	}

	@XmlElement(name = "CertRevoke_Reason")
	public void setCertRevoke_Reason(String certRevoke_Reason) {
		CertRevoke_Reason = certRevoke_Reason;
	}

	public String getCertRevoke_DataCount() {
		return CertRevoke_DataCount;
	}

	@XmlElement(name = "CertRevoke_DataCount")
	public void setCertRevoke_DataCount(String certRevoke_DataCount) {
		CertRevoke_DataCount = certRevoke_DataCount;
	}

	public String getCertRevoke_Data1() {
		return CertRevoke_Data1;
	}

	@XmlElement(name = "CertRevoke_Data1")
	public void setCertRevoke_Data1(String certRevoke_Data1) {
		CertRevoke_Data1 = certRevoke_Data1;
	}
}
