package th.in.oneauthen.util;

import java.security.Security;

import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class CryptoUtil {
	private static final String ENCRYPTION_KEY = "gxHo8;k,]y[l6fpvfw,j[vdsivd12345";
	public static final String SALT = "1234567890123456";
	
	private static final PaddedBufferedBlockCipher CIPHER;
	private static final ParametersWithIV KEY_PARAM;
	static {
		//Set up
		Security.addProvider(new BouncyCastleProvider());
	    AESEngine engine = new AESEngine();
	    CBCBlockCipher blockCipher = new CBCBlockCipher(engine); 
	    CIPHER = new PaddedBufferedBlockCipher(blockCipher); 
	    KeyParameter keyParam = new KeyParameter(ENCRYPTION_KEY.getBytes());
	    KEY_PARAM = new ParametersWithIV(keyParam, SALT.getBytes(), 0, 16);
	}
	
	public static byte[] encryptData ( byte[] plainText) throws Exception {
		int length;
		CIPHER.init(true, KEY_PARAM);
	    byte[] outputBytes = new byte[CIPHER.getOutputSize(plainText.length)];
	    length = CIPHER.processBytes(plainText,0,plainText.length, outputBytes, 0);
	    CIPHER.doFinal(outputBytes, length); 
	    return outputBytes;
	}
	
	public static byte[] decryptData ( byte[] cipherText) throws Exception{
		int length;
		CIPHER.init(false, KEY_PARAM);
	    byte[] comparisonBytes = new byte[CIPHER.getOutputSize(cipherText.length)];
	    length = CIPHER.processBytes(cipherText, 0, cipherText.length, comparisonBytes, 0);
	    CIPHER.doFinal(comparisonBytes, length); //Do the final block
	    return comparisonBytes;
	}
}
