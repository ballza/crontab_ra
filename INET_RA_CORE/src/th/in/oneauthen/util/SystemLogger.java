package th.in.oneauthen.util;

import org.apache.log4j.Logger;

public class SystemLogger {
	public static Logger generateSystemLogger() {
		return generateSystemLogger(SystemLogger.class);
	}
	
	public static Logger generateSystemLogger( Class loggerClass ) {
		return Logger.getLogger(loggerClass);
	}
}
