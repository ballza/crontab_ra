package th.in.oneauthen.util;

import th.in.oneauthen.object.DAO.SystemConfigDAO;

public class ConfigUtil {
	private static final String KEY_HOST = "mail_host";
	private static final String KEY_PORT = "mail_port";
	private static final String KEY_AUTH = "mail_auth";
	private static final String KEY_SOCKETFACTORY_PORT = "mail_socketfactory_port";
	private static final String KEY_MAIL_SEND = "mail_send";
	private static final String KEY_MAIL_AUTH = "mail_name_auth";
	private static final String KEY_MAIL_AUTH_PWD = "mail_auth_pwd";
	private static final String KEY_MAIL_SUBJECT = "mail_subject";
	private static final String KEY_MAIL_SUBJECT_REQUEST = "mail_subject_request";
	private static final String KEY_MAIL_HEAD_CONTENT = "mail_head_content";
	private static final String KEY_MAIL_TAIL_CONTENT = "mail_tail_content";
	private static final String KEY_MAIL_IMAGE = "mail_image";
	private static final String KEY_MAIL_CONFIG_SERVER = "mail_config_server";
	private static final String KEY_CHECK_EXD_MSG = "check_exd_msg";
	private static final String KEY_INFO_USER = "URL_GET_INFO_USER";
	private static final String KEY_UPDATE_INFO_USER = "URL_UPADTE_INFO_ONEID"; 
	private static final String KEY_MAIL_EXD = "MAIL_EXD";
	private static final String KEY_MONTH_EXD = "MONTH_EXD";
	private static final String KEY_CLIENT_ID = "client_id";
	private static final String KEY_CLIENT_SECRET = "client_secret";
	private static final String KEY_UPDATE_LOA4 = "URL_UPDATE_LOA4";
	private static final String KEY_REF_CODE = "ref_code";
	private static final String KEY_URL_API_GET_INFO_NO_AUTH = "URL_API_GET_INFO_NO_AUTH";
	private static final String KEY_URL_API_UPDATE_INFO_ONEID_NO_AUTH = "URL_API_UPDATE_INFO_ONEID_NO_AUTH";
	private static final String KEY_MAIL_IMAGE_ICON = "mail_icon";

	
	public static final String HOST;
	public static final String PORT;
	public static final String AUTH;
	public static final String SOCKETFACTORY_PORT;
	public static final String MAIL_SEND;
	public static final String MAIL_AUTH;
	public static final String MAIL_AUTH_PWD;
	public static final String MAIL_SUBJECT;
	public static final String MAIL_SUBJECT_REQEST;
	public static final String MAIL_HEAD_CONTENT;
	public static final String MAIL_TAIL_CONTENT;
	public static final String MAIL_IMAGE;
	public static final String MAIL_CONFIG_SERVER;
	public static final String CHECK_EXD_MSG;
	public static final String INFO_USER;
	public static final String UPDATE_INFO_USER;
	public static final String MAIL_EXD;
	public static final String MONTH_EXD;
	public static final String CLIENT_ID;
	public static final String CLIENT_SECRET;
	public static final String UPDATE_LOA4;
	public static final String REF_CODE;
	public static final String URL_API_GET_INFO_NO_AUTH;
	public static final String URL_API_UPDATE_INFO_ONEID_NO_AUTH;
	public static final String MAIL_IMAGE_ICON;
	
	static {
		SystemConfigDAO pathConfigDAO = new SystemConfigDAO();
		String mail_host = null;
		String mail_port = null;
		String mail_auth = null;
		String mail_socketfactory_port = null;
		String mail_send = null;
		String mail_name_auth = null;
		String mail_auth_pwd = null;
		String mail_subject = null;
		String mail_subject_request = null;
		String mail_head_content = null;
		String mail_tail_content = null;
		String mail_image = null;
		String mail_config_server = null;
		String check_exd_msg = null;
		String info_user = null;
		String update_info_user = null;
		String mail_exd = null;
		String month_exd = null;
		String client_id = null;
		String client_secret = null;
		String update_loa4 = null;
		String ref_code = null;
		String url_api_get_info_no_auth = null;
		String url_api_update_info_oneid_no_auth = null;
		String mail_icon = null;
		try {
			mail_host = pathConfigDAO.find(KEY_HOST).getValue();
			mail_port = pathConfigDAO.find(KEY_PORT).getValue();
			mail_auth = pathConfigDAO.find(KEY_AUTH).getValue();
			mail_socketfactory_port = pathConfigDAO.find(KEY_SOCKETFACTORY_PORT).getValue();
			mail_send = pathConfigDAO.find(KEY_MAIL_SEND).getValue();
			mail_name_auth = pathConfigDAO.find(KEY_MAIL_AUTH).getValue();
			mail_auth_pwd = pathConfigDAO.find(KEY_MAIL_AUTH_PWD).getValue();
			mail_subject = pathConfigDAO.find(KEY_MAIL_SUBJECT).getValue();
			mail_subject_request = pathConfigDAO.find(KEY_MAIL_SUBJECT_REQUEST).getValue();
			mail_head_content = pathConfigDAO.find(KEY_MAIL_HEAD_CONTENT).getValue();
			mail_tail_content = pathConfigDAO.find(KEY_MAIL_TAIL_CONTENT).getValue();
			mail_image = pathConfigDAO.find(KEY_MAIL_IMAGE).getValue();
			mail_config_server = pathConfigDAO.find(KEY_MAIL_CONFIG_SERVER).getValue();
			check_exd_msg = pathConfigDAO.find(KEY_CHECK_EXD_MSG).getValue();
			info_user = pathConfigDAO.find(KEY_INFO_USER).getValue();
			update_info_user = pathConfigDAO.find(KEY_UPDATE_INFO_USER).getValue();
			mail_exd = pathConfigDAO.find(KEY_MAIL_EXD).getValue();
			month_exd = pathConfigDAO.find(KEY_MONTH_EXD).getValue();
			 client_id = pathConfigDAO.find(KEY_CLIENT_ID).getValue();
			 client_secret = pathConfigDAO.find(KEY_CLIENT_SECRET).getValue();
			 update_loa4 = pathConfigDAO.find(KEY_UPDATE_LOA4).getValue();
			 ref_code = pathConfigDAO.find(KEY_REF_CODE).getValue();
			 url_api_get_info_no_auth = pathConfigDAO.find(KEY_URL_API_GET_INFO_NO_AUTH).getValue();
			 url_api_update_info_oneid_no_auth = pathConfigDAO.find(KEY_URL_API_UPDATE_INFO_ONEID_NO_AUTH).getValue();
			 mail_icon = pathConfigDAO.find(KEY_MAIL_IMAGE_ICON).getValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		  HOST = mail_host;
		  PORT = mail_port;
		  AUTH = mail_auth;
		  SOCKETFACTORY_PORT = mail_socketfactory_port;
		  MAIL_SEND = mail_send;
		  MAIL_AUTH = mail_name_auth;
		  MAIL_AUTH_PWD = mail_auth_pwd;
		  MAIL_SUBJECT = mail_subject;
		  MAIL_SUBJECT_REQEST = mail_subject_request;
		  MAIL_HEAD_CONTENT = mail_head_content;
		  MAIL_TAIL_CONTENT = mail_tail_content;
		  MAIL_IMAGE = mail_image;
		  MAIL_CONFIG_SERVER = mail_config_server;
		  CHECK_EXD_MSG = check_exd_msg;
		  INFO_USER = info_user;
		  UPDATE_INFO_USER = update_info_user;
		  MAIL_EXD = mail_exd;
		  MONTH_EXD = month_exd;
		  CLIENT_ID = client_id;
		  CLIENT_SECRET = client_secret;
		  UPDATE_LOA4 = update_loa4;
		  REF_CODE = ref_code;
		  URL_API_GET_INFO_NO_AUTH = url_api_get_info_no_auth;
		  URL_API_UPDATE_INFO_ONEID_NO_AUTH = url_api_update_info_oneid_no_auth;
		  MAIL_IMAGE_ICON = mail_icon;
		  
	}
}
