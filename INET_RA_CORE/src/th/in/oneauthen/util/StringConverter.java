package th.in.oneauthen.util;

import java.time.*;
import java.time.chrono.*;
import java.time.format.*;
import java.time.temporal.*;
import java.util.Locale; 
import java.io.PrintStream;
public class StringConverter {

    /**
     * Converts a LocalDate (ISO) value to a ChronoLocalDate date
     * using the provided Chronology, and then formats the
     * ChronoLocalDate to a String using a DateTimeFormatter with a
     * SHORT pattern based on the Chronology and the current Locale.
     *
     * @param localDate - the ISO date to convert and format.
     * @param chrono - an optional Chronology. If null, then IsoChronology
     *                 is used.
     */
	public static String toString(LocalDate localDate, Chronology chrono) {
		if (localDate != null) {
			Locale locale = Locale.getDefault(Locale.Category.FORMAT);
			ChronoLocalDate cDate;
			if (chrono == null) {
				chrono = IsoChronology.INSTANCE;
			}
			try {
				cDate = chrono.date(localDate);
			} catch (DateTimeException ex) {
				System.err.println(ex);
				chrono = IsoChronology.INSTANCE;
				cDate = localDate;
			}
			String pattern = "yyyy-MM-dd";
			DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
			return dateFormatter.format(cDate);
		} else {
			return "";
		}
	
    }
 
    /**
     * Parses a String to a ChronoLocalDate using a DateTimeFormatter
     * with a short pattern based on the current Locale and the
     * provided Chronology, then converts this to a LocalDate (ISO)
     * value.
     *
     * @param text   - the input date text in the SHORT format expected
     *                 for the Chronology and the current Locale.
     *
     * @param chrono - an optional Chronology. If null, then IsoChronology
     *                 is used.
     */
    public static LocalDate fromString(String text, Chronology chrono) {
        if (text != null && !text.isEmpty()) {
            Locale locale = Locale.getDefault(Locale.Category.FORMAT);
            if (chrono == null) {
                chrono = IsoChronology.INSTANCE;
            }
            String pattern = "d/M/yyyy GGGGG";
            DateTimeFormatter df = new DateTimeFormatterBuilder().parseLenient()
                                  .appendPattern(pattern)
                                  .toFormatter()
                                  .withChronology(chrono)
                                  .withDecimalStyle(DecimalStyle.of(locale));
            TemporalAccessor temporal = df.parse(text);
            ChronoLocalDate cDate = chrono.date(temporal);
            return LocalDate.from(cDate);
        }
        return null;
    }
    
    
 
}
