package th.in.oneauthen.pki.certificate;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.List;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;
import org.cesecore.config.AvailableExtendedKeyUsagesConfiguration;

public class X509CertificateUtility {

	X509Certificate cert;
	X500Name x500name;

	AvailableExtendedKeyUsagesConfiguration ekuConfig = new AvailableExtendedKeyUsagesConfiguration();

	public X509CertificateUtility(String base64) throws CertificateException {
		X509Certificate cert = (X509Certificate) CertificateFactory.getInstance("X509")
				.generateCertificate(new ByteArrayInputStream(Base64.decode(base64)));
		this.cert = cert;
		this.x500name = new JcaX509CertificateHolder(cert).getSubject();
	}

	public X509CertificateUtility(X509Certificate cert) throws CertificateEncodingException {
		this.cert = cert;
		this.x500name = new JcaX509CertificateHolder(cert).getSubject();
	}

	public X509Certificate getCertificate() {
		return this.cert;
	}

	public Collection<List<?>> getSumjectAltName() throws CertificateParsingException {
		return cert.getSubjectAlternativeNames();
	}

	public String getCertificateSubjectDN() {
		return cert.getSubjectDN().toString();
	}

	public String getCommonName() {
		RDN cn = x500name.getRDNs(BCStyle.CN)[0];
		String cnData = IETFUtils.valueToString(cn.getFirst().getValue());

		return new String(cnData.getBytes(), Charset.forName("UTF-8"));
	}

	public String getOU() {
		try {
			RDN cn = x500name.getRDNs(BCStyle.OU)[0];
			String cnData = IETFUtils.valueToString(cn.getFirst().getValue());
			return new String(cnData.getBytes(), Charset.forName("UTF-8"));
		} catch (Exception e) {
			return "";
		}
	}

	public String getO() {
		try {
			RDN cn = x500name.getRDNs(BCStyle.O)[0];
			String cnData = IETFUtils.valueToString(cn.getFirst().getValue());
			return new String(cnData.getBytes(), Charset.forName("UTF-8"));
		} catch (Exception e) {
			return "";
		}
	}

	public String getT() {
		try {
			RDN cn = x500name.getRDNs(BCStyle.T)[0];
			String cnData = IETFUtils.valueToString(cn.getFirst().getValue());
			return new String(cnData.getBytes(), Charset.forName("UTF-8"));
		} catch (Exception e) {
			return "";
		}
	}
	
	public String getCertSN() {
		try {
			BigInteger sn = this.cert.getSerialNumber();
			return sn.toString(16);
		} catch (Exception e) {
			return "";
		}
	}

	public String getOrganizationIdentifier() {
		try {
			ASN1ObjectIdentifier orgOID = new ASN1ObjectIdentifier("2.5.4.97");
			RDN cn = x500name.getRDNs(orgOID)[0];
			if (cn != null) {
				String cnData = IETFUtils.valueToString(cn.getFirst().getValue());
				return new String(cnData.getBytes(), Charset.forName("UTF-8"));
			} else
				return "N/A";
		} catch (Exception e) {
			return "";
		}
	}

	public String getCertificateFingerprintSHA1() {
		try {
			byte[] md = MessageDigest.getInstance("SHA1").digest(cert.getEncoded());
			return Hex.toHexString(md).toUpperCase();
		} catch (Exception e) {
			e.printStackTrace();
			return "Certificate fingerprint calculation error: " + e.getMessage();
		}
	}

	public String getReadableKeyUsage() {
		String strKeyUsage = "";
		if (this.cert != null) {
			if (this.cert.getKeyUsage()[0])
				strKeyUsage = strKeyUsage + "Digital Signature";
			if (this.cert.getKeyUsage()[1])
				strKeyUsage = strKeyUsage + ("".equals(strKeyUsage) ? "" : ", ") + "Non Repudiation";
			if (this.cert.getKeyUsage()[2])
				strKeyUsage = strKeyUsage + ("".equals(strKeyUsage) ? "" : ", ") + "Key Encipherment";
			if (this.cert.getKeyUsage()[3])
				strKeyUsage = strKeyUsage + ("".equals(strKeyUsage) ? "" : ", ") + "Data Encipherment";
			if (this.cert.getKeyUsage()[4])
				strKeyUsage = strKeyUsage + ("".equals(strKeyUsage) ? "" : ", ") + "Key Agreement";
			if (this.cert.getKeyUsage()[5])
				strKeyUsage = strKeyUsage + ("".equals(strKeyUsage) ? "" : ", ") + "Key Cert Sign";
			if (this.cert.getKeyUsage()[6])
				strKeyUsage = strKeyUsage + ("".equals(strKeyUsage) ? "" : ", ") + "CRL Sign";
			if (this.cert.getKeyUsage()[7])
				strKeyUsage = strKeyUsage + ("".equals(strKeyUsage) ? "" : ", ") + "Encipher Only";
			if (this.cert.getKeyUsage()[8])
				strKeyUsage = strKeyUsage + ("".equals(strKeyUsage) ? "" : ", ") + "Decipher Only";
		}
		return strKeyUsage;
	}

	public String getReadableEKU() {
		List<String> ekuList;
		try {
			String eku = "";
			ekuList = this.cert.getExtendedKeyUsage();
			if (ekuList != null) {
				for (String ekuObj : ekuList) {
					eku = eku + ("".equals(eku) ? "" : ", ") + ekuConfig.getPrettyPrintEKUName(ekuObj);
				}
			}
			return eku;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Certificate fingerprint calculation error: " + e.getMessage();
		}
	}

	public static String PARSE_REVOKE_REASON(String revokeReason) {
		String strRevokeReason = "";
		if ("0".equals(revokeReason))
			strRevokeReason = "Unused";
		else if ("1".equals(revokeReason))
			strRevokeReason = "Key Compromise";
		else if ("2".equals(revokeReason))
			strRevokeReason = "CA Compromise";
		else if ("3".equals(revokeReason))
			strRevokeReason = "Affiliation Changed";
		else if ("4".equals(revokeReason))
			strRevokeReason = "Superseded";
		else if ("5".equals(revokeReason))
			strRevokeReason = "Cessation Of Operation";
		else if ("6".equals(revokeReason))
			strRevokeReason = "Certificate Hold";
		else if ("7".equals(revokeReason))
			strRevokeReason = "Privilege Withdrawn";
		else if ("8".equals(revokeReason))
			strRevokeReason = "AACompromise";

		return strRevokeReason;
	}

	public static void main(String[] args) throws Exception {
		CertificateFactory cFac = CertificateFactory.getInstance("X509");

		X509Certificate cert1 = (X509Certificate) cFac
				.generateCertificate(new FileInputStream(new File("C:\\Users\\hp\\Desktop\\10003.cer")));
		X509Certificate cert2 = (X509Certificate) cFac
				.generateCertificate(new FileInputStream(new File("C:\\Users\\hp\\Desktop\\10003.cer")));

		X509CertificateUtility util1 = new X509CertificateUtility(cert1);
		System.out.println(util1.getCommonName());
		System.out.println(util1.getCertificateFingerprintSHA1());
		System.out.println(util1.getReadableKeyUsage());
		System.out.println(util1.getReadableEKU());
		System.out.println(util1.getOrganizationIdentifier());
		System.out.println(util1.getOU());
		System.out.println(util1.getO() + "test");
		System.out.println(util1.getT());
		System.out.println(util1.getCertSN());
		System.out.println("============================================================");
		// X509CertificateUtility util2 = new X509CertificateUtility(cert2);
		// System.out.println(util2.getCommonName());
		// System.out.println(util2.getCertificateFingerprintSHA1());
		// System.out.println(util2.getReadableKeyUsage());
		// System.out.println(util2.getReadableEKU());
	}
}