package th.in.oneauthen.pki;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import th.in.oneauthen.pki.certificate.X509CertificateUtility;

public interface GenericPKIAdapter {
	public static final String BASE64 = "Base64";
	public static final String STATUS = "Status";
	public static final String REVOKE_REASON = "RevokeReason";
	public static final String SERIAL_NUMBER = "SerialNumber";
	public static final String UNIQUE_ID= "uniqueId";
	public static final String REQ_CSR = "Csr";
	public static final String REJECT = "reject";
	public static final String APPLY_TIME = "applyTime";
	public static final String EMAIL = "email";
	public static final String USERNAME = "username";
	
	
	
	public HashMap<String, String> getCertificateBySerialNumber(String serialNumber);
	public Collection<CertProfileParameter> listCaSystemCertProfile();
	public String SaveRequestCert(String userName, String certType, String userId, String userEmail, String personal_mail, String userOrganization,
			String csr,Date registerTime, String CN, String C, String cnth, int countCert,String unique_id ,String ou ,String t, String oid , String l,
			String sn , String g);
	
	public HashMap<String, String> checkCertificateByuniqueID(String unique_id);
	public HashMap<String, String> checkCertificateByuniqueIDRegis(String unique_id);
	
	public Date SaveTimeAccep(String unique_id);
	public String SaveRevokeRequest(String userName, String unique_id, int revokeReason, String userEmail,
			String userOrganization, String certSn, String form_status, String action, Date registerTime);
	public Date syncForRevokeRequest(String unique_id);
	
	public List<HashMap<String, String>> serviceUpdateReq(List<String> uniqueList);
	public List<HashMap<String, String>> serviceUpdateCertData(List<String> uniqueList);
	
	public Date checkToken(String certSN, String token);
	public X509CertificateUtility checkAndResponseInfo(String certSN, String token);
	
	public Date unAccept(String certSN);
	
	public List<HashMap<String, String>> check10Accept(List<HashMap<String, String>> listMap);
	
	public ModelAccept newAccept(String certSN, String token);
	
	public String getMailConfig(String keyName);
	
	public int getFormID(String uniqueID);
	
	public String EditCSR(String uniqueID , String CSRB64);
	
	public Boolean deleteRequest(String uniqueID);
	
	
	
	
}
