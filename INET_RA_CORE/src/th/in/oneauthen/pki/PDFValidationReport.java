package th.in.oneauthen.pki;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.signatures.SignatureUtil;

public class PDFValidationReport {
	private boolean integrity;
	private int totalSignature;
	private ArrayList<PdfSignatureValidationReport> signatureInfoList = new ArrayList<>();
	
	public PDFValidationReport(byte[] document) throws IOException {
		PdfReader reader = new PdfReader(new ByteArrayInputStream(document));
		PdfDocument pdfDocument = new PdfDocument(reader);
		this.init(pdfDocument);
	}
	
	public PDFValidationReport(PdfDocument document) {
		this.init(document);
	}
	
	public void init(PdfDocument document) {
		Security.addProvider(new BouncyCastleProvider());
		
		boolean integrity = true;
		SignatureUtil pdfSignatureUtil = new SignatureUtil(document);
		List<String> names = pdfSignatureUtil.getSignatureNames();
		this.totalSignature = names.size();
		for ( String dict : names ) {
			PdfSignatureValidationReport sigValidationReport = new PdfSignatureValidationReport(pdfSignatureUtil, dict);
			this.signatureInfoList.add(sigValidationReport);
			integrity = integrity && sigValidationReport.isIntegrity();
		}
		this.integrity = integrity;
	}

	public boolean isIntegrity() {
		return integrity;
	}

	public void setIntegrity(boolean integrity) {
		this.integrity = integrity;
	}

	public int getTotalSignature() {
		return totalSignature;
	}

	public void setTotalSignature(int totalSignature) {
		this.totalSignature = totalSignature;
	}

	public ArrayList<PdfSignatureValidationReport> getSignatureInfoList() {
		return signatureInfoList;
	}

	public void setSignatureInfoList(ArrayList<PdfSignatureValidationReport> signatureInfoList) {
		this.signatureInfoList = signatureInfoList;
	}
}
