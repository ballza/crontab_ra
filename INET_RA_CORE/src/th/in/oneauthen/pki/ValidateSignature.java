package th.in.oneauthen.pki;



import java.util.ArrayList;

import org.bouncycastle.util.encoders.Base64;

public class ValidateSignature {

	public ArrayList<String> getSignature(String pdfData) {
		
		ArrayList<String> certs = new ArrayList<>();
		try {
			byte[] rawPdf = Base64.decode(pdfData);
			PDFValidationReport report = new PDFValidationReport(rawPdf);
			
			if (report != null && report.getTotalSignature() > 0 ) {
				for (PdfSignatureValidationReport sigReport : report.getSignatureInfoList()) {
					certs.add(Base64.toBase64String(sigReport.getSignerCertifiate().getEncoded()));
				}	
			}else {				
				throw new Exception("This is unsigned document.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return certs;
	}
}
