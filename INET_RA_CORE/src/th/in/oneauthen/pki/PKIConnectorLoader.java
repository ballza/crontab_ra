package th.in.oneauthen.pki;

public class PKIConnectorLoader {
	public static GenericPKIAdapter adapterLoader () {
		GenericPKIAdapter adapter = null;
		
		try {
//			Class<?> clazz = Class.forName("th.in.oneauthen.pki.impl.CHT_RA_PKIAdapter");
			Class cls = Class.forName("th.in.oneauthen.pki.impl.CHT_RA_PKIAdapter");
			GenericPKIAdapter newAdapter = (GenericPKIAdapter) cls.newInstance();
			
			if (newAdapter==null) throw new Exception ("Error loaded PKI Connector!!");
			else adapter = newAdapter;
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return adapter;
	}
}
