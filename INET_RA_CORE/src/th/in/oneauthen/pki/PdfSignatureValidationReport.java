package th.in.oneauthen.pki;

import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cms.SignerInformationVerifier;
import org.bouncycastle.cms.jcajce.JcaSignerInfoVerifierBuilder;
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;
import org.bouncycastle.tsp.TimeStampToken;

import com.itextpdf.signatures.PdfPKCS7;
import com.itextpdf.signatures.PdfSigner;
import com.itextpdf.signatures.SignaturePermissions;
import com.itextpdf.signatures.SignatureUtil;

public class PdfSignatureValidationReport {
	public static final String LOCAL_TIMESOURCE = "Local time";
	public static final String UNKNOW_TSA = "Unknow timestamp authority";
	
	private int revisionNo;//
	private int signatureStatus;
	private String signatureLevel;//
	private int signatureType; //
	private boolean integrity = false; //
	private boolean coverWholeDoc; //
	private Date signingTime; //
	private String timeSource; //
	private X509Certificate signerCertifiate; //
	private String reason; //
	private String location; //
	
	public PdfSignatureValidationReport(SignatureUtil sigInfo, String sigFieldName) {
		PdfPKCS7 sigObject = sigInfo.readSignatureData(sigFieldName);
		try {
			this.integrity = sigObject.verifySignatureIntegrityAndAuthenticity();
		} catch (Exception e) {
		}
		this.revisionNo = sigInfo.getRevision(sigFieldName);
		this.coverWholeDoc = sigInfo.signatureCoversWholeDocument(sigFieldName);
		this.reason = sigObject.getReason();
		this.location = sigObject.getLocation();
		this.signatureLevel = sigObject.getFilterSubtype().getValue();
		TimeStampToken tsToken = sigObject.getTimeStampToken();
		if (tsToken == null) {
			this.signingTime = sigObject.getSignDate().getTime();
			this.timeSource = LOCAL_TIMESOURCE;
		} else {
			this.signingTime = tsToken.getTimeStampInfo().getGenTime();
			this.timeSource = GET_TSA_NAME(tsToken);
		}
		this.signerCertifiate = sigObject.getSigningCertificate();
		SignaturePermissions perms = null; 
		perms = new SignaturePermissions(sigInfo.getSignatureDictionary(sigFieldName), perms);
		this.signatureType = perms.isCertification() ? PdfSigner.CERTIFIED_NO_CHANGES_ALLOWED : PdfSigner.NOT_CERTIFIED;
	}
	
	public int getRevisionNo() {
		return revisionNo;
	}

	public void setRevisionNo(int revisionNo) {
		this.revisionNo = revisionNo;
	}

	public int getSignatureStatus() {
		return signatureStatus;
	}

	public void setSignatureStatus(int signatureStatus) {
		this.signatureStatus = signatureStatus;
	}

	public String getSignatureLevel() {
		return signatureLevel;
	}

	public void setSignatureLevel(String signatureLevel) {
		this.signatureLevel = signatureLevel;
	}

	public int getSignatureType() {
		return signatureType;
	}

	public void setSignatureType(int signatureType) {
		this.signatureType = signatureType;
	}

	public boolean isIntegrity() {
		return integrity;
	}

	public void setIntegrity(boolean integrity) {
		this.integrity = integrity;
	}

	public boolean isCoverWholeDoc() {
		return coverWholeDoc;
	}

	public void setCoverWholeDoc(boolean coverWholeDoc) {
		this.coverWholeDoc = coverWholeDoc;
	}

	public Date getSigningTime() {
		return signingTime;
	}

	public void setSigningTime(Date signingTime) {
		this.signingTime = signingTime;
	}

	public String getTimeSource() {
		return timeSource;
	}

	public void setTimeSource(String timeSource) {
		this.timeSource = timeSource;
	}

	public X509Certificate getSignerCertifiate() {
		return signerCertifiate;
	}

	public void setSignerCertifiate(X509Certificate signerCertifiate) {
		this.signerCertifiate = signerCertifiate;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public static String GET_TSA_NAME(TimeStampToken tsToken) {
		if (tsToken.getTimeStampInfo().getTsa() == null
				|| StringUtils.isEmpty(tsToken.getTimeStampInfo().getTsa().toString())) {
			String timeSource = "";
			try {
				X509CertificateHolder tsaCert = null;
				@SuppressWarnings("rawtypes")
				Collection certStore = tsToken.getCertificates().getMatches(tsToken.getSID());
				for (Object cert : certStore) {
					tsaCert = (X509CertificateHolder) cert;
					break;
				}

				/* Create verifier object for token.validate() */
				JcaSignerInfoVerifierBuilder verifierBuilder = new JcaSignerInfoVerifierBuilder(
						new BcDigestCalculatorProvider());
				SignerInformationVerifier verifier = null;
				verifier = verifierBuilder.build(tsaCert);
				timeSource = tsaCert.getSubject().toString();
			} catch (Exception e) {
				timeSource = UNKNOW_TSA;
			}

			return timeSource;
		} else {
			return tsToken.getTimeStampInfo().getTsa().toString();
		}
	}
}
