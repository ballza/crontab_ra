package th.in.oneauthen.pki;

import java.util.HashMap;

public class CertProfileParameter {
	private String profileName;
	private String profileId;
	private HashMap<String, String> profileProperties;
	public String getProfileName() {
		return profileName;
	}
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public HashMap<String, String> getProfileProperties() {
		return profileProperties;
	}
	public void setProfileProperties(HashMap<String, String> profileProperties) {
		this.profileProperties = profileProperties;
	}
}
