package th.in.oneauthen.util2;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import th.in.oneauthen.object.DAO.SystemConfigDAO;
import th.in.oneauthen.util.ConfigUtil;

public class MailNotificationUtil {
	private static final String SYSTEM_PARAM_SMTP_HOST = "mail_host";
	private static final String SYSTEM_PARAM_SMTP_PORT = "mail_socketfactory_port";
	private static final String SYSTEM_PARAM_SMTP_ACCOUNT = "mail_name_auth";
	private static final String SYSTEM_PARAM_SMTP_ACCOUNT_PWD = "mail_auth_pwd";
	private static final String SYSTEM_PARAM_SMTP_SENDER_ADDR = "mail_send";

	private final String smtpHost;
	private final String smtpPort;
	private final String smtpAccount;
	private final String smtpPwd;
	private final String smtpSender;

	private final Properties mailConfig = new Properties();

	public MailNotificationUtil() {
	
		SystemConfigDAO sysConfig = new SystemConfigDAO();
		this.smtpHost = sysConfig.getConfig(SYSTEM_PARAM_SMTP_HOST, "smtp.inetmail.cloud");
		this.smtpPort = sysConfig.getConfig(SYSTEM_PARAM_SMTP_PORT, "25");
		this.smtpAccount = sysConfig.getConfig(SYSTEM_PARAM_SMTP_ACCOUNT, "signing@oneauthen.in.th");
		this.smtpPwd = sysConfig.getConfig(SYSTEM_PARAM_SMTP_ACCOUNT_PWD, "");	
		this.smtpSender = sysConfig.getConfig(SYSTEM_PARAM_SMTP_SENDER_ADDR, "noreply@oneauthen.in.th");
		
		this.mailConfig.put("mail.smtp.host", this.smtpHost);
		this.mailConfig.put("mail.smtp.socketFactory.port", this.smtpPort);
		this.mailConfig.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		this.mailConfig.put("mail.smtp.auth", "true");
		this.mailConfig.put("mail.smtp.port", this.smtpPort);		
		
	}

	public Session createMailSession() {
		Session session = null;
		try {
			session = Session.getInstance(this.mailConfig, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(smtpAccount, smtpPwd);
				}
			});
		} catch (Exception mailE) {
			session = Session.getInstance(this.mailConfig, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(smtpAccount, smtpPwd);
				}
			});
		}
		return session;
	}

	public void sendSigningDocumentMail(String pdfPath, String targetEmail) {
		try {
			Message message = new MimeMessage(this.createMailSession());
			message.setFrom(new InternetAddress(this.smtpSender));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(targetEmail));
			message.setSubject("Signing Document");

			MimeBodyPart messageBodyPart = new MimeBodyPart();
			Multipart multipart = new MimeMultipart();
			messageBodyPart = new MimeBodyPart();
			String file = pdfPath;
			String fileName = "Document.pdf";
			DataSource source = new FileDataSource(file);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(fileName);
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			System.out.println("Sending");
			Transport.send(message);
			System.out.println("Done");

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public void sendErrorLogMail(String content, Exception e) {
		try {
			Message message = new MimeMessage(this.createMailSession());
			message.setFrom(new InternetAddress(this.smtpSender));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("chaichana.si@oneauthen.in.th"));
			message.setRecipients(Message.RecipientType.CC,
					InternetAddress.parse("paradorn.at@oneauthen.in.th", false));
			message.setSubject("Signing Document Failed!!");

			MimeBodyPart messageBodyPart = new MimeBodyPart();
			// Now set the actual message
			messageBodyPart.setText(content);
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);
			messageBodyPart = new MimeBodyPart();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			e.printStackTrace(new PrintStream(baos));

			DataSource source = new ByteArrayDataSource(baos.toByteArray(), "text/plain");
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName("Failure.log");
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			System.out.println("Sending");
			Transport.send(message);
			System.out.println("Done");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void sendEmail(String subject, String content, String targetAddress) {
		try {
			
			Message message = new MimeMessage(this.createMailSession());
			message.setFrom(new InternetAddress(this.smtpSender));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(targetAddress));
			message.setSubject(subject);
			message.setContent(content, "text/html; charset=utf-8");
			message.setSentDate(new Date());
			Transport.send(message);
			System.out.println("Done");
	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendEmailEXP(String subject, String content, String targetAddress) {
		try {
			
			Message message = new MimeMessage(this.createMailSession());
			message.setFrom(new InternetAddress(this.smtpSender));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(targetAddress));
			message.setSubject(subject);
			message.setSentDate(new Date());

	         MimeMultipart multipart = new MimeMultipart("related");
	         BodyPart messageBodyPart = new MimeBodyPart();
	         String htmlText = content+"<H1></H1>";
	         messageBodyPart.setContent(htmlText, "text/html; charset=UTF-8");
	         // add it
	         multipart.addBodyPart(messageBodyPart);
	         messageBodyPart = new MimeBodyPart();
	         DataSource fds = new FileDataSource(ConfigUtil.MAIL_IMAGE_ICON);

	         messageBodyPart.setDataHandler(new DataHandler(fds));
	         messageBodyPart.setHeader("Content-ID", "<iconCA>");
	         messageBodyPart.setFileName("icon.png");
	         
	         // add image to the multipart
	         multipart.addBodyPart(messageBodyPart);

	         // put everything together
	         message.setContent(multipart);
	         // Send message
	         Transport.send(message);

	         System.out.println("Sent message successfully....");
			
			
			
			
	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
