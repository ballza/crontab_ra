package th.in.oneauthen.util2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import th.in.oneauthen.object.DAO.SystemConfigDAO;
import th.in.oneauthen.util.smsObject.corpsms_request;

public class SMSNotificationUtil {
	public static void sendSMS(String mobileNo, String content, String refCode) {
		try {
			corpsms_request smsObject = new corpsms_request(mobileNo, content, refCode);

			SystemConfigDAO sysConfig = new SystemConfigDAO();
			String smsHost = sysConfig.getConfig("sms.targetURL",
					"https://www.corp-sms.com/CorporateSMS/SMSReceiverXML");
			String smsUsr = sysConfig.getConfig("sms.username", "oneauthenuser");
			String smsPwd = sysConfig.getConfig("sms.password", "oneauthenpass");

			HttpClient httpClient = HttpClientBuilder.create().build();
			BufferedReader br = null;
			String output;
			StringBuilder responseBuilder = null;
			String response = null;
			URIBuilder uriBuilder;

			uriBuilder = new URIBuilder(smsHost); // สร้าง URL
			HttpPost postMethod = new HttpPost(uriBuilder.build()); // สร้าง URL โดยใช้ Method POST
			postMethod.setHeader("Content-Type", "application/x-www-form-urlencoded");
			postMethod.setHeader("Authorization", generateBasicAuth(smsUsr, smsPwd));
			postMethod.setHeader("Host", "www.corp-sms.com");
			postMethod.setHeader("Connection", "close");
			
			StringEntity params = new StringEntity(smsObject.toString(), Charset.forName("UTF-8")); 
			postMethod.setEntity(params);
			
			HttpResponse httpResponse = httpClient.execute(postMethod);// execut ค่าใน method API

			if (httpResponse == null) {
				throw new Exception("---------- http response is null -----------------");
			}

			int responseCode = httpResponse.getStatusLine().getStatusCode();
			if (responseCode == 201 || responseCode == 200) {
				br = new BufferedReader(new InputStreamReader((httpResponse.getEntity().getContent())));
				responseBuilder = new StringBuilder();

				while ((output = br.readLine()) != null) {
					responseBuilder.append(output);
				}
				System.out.println(httpResponse.getStatusLine().getStatusCode());

			} else {
				System.out.println("Failed : HTTP error code : " + httpResponse.getStatusLine().getStatusCode());

			}

			response = responseBuilder.toString();
			System.out.println("--------------- response ---------------\n" + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String generateBasicAuth(String username, String password) {
		String data = (username + ":" + password);
		return "Basic " + Base64.encodeBase64String(data.getBytes());
	}
}
