package th.in.oneauthen.emailObj;

public enum CertType{
	corporation_1year("10001"),
	corporation_2year("10002"),
	personal_1year("10003"),
	personal_2year("10004"),
	employees_1year("10005"),
	employees_2year("10006");

	  private String id;

	CertType(String id){
	    this.id = id;
	  }

	  public String getID(){
	    return id;
	  }

	}
