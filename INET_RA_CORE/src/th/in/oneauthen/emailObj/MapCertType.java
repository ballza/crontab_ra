package th.in.oneauthen.emailObj;



public class MapCertType {

	public static String getCertType(String certType) {
		
		if(CertType.corporation_1year.getID().equalsIgnoreCase(certType)
				|| CertType.corporation_2year.getID().equalsIgnoreCase(certType)) {
			return "นิติบุคคล";
		}else if (CertType.personal_1year.getID().equalsIgnoreCase(certType)
				|| CertType.personal_2year.getID().equalsIgnoreCase(certType)) {
			return "บุคคลทั่วไป";
		}else {
			return "เจ้าหน้าที่นิติบุคคล";
		}
	}
	
	public static String getCertYear(String certType) {
		
		if(CertType.corporation_1year.getID().equalsIgnoreCase(certType)||CertType.personal_1year.getID().equalsIgnoreCase(certType)||
				CertType.employees_1year.getID().equalsIgnoreCase(certType)	) {
			return "1";
		}else {
			return "2";
		}
		
	}
	
}
