package th.in.senza.otp.utility;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;

import com.itextpdf.barcodes.BarcodeQRCode;
import com.itextpdf.barcodes.qrcode.EncodeHintType;
import com.itextpdf.barcodes.qrcode.ErrorCorrectionLevel;

import th.in.oneauthen.object.OtpDB;
import th.in.oneauthen.object.UserRaDB;
import th.in.oneauthen.object.UserUidDB;
import th.in.oneauthen.object.DAO.OtpDAO;
import th.in.oneauthen.util2.MailNotificationUtil;
import th.in.oneauthen.util2.SMSNotificationUtil;

public class UserOTPUtility {
	private static Base32 B32Encoder = new Base32();

	public enum OTP {
		SMS(1, 300000), EMAIL(2, 300000), AUTHENTICATOR(3, 30000);

		public final int type;
		public final int interval;

		private OTP(int type, int interval) {
			this.type = type;
			this.interval = interval;
		}

		public static OTP getOTP(int type) {
			switch (type) {
			case 1:
				return SMS;
			case 2:
				return EMAIL;
			case 3:
				return OTP.AUTHENTICATOR;
			}
			return null;
		}
	}

	public static int OTP_STATUS_ACTIVE = 1;
	public static int OTP_STATUS_USED = 2;
	public static int OTP_STATUS_EXPIRED = 3;
	public static int OTP_STATUS_DISABLED = 4;

	public static OtpDB GENERATE_OTP(UserRaDB userId, int otpType) {
		byte[] seedValue = SecureRandom.getSeed(64);
		byte[] rawRefValue = SecureRandom.getSeed(64);

		OtpDB otpValue = new OtpDB();
		otpValue.setUserId(userId.getUserId());
		otpValue.setSeed(Hex.encodeHexString(seedValue));
		otpValue.setRefCode(B32Encoder.encodeAsString(rawRefValue).substring(0, 6));
		if (OTP.AUTHENTICATOR.type == otpType) {
			otpValue.setT0(0);
		} else {
			otpValue.setT0(System.currentTimeMillis());
		}
		otpValue.setType(otpType);
		otpValue.setStatus(OTP_STATUS_ACTIVE);
		return otpValue;
	}

	public static byte[] GENERATE_OTP_QR(OtpDB otpObject, UserRaDB username) throws DecoderException, IOException {
		String issuer = "Inet RA";
		String account = username.getUserName();

		byte[] rawSeedVal = Hex.decodeHex(otpObject.getSeed().toCharArray());
		String secret = B32Encoder.encodeAsString(rawSeedVal);

		String qrCodeData = "otpauth://totp/" + issuer + ":" + account + "?secret=" + secret + "&issuer=" + issuer;
		String fileFormat = "png";
		String charset = "UTF-8"; // or "ISO-8859-1"

		// Retrieve all necessary properties to create the barcode
		Map<EncodeHintType, Object> hints = new HashMap<>();
		// Character set
		hints.put(EncodeHintType.CHARACTER_SET, charset);
		hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
		BarcodeQRCode iTextQR = new BarcodeQRCode(qrCodeData, hints);
		Image qrImage = iTextQR.createAwtImage(Color.BLACK, Color.WHITE);
		// Create the QR-code
		qrImage = qrImage.getScaledInstance(450, 450, Image.SCALE_SMOOTH);
		BufferedImage bufferedImage = new BufferedImage(qrImage.getWidth(null), qrImage.getHeight(null),
				BufferedImage.TYPE_INT_RGB);
		bufferedImage.getGraphics().drawImage(qrImage, 0, 0, null);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, fileFormat, baos);
		return baos.toByteArray();
	}

	public static String GET_OTP_VALUE(OtpDB otpObject, long generateTime) {
		OTP otp = OTP.getOTP(otpObject.getType());
//		System.out.println("----- BEGIN OTP CALCULATION -----");
//		System.out.println(generateTime);
		String reflexTime = TOTP.reflectTimeCalculation(generateTime, otpObject.getT0(), otp.interval);
//		System.out.println(reflexTime);
		String otpVal = TOTP.generateTOTP(otpObject.getSeed(), reflexTime, "" + 6);
//		System.out.println(otpVal);
//		System.out.println("----- END OTP CALCULATION -----");
		return otpVal;
	}

	public static boolean VERIFY_OTP(String otp, int credentialId, String refCode) {
		return VERIFY_OTP(otp, System.currentTimeMillis(), credentialId, refCode);
	}

	public static boolean VERIFY_OTP(String otp, long acceptedTime, int credentialId, String refCode) {
		OtpDB otpDb = null;
		if (StringUtils.isEmpty(refCode)) {
			otpDb = new OtpDAO().findByCredential(credentialId);
		} else {
			otpDb = new OtpDAO().findByCredentialAndRefCode(credentialId, refCode);
		}
		return (GET_OTP_VALUE(otpDb, acceptedTime).equals(otp));
	}
	public static boolean VERIFY_OTP(String otp, OtpDB otpObj) {
		return VERIFY_OTP(otp, System.currentTimeMillis(), otpObj);
	}
	public static boolean VERIFY_OTP(String otp, long acceptedTime, OtpDB otpObj) {
		return (GET_OTP_VALUE(otpObj, acceptedTime).equals(otp));
	}
	
	private static final String otpMailSubject = "[INET RA] Activation Code";
	private static final String otpMailTemplate = "<h3>##PROFILE_NAME##</h3><div><h1>รหัสเปิดใช้งาน</h1><p>กรุณาใช้รหัสเปิดใช้งานต่อไปนี้สำหรับการเข้าสู่ระบบ  หมายเลขอ้างอิง : ##REF_CODE##</p><p>รหัสเปิดใช้งาน : <label style=\"font-weight: bold;\">##OTP##</label></p><p>หากท่านพบปัญหาการใช้งานรหัสเปิดใช้งาน (OTP) โปรดติดต่อ ra@inet.co.th เพื่อแจ้งปัญหา รหัสเปิดใช้งานนี้จะหมดอายุภายใน 5 นาที</p><p>ขอบคุณผู้ใช้บริการ<br/>INET RA Team</p></div>";
	
	private static final String NOTIFICATION_PARAM_PROFILE_NAME = "##PROFILE_NAME##";
	private static final String NOTIFICATION_PARAM_REF_CODE = "##REF_CODE##";
	private static final String NOTIFICATION_PARAM_EMAIL = "##EMAIL##";
	private static final String NOTIFICATION_PARAM_OTP = "##OTP##";
	
	public static boolean sendEmail( OtpDB otpObject, String mailAddr, String profileName ) {
		String mailSubject = new String(otpMailSubject);
		String mailContent = new String(otpMailTemplate);
		
		mailContent = mailContent.replaceAll(NOTIFICATION_PARAM_PROFILE_NAME, profileName);
		mailContent = mailContent.replaceAll(NOTIFICATION_PARAM_OTP, UserOTPUtility.GET_OTP_VALUE(otpObject, System.currentTimeMillis()));
		mailContent = mailContent.replaceAll(NOTIFICATION_PARAM_REF_CODE, otpObject.getRefCode());
		mailContent = mailContent.replaceAll(NOTIFICATION_PARAM_EMAIL, mailAddr);
		System.out.println(" ref : " + otpObject.getRefCode());
		System.out.println(" otp : " + UserOTPUtility.GET_OTP_VALUE(otpObject, System.currentTimeMillis()));
		
		new MailNotificationUtil().sendEmail(mailSubject, mailContent, mailAddr);
		return true;
	}

	private static final String smsTemplate = "Your OTP is ##OTP## (Ref: ##REF_CODE##) for login INET RA. OTP will be expired within 5 min.";
	public static boolean sendSMS( OtpDB otpObject, String mobileNo, String profileName  ) {
		String smsContent = new String(smsTemplate);
		
		smsContent = smsContent.replaceAll(NOTIFICATION_PARAM_PROFILE_NAME, profileName);
		smsContent = smsContent.replaceAll(NOTIFICATION_PARAM_OTP, UserOTPUtility.GET_OTP_VALUE(otpObject, System.currentTimeMillis()));
		smsContent = smsContent.replaceAll(NOTIFICATION_PARAM_REF_CODE, otpObject.getRefCode());
		
		SMSNotificationUtil.sendSMS(mobileNo, smsContent, otpObject.getRefCode());
		return true;
	}
	
//	public static void main(String[] args) throws Exception {
//		CredentialOTPUtility otp = new CredentialOTPUtility();
//		byte[] seed = SecureRandom.getSeed(64);
//		String b32Seed = new Base32().encodeAsString(seed);
//
//		otp.generateAuthenticatorCode(b32Seed);
//		System.out.println("Complete");
//
//		for (int i = 0; i < 1000; i++) {
//			String time = TOTP.reflectTimeCalculation(System.currentTimeMillis(), 0, 30000);
//			System.out.println(TOTP.generateTOTP(Hex.encodeHexString(seed), time, "" + 6));
//			Thread.sleep(1000);
//		}
//	}
//
//	public void generateAuthenticatorCode(String seedValue) throws Exception {
//		String issuer = "PromptSign";
//		String account = "ลายมือชื่อภาษาไทย";
//		String qrCodeData = "otpauth://totp/" + issuer + ":" + account + "?secret=" + seedValue + "&issuer=" + issuer;
//		String fileFormat = "png";
//		String charset = "UTF-8"; // or "ISO-8859-1"
//
//		try {
//			// Retrieve all necessary properties to create the barcode
//			Map<EncodeHintType, Object> hints = new HashMap<>();
//			// Character set
//			hints.put(EncodeHintType.CHARACTER_SET, charset);
//			hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
//			BarcodeQRCode iTextQR = new BarcodeQRCode(qrCodeData, hints);
//			Image qrImage = iTextQR.createAwtImage(Color.BLACK, Color.WHITE);
//			// Create the QR-code
//			qrImage = qrImage.getScaledInstance(600, 600, Image.SCALE_SMOOTH);
//			BufferedImage bufferedImage = new BufferedImage(qrImage.getWidth(null), qrImage.getHeight(null),
//					BufferedImage.TYPE_INT_RGB);
//			bufferedImage.getGraphics().drawImage(qrImage, 0, 0, null);
//			ImageIO.write(bufferedImage, fileFormat, new File("D:\\qr450.jpg"));
//		} catch (Exception e) {
//			System.err.println(e);
//		}
//	}
}
