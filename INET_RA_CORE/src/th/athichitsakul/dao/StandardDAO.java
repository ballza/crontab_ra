package th.athichitsakul.dao;

import java.util.List;

public interface StandardDAO<T> {
	public void save(T object) throws Exception;
	public void remove(Object primaryKey) throws Exception;
	public T find(Object primaryKey) throws Exception;
	public List<T> list() throws Exception;
}
