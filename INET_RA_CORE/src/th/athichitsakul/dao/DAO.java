package th.athichitsakul.dao;

import java.util.ArrayList;
import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class DAO {
	private static ArrayList<String> keyset = new ArrayList<>();
	private static final HashMap<String, EntityManagerFactory> factoryMAP = new HashMap<>();
	private EntityManagerFactory factory;
	private EntityManager entityManager;
	private EntityTransaction transaction;

	public DAO(String puID) {
		EntityManagerFactory factory = factoryMAP.get(puID);
		if (factory == null) {
//			System.out.println("=== Create new Entity manager factory for : " + puID + "===");
			this.factory = Persistence.createEntityManagerFactory(puID);
			this.init();
			keyset.add(puID);
			factoryMAP.put(puID, this.factory);
		}else {
//			System.out.println("=== Load registerd factory for : " + puID + "===");
			this.factory = factory;
			this.init();
		}
	}
	
	public void init() {
		this.entityManager = this.factory.createEntityManager(); 
	}
	
	public void begin() {
		if (entityManager==null || !entityManager.isOpen()) {
			entityManager = factory.createEntityManager();
		}
		transaction = entityManager.getTransaction();
		transaction.begin();
	}
	public void close() {
		try{
			transaction.commit();
		}catch (Exception e) {
			// Do nothing, just want to close}
		}
		try{
			entityManager.close();
		}catch (Exception e) {
			// Do nothing, just want to close}
		}
	}
	public EntityManager getEntityManager() {
		return entityManager;
	}
	public EntityTransaction getTransaction() {
		return transaction;
	}
	public void clear() {
		entityManager.clear();
	}
	public void save(Object entity) {
		entityManager.persist(entity);
		transaction.commit();
	}
	
	public static void destroy() {
		for (String factoryKey : keyset) {
			EntityManagerFactory emf = factoryMAP.get(factoryKey);
			emf.close();
			factoryMAP.remove(factoryKey);
		}
		factoryMAP.clear();
	}
}