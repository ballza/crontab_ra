package th.athichitsakul.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import th.athichitsakul.dao.DAO;
import th.athichitsakul.dao.StandardDAO;

public abstract class StandardDAOImpl<T> implements StandardDAO<T>{
	Class<T> objectClass;
	protected DAO localDAO = null;
	public StandardDAOImpl(String puID)  {
		localDAO = new DAO(puID);
		objectClass = (Class<T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	public void save(T object)throws Exception{
		try{
			localDAO.begin();
			localDAO.save(object);
		}catch(Exception e){
			if (localDAO.getTransaction().isActive()) {
				localDAO.getTransaction().rollback();
			}
			throw e;
		}finally{
			localDAO.close();
		}
	}
	public void remove(Object primaryKey)throws Exception{
		try{
			localDAO.begin();
			Object object = localDAO.getEntityManager().find(objectClass, primaryKey);
			if(object==null) throw new NullPointerException("Removing Object Not Found");
			localDAO.getEntityManager().remove(object);
		}catch(Exception e){
			throw e;
		}finally{
			localDAO.close();
		}
	}
	public T find(Object primaryKey) throws Exception{
		T queryResult = null;
		try{
			localDAO.begin();
			queryResult = localDAO.getEntityManager().find(objectClass, primaryKey);
		}catch(Exception e){
			throw e;
		}finally{
			localDAO.close();
		}
		return queryResult;
	}
	public List<T> list() throws Exception{
		List<T> queryResult = null;
		try{
			localDAO.begin();
			queryResult = localDAO.getEntityManager().createQuery("FROM ".concat(objectClass.getName() )).getResultList();
		}catch(Exception e){
			throw e;
		}finally{
			localDAO.close();
		}
		return queryResult;
	}
}
