package th.in.oneauthen;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class testDate {

	
	public static void main(String[] args){
		
//		String isoDatePattern = "EEEE MMMM dd HH:mm:ss z yyyy";
//
//		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(isoDatePattern);
//
//		String dateString = simpleDateFormat.format(new Date());
//		System.out.println(dateString);
//		
//		String input = "Fri Aug 14 18:51:05 ICT 2020";
//		 
//		SimpleDateFormat oldFormat = new SimpleDateFormat("EEEE MMMM dd HH:mm:ss z yyyy"); 
//		Date date = oldFormat.parse(input); 
//		SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd"); 
//		String output = newFormat.format(date); 
//		System.out.println("Date in old format: " + input); 
//		System.out.println("Date in new format: " + output);
		
		String jsonStr = "{\"Email\":\"eye.q.21@gmail.com\",\"CertType\":\"10006\",\"timeRegis\":\"Sun Jul 21 00:35:28 ICT 2019\"}";
		String jsonStrFail = "{\"C\":\"TH\",\"CN\":\"โคโคแม็ก\",\"Email\":\"thanonphat.su@oneauthen.in.th\",\"CertType\":\"10003\",\"O\":\"personal_only\",\"timeRegis\":\"23/04/2019 , 11:25\"}";
		

		
		try {
			System.out.println(format(jsonStr));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println(jsonStrFail);
		}

	
	}
	
	public static String format(String jsonStr) throws ParseException {
		
		
		JsonParser parser = new JsonParser();
		JsonObject json = parser.parse(jsonStr).getAsJsonObject();
		
		String rawDate = "";
		if(json.has("timeRegis")) {
			rawDate = json.get("timeRegis").getAsString();
			SimpleDateFormat oldFormat = new SimpleDateFormat("EEEE MMMM dd HH:mm:ss z yyyy"); 
			Date date = oldFormat.parse(rawDate); 
			SimpleDateFormat newFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.S");
			String output = newFormat.format(date); 
			json.addProperty("timeRegis", output);
			
		}else{
			rawDate = json.get("timeRevoke").getAsString();
			SimpleDateFormat oldFormat = new SimpleDateFormat("EEEE MMMM dd HH:mm:ss z yyyy"); 
			Date date = oldFormat.parse(rawDate); 
			SimpleDateFormat newFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.S");
			String output = newFormat.format(date); 
			json.addProperty("timeRevoke", output);
		}
		

		
		return json.toString();
		
		
	}
}
