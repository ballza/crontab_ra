package th.in.oneauthen;

import java.io.ByteArrayInputStream;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import javax.security.cert.X509Certificate;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;

import th.in.oneauthen.object.UserUidDB;
import th.in.oneauthen.object.UserRaDB;
import th.in.oneauthen.object.DAO.UserUidDAO;
import th.in.oneauthen.object.DAO.UserRaDAO;

public class testDB {
	public static void main(String[] args) throws Exception {
		UserRaDAO raUserDAO = new UserRaDAO();
		UserUidDAO userUidDAO = new UserUidDAO();
		for (int i =0; i<=10;i++) {
			UserRaDB raUser = new UserRaDB();
			raUser.setUserName(""+System.currentTimeMillis());
			raUser.setLastLogin(new Date());
			raUser.setPersonalID(""+System.currentTimeMillis());
			raUser.setSamlTokenUid(""+System.currentTimeMillis());
			raUserDAO.save(raUser);
			
			UserUidDB userUID = new UserUidDB();
			userUID.setUserName(""+System.currentTimeMillis());
			userUID.setPassword("foo123");
			userUID.setSamlTokenUid(""+System.currentTimeMillis());
			userUID.setOneId(""+System.currentTimeMillis());
			userUID.setLastLogin(new Date());
			userUID.setOneEmail(""+System.currentTimeMillis());
			userUID.setCompanyName(""+System.currentTimeMillis());
			userUID.setType(""+System.currentTimeMillis());
			userUidDAO.save(userUID);
		}
		System.out.println("DONE");
		System.exit(0);
	}
}
