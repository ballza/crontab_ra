package th.in.oneauthen;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;

public class AsymmetricKeyCryptographyTest {
	
	private static String HEX_PUBLIC = "30820122300d06092a864886f70d01010105000382010f003082010a0282010100bd204f16f2946317672314f9eaa2618af7c6c5c40dcb47f6df0d20621d1e63265f053fa7e89d3c304f6df6856df5888311dcd8eac617b22dad5103ec538fd297a3e1d5c4c9b5f1008d38ceba855bc1f9f4253bf4091427f80cb08b7d2d7795c815fe125d074c4ad6a1848eddf4f1ad917dbe7c83be16a0db6f264b3adc608b5687ee5399c68778810df6284aae9a3ee7905c0dd36165d6de64090c85bc628a2959eaec6e28bf48934b0ae9c8272497e16a92f2ae1e4dd680d4738855df85abca8b1651ef7865c3dfc8915070cd41f245a2f17f8b537ac75b1372a4b96547329f1daab77f84dec5911a7aeca4825f90d499043db789a38b8b9d8e267a81fc011f0203010001";
	private static String HEX_PRIVATE = "308204bc020100300d06092a864886f70d0101010500048204a6308204a20201000282010100bd204f16f2946317672314f9eaa2618af7c6c5c40dcb47f6df0d20621d1e63265f053fa7e89d3c304f6df6856df5888311dcd8eac617b22dad5103ec538fd297a3e1d5c4c9b5f1008d38ceba855bc1f9f4253bf4091427f80cb08b7d2d7795c815fe125d074c4ad6a1848eddf4f1ad917dbe7c83be16a0db6f264b3adc608b5687ee5399c68778810df6284aae9a3ee7905c0dd36165d6de64090c85bc628a2959eaec6e28bf48934b0ae9c8272497e16a92f2ae1e4dd680d4738855df85abca8b1651ef7865c3dfc8915070cd41f245a2f17f8b537ac75b1372a4b96547329f1daab77f84dec5911a7aeca4825f90d499043db789a38b8b9d8e267a81fc011f020301000102820100519c3784b778c0a9c09faad58d2234e53ab653709e6331d29dbd9453c22cf2ac0424c7ab34fd64939432e745fc850f3b3fe204d88a527f58a689671ffb8efc58e326f88fca5af07c44745700b5bb3aa1197157c59e854c1a059fc91163254f44dff89b03f9ba3a3113d197b4bf95ea47f5f2d4d82496af23017b8ccb017dc7a1f960375ea72117e51101ff6c70fd1317377484d1f7f433142f1cc1b41880665c87e10659f1c58e313b1d52552c6dd8e8d673491323cea40641a3bb59a944751ac43f01afa10a6574e09ab1ae98796cddc9f6ac85dce40a6963c1cdc179c08cb7c84399a5037be789c8beace3a445eb307a5d8d56ea13dcae0bc2a984059496c102818100e1a9bfcf22553d35bac79fc0a0b958d3fa84aa77a15171fcca8e944a49370a28e90432073de4e2b8d34e73b5802375877739ddbe13b1433d5f4215529b0124792ef7435cbdc55abda747f1ccb53c0b51784b4db08bfd2abf6a49adfb14e35793562c422d9487732a0932895cb140c6ca17723ad7479c1d507b1d569f0a07431502818100d68d22525e18e4d7cf193d054e9b698cca87411e3940afd4c2f73d591f1fa2360bf8c6c23c79365b9410a283ed26994654b6b9328ccde4d5c3bfda0405bf6b7192ed1b54c94f2ba80cb93c6012bc7e63698a66512bc5f6dd6efa9a29a4983a6b1c0c61b06615e6850bd8e26d3f2eb9e7fc0c543bf1fb33fd366bfc0b6afcd0630281801cba365fd11816d5bb346482ea330e3ad61e8370fd4958007d6ccf35f1c21ef9fc47fb726e8fafc925a54c17a3fb5f9fe22eacaae0e8e50d700e0296e482adb8fba0a0a3b2c8e7f752cf56d9df8c884853cf0a51da505d5bc6d5205fe155dbb536f82568960af43f3ab28348188c8d81f6a0251b78d347ec11f03731c1de10cd0281802d11d2bee8c363f72dab08957fd0c7d86a870517db410a6eb0520d921626a805a0782f1088719208a4cee5c4bee197db1d5a9106dc6b3801ebaff69557bbc14f47650593c7021992a0b19ccc35c360ed73333e911897ea66d3b45304226e53ff42121bc7597a8a45329ae9ddb72312ac77d7032ee0833b859a940e8b3b7b462f02818049188fcb15d6ad3b357af3afac457a15b9a592fe71c1ce372810bfda99668aa10a97f7efa02b6a8f03875e51e1f78ec76c30b627ae0d29665a744cbdb2c97b3f859f47ea66bcab477e1cb6fe453d7462d6666915d7956bb445affc0d702ec59631f71e6a6a5695bd0712a671913d38c387403335d19841bc22221b1c4ca19123";
	
	
	public static void main(String[] args) throws Exception {
//		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
//		kpg.initialize(2048);
//		KeyPair kp = kpg.generateKeyPair();
//		System.out.println("##### Public Key ######");
//		System.out.println(new BigInteger(kp.getPublic().getEncoded()).toString(16));
//		System.out.println("##### Private Key ######");
//		System.out.println(new BigInteger(kp.getPrivate().getEncoded()).toString(16));
		Security.addProvider(new BouncyCastleProvider());
		
		byte[] rawPub = Hex.decode(HEX_PUBLIC.getBytes("UTF-8"));
		byte[] rawPri = Hex.decode(HEX_PRIVATE.getBytes("UTF-8"));
		
		PKCS8EncodedKeySpec privSpec = new PKCS8EncodedKeySpec(rawPri);
		KeyFactory privKf = KeyFactory.getInstance("RSA");
		PrivateKey priv = privKf.generatePrivate(privSpec);		
		
		X509EncodedKeySpec pubSpec = new X509EncodedKeySpec(rawPub);
		KeyFactory pubKf = KeyFactory.getInstance("RSA");
		PublicKey pub = pubKf.generatePublic(pubSpec);
		
		String signatureMessage = "Sample sign content";
		String encryptMessage = "x20athinkpad;foo123";
		
		String jsOutput = "216619021d203c3d665474c1eee7ce1305585ee2c054346e19b984911cac715d3f0c3f0d3282dda67093d81e6af840f77ffbe0fd0d0abb34f2e73a8826fe7bba1f57ae8a3200f31a53bfbfe60e722a470be82bf22160c6aa77e1abcb5a59e412148e1d4dea8d5d1d4af07d71b025a2158b4021116a910ac1da27a4ee28845a80fb6874bcdd3edbd206426cf3cbfec7e961b259126de932a31df30b13ffd267f7de383c940d4399ef4e1a14bf3ec496109992c5dffb75c162c72ffbe922c223f4fa370ab17890ab10a8467fffa8631cce719d2bb2c58468b9a5b6f2ebb1da35e0d164aa35a384c8fdd7b529d9fef71183ec7504990ab4d551cfd97eeab3cbcf48";
//		for (int i=0; i<20; i++) {
//			long start = System.currentTimeMillis();
//			System.out.println("########## Sign - verify sample ###########");
//			System.out.println("Message = " + signatureMessage);
//			String signature = sign(signatureMessage, priv);
//			System.out.println("Signature text = " + signature);
//			System.out.println("Verification result = "+verify(signatureMessage, signature, pub));
//			
//			System.out.println("########## Encrypt - decrypt sample ###########");
//			System.out.println("Message = " + encryptMessage);
//			String cipher = encrypt(encryptMessage, pub);
//			System.out.println("Cipher text = " + cipher);
//			System.out.println("Decryption result = "+decrypt(cipher, priv));
//			System.err.println("---- End loop "+i+" in "+(System.currentTimeMillis()-start)+"ms");
//		}
		System.out.println("########## Encrypt - decrypt sample ###########");
		System.out.println("Message = " + encryptMessage);
		String cipher = "";
		for (int i=0; i<20; i++) {
			cipher = encryptHex(encryptMessage, pub);
			System.out.println("Cipher text"+i+"\t= " + cipher);
		}
		System.out.println("JSCipher   \t= "+jsOutput);
		System.out.println("Decryption result = "+decryptHex(cipher, priv));
		System.out.println("Decryption result = "+decryptHex(jsOutput, priv));
		
	}
	
	public static String sign(String message, PrivateKey privateKey) throws SignatureException{
	    try {
	        Signature sign = Signature.getInstance("SHA1withRSA");
	        sign.initSign(privateKey);
	        sign.update(message.getBytes("UTF-8"));
	        return new String(Base64.encode(sign.sign()),"UTF-8");
	    } catch (Exception ex) {
	        throw new SignatureException(ex);
	    }
	}

	public static boolean verify(String message, String signature, PublicKey publicKey) throws SignatureException{
	    try {
	        Signature sign = Signature.getInstance("SHA1withRSA");
	        sign.initVerify(publicKey);
	        sign.update(message.getBytes("UTF-8"));
	        return sign.verify(Base64.decode(signature.getBytes("UTF-8")));
	    } catch (Exception ex) {
	        throw new SignatureException(ex);
	    }
	}
	
	public static String encrypt(String message, PublicKey publicKey) throws SignatureException {
		try {
			Cipher crypto = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
			crypto.init(Cipher.ENCRYPT_MODE, publicKey);
			crypto.update(message.getBytes("UTF-8"));
			return new String(Base64.encode(crypto.doFinal()),"UTF-8");
		} catch (Exception ex) {
			throw new SignatureException(ex);
		}
	}
	
	public static String decrypt(String message, PrivateKey privateKey) throws SignatureException {
		try {
			Cipher crypto = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
			crypto.init(Cipher.DECRYPT_MODE, privateKey);
			crypto.update(Base64.decode(message.getBytes("UTF-8")));
			return new String(Base64.encode(crypto.doFinal()),"UTF-8");
		} catch (Exception ex) {
			throw new SignatureException(ex);
		}
	}
	public static String encryptHex(String message, PublicKey publicKey) throws SignatureException {
		try {
//			Cipher crypto = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
			Cipher crypto = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			crypto.init(Cipher.ENCRYPT_MODE, publicKey);
			crypto.update(message.getBytes("UTF-8"));
			return new String(Hex.encode(crypto.doFinal()),"UTF-8");
		} catch (Exception ex) {
			throw new SignatureException(ex);
		}
	}
	public static String decryptHex(String message, PrivateKey privateKey) throws SignatureException {
		try {
//			Cipher crypto = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
			Cipher crypto = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			crypto.init(Cipher.DECRYPT_MODE, privateKey);
			crypto.update(Hex.decode(message.getBytes("UTF-8")));
			return new String(crypto.doFinal(),"UTF-8");
		} catch (Exception ex) {
			throw new SignatureException(ex);
		}
	}
}
