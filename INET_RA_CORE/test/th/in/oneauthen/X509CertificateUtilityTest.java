package th.in.oneauthen;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import th.in.oneauthen.pki.certificate.X509CertificateUtility;

public class X509CertificateUtilityTest {
	public static void main(String[] args) throws CertificateException, FileNotFoundException {
		X509Certificate cert = (X509Certificate) CertificateFactory.getInstance("X509").generateCertificate(new FileInputStream("C:\\Users\\parad\\Downloads\\req0107559000371_00000_1549618230474.cer"));
		X509CertificateUtility certUtil = new X509CertificateUtility(cert);
		System.out.println(certUtil.getCertificateSubjectDN());
		System.out.println(certUtil.getCommonName());
		System.out.println(certUtil.getOrganizationIdentifier());
	}
}
  